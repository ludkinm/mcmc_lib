#include "hug.hpp"
#include "mcmc_lib/models/Quartic.hpp"
#include "mcmc_lib/models/model-factories.hpp"

int main(int argc, char *argv[])
{
    const arma::uword d{grab_arg<arma::uword> (1, argc, argv, 5)};
    auto mod = model::make_unit_scaled<model::vec::Quartic>(d);
    do_hug(mod, argc, argv);
    return 0;
}
