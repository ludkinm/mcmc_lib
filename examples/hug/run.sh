#!/bin/bash
DIM=20
NITS=100000
BURN=0
SAVE=0
TAIL=0
target=quartic

## equally spaced grids
Lmin=1
Lmax=10
Lstp=1

Emin=0.05
Emax=1.5
Estp=0.05

L=`Rscript --vanilla -e "cat(seq($Lmin, $Lmax, $Lstp))"`
eps=`Rscript --vanilla -e "cat(seq($Emin, $Emax, $Estp))"`

## get table header
if [[ ! (-a "hug_${target}_summary.tab") ]]
then
    echo "making the header"
    ./examples/hug/${target} 5 1 | head -n1 > hug_${target}_summary.tab;
fi

## run over a grid and get table body
for l in ${L[@]}; do
    for e in ${eps[@]}; do
	./examples/hug/${target} ${DIM} ${NITS} ${BURN} ${SAVE} ${TAIL} $l $e | tail -n1 >> hug_${target}_summary.tab;
	echo $l $e
    done
done
