#!/bin/bash
DIM=20
NITS=100000
BURN=0
SAVE=0
TAIL=0
target=quartic

Lmin=1
Lmax=10
Lstp=1

Kmin=0.05
Kmax=1.5
Kstp=0.05

lambda=`Rscript --vanilla -e "cat(seq($Lmin, $Lmax, $Lstp))"`
kappa=`Rscript --vanilla -e "cat(seq($Kmin, $Kmax, $Kstp))"`

## get table header
if [[ ! (-a "hophess_${target}_summary.tab") ]]
then
    echo "making the header"
    ./examples/hophess/${target} 5 1 | head -n1 > hophess_${target}_summary.tab;
fi

## run over a grid and get table body
for l in ${lambda[@]}; do
    for k in ${kappa[@]}; do
	./examples/hophess/${target} ${DIM} ${NITS} ${BURN} ${SAVE} ${TAIL} $l $k | tail -n1 >> hophess_${target}_summary.tab;
	echo $l $k
    done
done
