#include "mcmc_lib/bases/MCMC.hpp"
#include "mcmc_lib/util/KStest.hpp"
#include "mcmc_lib/kernels/HMC.hpp"
#include "mcmc_lib/states/Modelled.hpp"
#include "mcmc_lib/util/grab_arg.hpp"

void do_hmc(const std::shared_ptr<model::vec::DrawableGradient>& mod, int argc, char *argv[])
{
    const arma::uword nits     {grab_arg<arma::uword> (2, argc, argv, 10000)};
    const arma::uword burn     {grab_arg<arma::uword> (3, argc, argv, 0)};
    const bool save            {grab_arg<bool>        (4, argc, argv, true)};
    const double tail          {grab_arg<double>      (5, argc, argv, 0.0)};
    const arma::uword numSteps {grab_arg<arma::uword> (6, argc, argv, 10)};
    const double stepSize      {grab_arg<double>      (7, argc, argv, 0.5)};

    // make a state with a model
    state::modelled::Gradient x{mod};

    // make a kernel
    kernel::HMC kern(numSteps, stepSize, matrices::IDMatrix{x.dimension()}, x);

    // init state
    if(tail > 0.0){
	x = tail * arma::ones(x.dimension());
    } else{
	x = x.modPtr->draw();
    }

    // do MCMC
    mcmc::MCMC myMCMC{x};
    myMCMC.table_add_column("tail", tail);
    myMCMC.run(kern, nits, burn);
    myMCMC.table_add_column("tail", tail);
    myMCMC.calc_ess_xs();
    myMCMC.calc_ess_logpi();
    kstest::KS(myMCMC, *(x.modPtr));
    myMCMC.print_table(std::cout);
    if(save){
	std::string fname{kern.name + "_" + x.modPtr->name};
	myMCMC.calc_ess_xs(fname);
	myMCMC.calc_ess_logpi(fname);
	myMCMC.save_samples(fname);
    }
}
