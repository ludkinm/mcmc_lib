#!/bin/bash
target=$1
if [ -z $target ]; then
    echo "must provide target as \$1"
    exit
fi

DIM=20
NITS=100000
BURN=0
SAVE=0
TAIL=0

## equally spaced grids
Lmin=1
Lmax=10
Lstp=1

Emin=0.15
Emax=1.5
Estp=0.15

L=`Rscript --vanilla -e "cat(seq($Lmin, $Lmax, $Lstp))"`
eps=`Rscript --vanilla -e "cat(seq($Emin, $Emax, $Estp))"`

## get table header
if [[ ! (-a "hmc_${target}_summary.tab") ]]
then
    echo "making the header"
    ./examples/hmc/${target} 5 1 | head -n1 > hmc_${target}_summary.tab;
fi

## run over a grid and get table body
for l in ${L[@]}; do
    for e in ${eps[@]}; do
	./examples/hmc/${target} ${DIM} ${NITS} ${BURN} ${SAVE} ${TAIL} $l $e | tail -n1 >> hmc_${target}_summary.tab;
	echo $l $e
    done
done
