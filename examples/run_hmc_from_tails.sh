#!/bin/bash
DIM=8
NITS=25000

L=`Rscript --vanilla -e "cat(seq(1, 10, 1))"`
eps=`Rscript --vanilla -e "cat(seq(0.011, 0.02, 0.001))"`

## get table header
if [[ ! (-a "hmc_quartic_from_tails_summary.tab") ]]
then
    echo "making the header"
    ./examples/quartic/hmc_from_tails 5 1 | head -n1 > hmc_quartic_from_tails_summary.tab;
fi

## run over a grid and get table body
for l in ${L[@]}; do
    for e in ${eps[@]}; do
	./examples/quartic/hmc_from_tails ${DIM} ${NITS} $l $e | tail -n1 >> hmc_quartic_from_tails_summary.tab;
	echo $l $e
    done
done
