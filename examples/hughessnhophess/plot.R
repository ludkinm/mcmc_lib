library(ggplot2)
library(egg)
library(reshape2)

fs <- Sys.glob("./HugHessNHopHess_*_xs.tab")
xs <- lapply(fs, function(f){ x <- read.table(f); x <- cbind(i=1:nrow(x), x); mx <- melt(x, measure.vars = "i"); list(x=mx, f=f) })
ps <- lapply(xs, function(l){ ggplot(l$x, aes(V1,V2)) + geom_density_2d(bins=15) + ggtitle(l$f); })

ggarrange(plots=ps)
