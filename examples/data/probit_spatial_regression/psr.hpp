// probit spatial regression has:
// parameters: theta = rho, psi
// latent variables: Z
// data: Y
// the model is split into the model for (Z|Y, \theta) and (\theta | Y, Z)
#pragma once
#include <memory>
#include <armadillo>
#include "mcmc_lib/debug.hpp"
#include "mcmc_lib/bases/State.hpp"
#include "mcmc_lib/util/Checks.hpp"

namespace state{

namespace psr{

arma::vec phiByPhi(const arma::vec& x) { return arma::normpdf(x)/arma::normcdf(x); }

// data wrapper
struct PSR_data{
    arma::uword N;
    arma::uword D;
    arma::mat Y;
    arma::vec y; // columnwise layout of Y into one vector
    arma::mat dist;
    PSR_data(const arma::mat& Y_) : N{Y_.n_rows}, D{N*N}, Y{Y_}, y{arma::vectorise(Y)}
    {
	dist = arma::mat(N*N, N*N);
	for(arma::uword i=0; i < N; ++i)
	    for(arma::uword j=0; j < N; ++j)
		for(arma::uword k=0; k < N; ++k)
		    for(arma::uword l=0; l < N; ++l)
			dist(i+N*j, k+N*l) = sqrt(pow((double(i)-double(k))/double(N-1), 2) + pow((double(j)-double(l))/double(N-1),2));
    }
};

// forward decl the states for params and latents
struct PSR_param;
struct PSR_latent;
struct PSR_state;

// state for parameters
class PSR_param: public base::Logpi<arma::vec>{
private:
    double rho;
    double psi;
    std::shared_ptr<const PSR_data> data;
    std::shared_ptr<PSR_latent> Z;
    arma::mat sigma;
    arma::mat cholSigma;
    void calcSigma(void)
    {
	sigma = exp(rho - exp(-psi) * data->dist);
	cholSigma = arma::chol(sigma);
    }
public:
    friend PSR_latent;
    friend PSR_state;

    PSR_param(double r, double p, const std::shared_ptr<const PSR_data>& d) : rho{r}, psi{p}, data{d}, Z{}
    {
	calcSigma();
    }

    PSR_param(double r, double p, const std::shared_ptr<const PSR_data>& d, const std::shared_ptr<PSR_latent>& z) : rho{r}, psi{p}, data{d}, Z{z}
    {
	calcSigma();
    }

    virtual arma::uword dimension(void) const override
    {
	return 2;
    }

    virtual explicit operator arma::vec() const override
    {
	return arma::vec{rho, psi};
    }

    std::ostream& operator<<(std::ostream& os) const override
    {
	os << "(rho, psi): " << rho << ", " <<  psi << "; (data,Z): " << data << ", " << Z;
	DEBUG(os << sigma);
	return os;
    }

    virtual State& operator=(const arma::vec& y) override
    {
	rho = y(0);
	psi = y(1);
	calcSigma();
	return *this;
    }

    virtual double get_logpi(void) override;
};

// state for parameters
class PSR_latent: public base::Gradient<arma::vec, arma::vec>{
private:
    arma::vec z;
    std::shared_ptr<const PSR_data> data;
    std::shared_ptr<PSR_param> theta;
public:
    friend PSR_param;
    friend PSR_state;

    PSR_latent(const arma::vec& Z, const std::shared_ptr<const PSR_data>& d) : z{Z}, data{d}, theta{} {}

    PSR_latent(const arma::vec& Z, const std::shared_ptr<const PSR_data>& d, const std::shared_ptr<PSR_param>& t) : z{Z}, data{d}, theta{t} {}

    virtual arma::uword dimension(void) const override
    {
	return z.n_elem;
    }

    virtual explicit operator arma::vec() const override
    {
	return z;
    }

    std::ostream& operator<<(std::ostream& os) const override
    {
	os << "z: " << z.t() <<  "; (data, theta): " << data << ", " << theta;
	return os;
    }

    virtual State& operator=(const arma::vec& y) override
    {
	z = y;
	return *this;
    }

    virtual double get_logpi(void) override
    {
	return -0.5 * theta->rho * theta->rho - 0.5 * theta->psi * theta->psi - 0.5 * arma::dot(z, z) + arma::accu(log(arma::normcdf(data->y % (theta->cholSigma.t() * z))));
    }

    virtual arma::vec get_gradient(void) override
    {
	return -z + theta->cholSigma * (data->y % phiByPhi(data->y % (theta->cholSigma.t() * z)));
    }
};

double PSR_param::get_logpi(void)
{
    return -0.5 * rho * rho - 0.5 * psi * psi - 0.5 * arma::dot(Z->z, Z->z) + arma::accu(log(arma::normcdf(data->y % (cholSigma.t() * Z->z))));
}

// wrap both param and latent in struct
struct PSR_state{
    std::shared_ptr<const PSR_data> data;
    std::shared_ptr<PSR_param> theta;
    std::shared_ptr<PSR_latent> Z;
    PSR_state(const arma::mat& Y, double rho, double psi, const arma::vec& z) :
    data{std::make_shared<const PSR_data>(Y)},
    theta{std::make_shared<PSR_param>(rho, psi, data)},
    Z{std::make_shared<PSR_latent>(z, data)}
    {
	theta->Z = Z;
	Z->theta = theta;
    }
};

} // namespace psr
} // namespace state
