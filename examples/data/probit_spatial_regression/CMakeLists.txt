file(GLOB_RECURSE CPPFILES RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} *.cpp)

get_filename_component(GROUP ${CMAKE_CURRENT_SOURCE_DIR} NAME_WE)

set(GROUP ${GROUP}_${GROUP_DIR})
add_custom_target(${GROUP})

foreach(CPPFILE ${CPPFILES})
  get_filename_component(CPPNAME ${CPPFILE} NAME_WE)
  set(EXNAME ${CPPNAME}_${GROUP}) # each executable must be unique within a project so I name them cpp_directory_with_underscore_for_slash
  add_executable(${EXNAME} ${CPPFILE})
  target_link_libraries(${EXNAME} mcmc_lib)
  set_target_properties(${EXNAME} PROPERTIES OUTPUT_NAME ${CPPNAME}) # then rename them to the source file name
  add_dependencies(${GROUP} ${EXNAME})
  unset(EXNAME)
endforeach()

add_dependencies(visual_tests ${GROUP})

unset(GROUP)
