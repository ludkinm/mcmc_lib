#include "psr.hpp"
#include "mcmc_lib/kernels/RWM.hpp"
#include "mcmc_lib/kernels/Hug.hpp"

int main(void)
{
    arma::uword d{3};

    arma::mat Y = arma::ones(d, d);
    Y.diag() *= 10;

    state::psr::PSR_state S{Y, 1.0, 1.0, arma::randn(d*d)};

    std::cout << S.theta << std::endl;
    std::cout << S.Z << std::endl;

    std::cout << *(S.theta) << std::endl;
    std::cout << *(S.Z) << std::endl;

    kernel::RWM rwm_t{matrices::IDMatrix{0.1, 2}, *(S.theta)};
    kernel::Hug hug_z{1, 0.1, matrices::IDMatrix{d*d}, *(S.Z)};

    rwm_t.transition(*(S.theta));

    std::cout << *(S.theta) << std::endl;
    std::cout << *(S.Z) << std::endl;

    hug_z.transition(*(S.Z));

    std::cout << *(S.theta) << std::endl;
    std::cout << *(S.Z) << std::endl;

    return 0;
}
