#include <memory>
#include <armadillo>
#include "mcmc_lib/debug.hpp"
#include "mcmc_lib/bases/State.hpp"
#include "mcmc_lib/util/Checks.hpp"

namespace model{
namespace vec{

/**
 * Rasch model
 * M binary responses from N people
 * Y_{ij} = {-1, 1} for i = 1, ..., M, j = 1, ..., N
 * \beta_i - difficulty of question i
 * \eta_j - ability of person j
 * P(Y_{ij} = 1) = Bernoulii[ p = \Phi(\eta_j - \beta_i) ]
 * \beta_0 = 0 for identifiability
 * \theta = (\beta_1,...\beta_{M-1}, \eta_0, ..., \eta_{N-1}) is the parameter of interest
 * prior \beta, \eta ~ N(0,1/\tau)
 */
class Rasch : public vec::DrawableHessian<matrices::SquareMatrix>{
protected:
    arma::mat Y;
    arma::uword M;		// no. tests
    arma::uword N;		// no. people
    double tau;
    arma::mat calc_Z(const arma::vec& x) const
    {
	return calc_Z(get_beta(x), get_eta(x));
    }
    arma::mat calc_Z(const arma::vec& beta, const arma::vec& eta) const
    {
	arma::mat Z(arma::size(Y));
	for(arma::uword i = 0; i < M; ++i)
	    for(arma::uword j = 0; j < N; ++j)
		Z(i,j) = Y(i,j) * (eta(j) - beta(i));
	return Z;
    }
public:
    Rasch() = default;
    Rasch(arma::mat y, double t = 1.0) : Rasch{y.n_rows, y.n_cols, t} { Y = y; }
    Rasch(arma::uword m, arma::uword n, double t = 1.0) : vec::DrawableHessian<matrices::SquareMatrix>{}, Y{arma::mat(m,n)}, M{m}, N{n}, tau{t}
    {
	check::sign(t);
	name = "Rash";
	DEBUG_COUT("M,N = " << M << ", " << N);
    }
    arma::mat get_Y(void) const { return Y; }
    arma::uword get_M(void) const { return M; }
    arma::uword get_N(void) const { return N; }
    arma::vec get_beta(const arma::vec& x) const
    {
	return arma::join_vert(arma::vec{0.0}, x.subvec(0, M-2));
    }
    arma::vec get_eta(const arma::vec& x) const
    {
	return x.subvec(M-1, dimension()-1);
    }
    arma::uword dimension() const override { return M + N - 1; }

    double logpi(const arma::vec& x) const override
    {
	arma::mat Z{calc_Z(x)};
	return arma::accu(log(arma::normcdf(Z)) - 0.5 * tau * arma::dot(x,x));
    }

    arma::vec gradient(const arma::vec& x) const override
    {
	arma::mat Z{ calc_Z(x) };
	Z = Y % arma::normpdf(Z) / arma::normcdf(Z);
	arma::vec gBeta{arma::zeros(M)};
	arma::vec gEta{arma::zeros(N)};
	for(arma::uword i = 0; i < M; ++i)
	    for(arma::uword j = 0; j < N; ++j){
		gBeta(i) -= Z(i,j);
		gEta(j) -= Z(i,j);
	    }
	arma::vec g = arma::join_vert(gBeta, gEta);
	g = g.subvec(1, dimension());
	return g - tau * x;
    }

    matrices::SquareMatrix negative_hessian(const arma::vec& x) const override
    {
	arma::mat h = arma::zeros(M+N+1, M+N+1);
	arma::vec beta = get_beta(x);
	arma::vec eta  = get_eta(x);
	for(arma::uword i = 0; i < M; ++i){
	    for(arma::uword j = 0; j < N; ++j){
		double tmp = Y(i,j) * (eta(j) - beta(i));
		double pbp = arma::normpdf(tmp)/arma::normcdf(tmp);
		// d2/beta2
		h(i, i) += pbp * (tmp + pbp);
		// d2/eta2
		h(j+M, j+M) += pbp * (tmp + pbp);
		// d2/eta/beta
		h(j+M, i) = h(i, j+M) = -pbp * (tmp + pbp);
		// others = 0
	    }
	}
	h = h.submat(1, 1, x.n_elem, x.n_elem); // cut off the bit for beta_0
	h.diag() += tau;
	return matrices::SquareMatrix{h};
    }

    arma::vec draw() const override
    {
	const arma::vec x = arma::randn(dimension());
	arma::vec beta = get_beta(x);
	arma::vec eta = get_eta(x);
	arma::mat Z = calc_Z(beta, eta);
	Z = Y % arma::normpdf(Z) / arma::normcdf(Z);
	for(arma::uword i = 0; i < dimension(); ++i){
	    for(arma::uword i = 1; i < M; ++i){
		beta(i) = -sum(Z.row(i))/tau/dimension();
		//update Z
		Z = calc_Z(beta, eta);
		Z = Y % arma::normpdf(Z) / arma::normcdf(Z);
	    }
	    for(arma::uword j = 0; j < N; ++j){
		eta(j) = sum(Z.col(j))/tau/dimension();
		//update Z
		Z = calc_Z(beta, eta);
		Z = Y % arma::normpdf(Z) / arma::normcdf(Z);
	    }
	}
	return arma::join_vert(beta.subvec(1, M-1), eta);
    }
};

} // namespace vec

namespace rasch{

/**
 * Simulate data from the model
 */
std::tuple<arma::vec, arma::vec, arma::mat> sim(arma::uword M, arma::uword N, double tau)
{
    arma::vec beta {arma::join_vert(arma::vec{0.0}, arma::randn(M-1)/sqrt(tau))};
    arma::vec eta {arma::randn(N)/sqrt(tau)};
    arma::mat Y(M,N);
    for(arma::uword i = 0; i < M; ++i)
	for(arma::uword j = 0; j < N; ++j)
	    Y(i,j) = ( (arma::randu() < arma::normcdf(eta(j) - beta(i))) ? 1.0 : -1.0);
    return {beta, eta, Y};
}

/**
 * pnorm from R - eps avoids overflow in the erf_inv functions
 */
arma::vec pnorm(const arma::vec& x, double eps = 0.001)
{
    arma::vec z = (1-2*eps) * x + eps;
    z.transform( [](double val) { return sqrt(2.0) * boost::math::erf_inv(2.0*val - 1.0); } );
    return z;
}

std::tuple<arma::vec, arma::vec> approxStart(const arma::mat& Y)
{
    arma::vec betaHat = arma::mean(0.5*(1+Y), 1);
    arma::vec etaHat = arma::mean(0.5*(1+Y), 0).t();
    return {-pnorm(betaHat), pnorm(etaHat)};
}
}
}

namespace state{
namespace rasch{

inline double phiByPhi(const double& x) { return arma::normpdf(x)/arma::normcdf(x); }

/**
 * Model over (beta, eta) where beta[0] = 0.0 for identifiablity
 * We work with the full state then set beta[0] to 0.0 on construction/assignment
 * Y_{ij} = {-1, 1} for i = 1, ..., nTests, j = 1, ..., nStudents
 * \beta_i - difficulty of question i
 * \eta_j - ability of person j
 * P(Y_{ij} = 1) = Bernoulii[ p = \Phi(\eta_j - \beta_i) ]
 * \beta_0 = 0 for identifiability
 * \theta = (\beta_1,...\beta_{nTests-1}, \eta_0, ..., \eta_{nStudents-1}) is the parameter of interest
 * prior \beta, \eta ~ N(0,1/\tau)
 */
class Rasch: public base::Hessian<std::pair<arma::vec, arma::vec>, arma::vec, matrices::SquareMatrix>{
private:
    using Base = typename base::Hessian<std::pair<arma::vec, arma::vec>, arma::vec, matrices::SquareMatrix>;
    std::shared_ptr<const arma::mat> Y; // pointer to data
    arma::uword nTests;
    arma::uword nStudents;
    double tau;
    arma::vec beta;
    arma::vec eta;
    arma::mat Z;
    void update_Z(void)
    {
	Z.set_size(nTests, nStudents);
	for(arma::uword i = 0; i < nTests; ++i)
	    for(arma::uword j = 0; j < nStudents; ++j)
		Z(i,j) = Y->at(i,j) * (eta(j) - beta(i));
    }
public:
    Rasch(const arma::vec& b, const arma::vec& e,
	  const arma::mat& data, double t=1.0): Y{std::make_shared<arma::mat>(data)},
						nTests{Y->n_rows},
						nStudents{Y->n_cols},
						tau{t},
						beta{b}, eta{e},
						Z{}
    {
	DEBUG_COUT("Building a rasch state: " << *this);
	if(beta.n_elem != nTests)
	    throw std::logic_error("Mismatch in Rasch state: dimensions of beta and data are incompatible");
	if(eta.n_elem != nStudents)
	    throw std::logic_error("Mismatch in Rasch state: dimensions of eta and data are incompatible");
	check::sign(t);
	beta(0) = 0.0;
	DEBUG_COUT("Now update Z");
	update_Z();
	DEBUG_COUT("Built a rasch state: " << *this);
    }

    arma::uword n_tests(void) const { return nTests; }
    arma::uword n_students(void) const { return nStudents; }

    double get_logpi(void) override
    {
	return arma::accu(log(arma::normcdf(Z))) - 0.5 * tau * (arma::dot(beta, beta) + arma::dot(eta, eta));
    }

    arma::vec get_gradient(void) override
    {
	arma::vec gBeta{-tau * beta};
	arma::vec gEta{-tau * eta};
	for(arma::uword i = 0; i < nTests; ++i){
	    for(arma::uword j = 0; j < nStudents; ++j){
		double tmp { arma::normpdf(Z(i,j)) / arma::normcdf(Z(i,j)) };
		gBeta(i) -= Y->at(i,j) * tmp;
		gEta(j)  += Y->at(i,j) * tmp;
	    }
	}
	return arma::join_vert(gBeta.subvec(1, nTests-1), gEta);
    }

    matrices::SquareMatrix get_negative_hessian(void) override
    {
	arma::mat h{arma::zeros(dimension()+1, dimension()+1)};
	h.diag() += tau;
	for(arma::uword i = 0; i < nTests; ++i){
	    for(arma::uword j = 0; j < nStudents; ++j){
		double pbp = phiByPhi(Z(i,j));
		// d2/beta2
		h(i, i) += pbp * (Z(i,j) + pbp);
		// d2/eta2
		h(j+nTests, j+nTests) += pbp * (Z(i,j) + pbp);
		// d2/eta/beta
		h(j+nTests, i) = h(i, j+nTests) = -pbp * (Z(i,j) + pbp);
	    }
	}
	return matrices::SquareMatrix{h.submat(1,1,dimension(),dimension())};
    }

    // override from state::base::Hessian
    arma::uword dimension(void) const override { return nTests + nStudents-1; }

    explicit operator arma::vec() const override { return arma::join_vert(beta.subvec(1, nTests-1), eta); }

    std::ostream& operator<<(std::ostream& os) const override
    {
	os << "beta: "; beta.t().raw_print(os); os << "eta: "; eta.t().raw_print(os);
	DEBUG(os << Z);
	return os;
    }

    Rasch& operator=(const arma::vec& y) override
    {
	beta.subvec(1, nTests-1) = y.subvec(0, nTests-2);
	beta[0] = 0.0;
	eta = y.subvec(nTests-1, dimension()-1);
	update_Z();
	return *this;
    }

};

} // namespace rasch
} // namespace state
