#include "mcmc_lib.hpp"
#include "mcmc_lib/states/Rasch.hpp"
#include "mcmc_lib/models/Rasch.hpp"

int main(int argc, char *argv[])
{
    const arma::uword nTests    = grab_arg<arma::uword>(1, argc, argv, 10);
    const arma::uword nStudents = grab_arg<arma::uword>(2, argc, argv, 100);
    const double tau = grab_arg<double>(3, argc, argv, 1.0);

    arma::arma_rng::set_seed_random();

    auto [beta, eta, Y] = model::rasch::sim(nTests, nStudents, tau);

    beta.save("rasch_beta.tab", arma::raw_ascii);
    eta.save("rasch_eta.tab", arma::raw_ascii);
    Y.save("rasch_Y.tab", arma::raw_ascii);

    return 0;
}
