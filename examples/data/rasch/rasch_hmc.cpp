#include "mcmc_lib.hpp"
#include "Rasch.hpp"
#include "Rasch.hpp"

int main(int argc, char *argv[])
{
    const arma::uword nits = grab_arg<arma::uword>(1, argc, argv, 20000);
    const arma::uword burn = grab_arg<arma::uword>(2, argc, argv, nits/2);
    const arma::uword B = grab_arg<arma::uword>(3, argc, argv, 8);
    const double del = grab_arg<double>(4, argc, argv, 0.225);
    const double sig = grab_arg<double>(5, argc, argv, 1.0);
    const double tau = grab_arg<double>(6, argc, argv, 1.0);

    arma::mat Y;
    Y.load("rasch_Y.tab", arma::raw_ascii);

    arma::arma_rng::set_seed_random();

    auto [beta, eta] = model::rasch::approxStart(Y);

    state::rasch::Rasch s{beta, eta, Y, tau};

    DEBUG_COUT("s = " << s
	       << "\nx = " << arma::vec(s)
	       << "\ng = " << s.get_gradient()
	       << "\nh = " << s.get_negative_hessian()
	       << std::endl);

    arma::vec sigma{sig * arma::join_vert(double(s.n_tests())/double(s.n_students()) * arma::ones(s.n_tests()-1), arma::ones(s.n_students()))};

    kernel::HMC kern{B, del, matrices::DiagMatrix{1.0/sigma}, s};

    mcmc::MCMC m{s};

    m.run(kern, nits, burn);
    m.calc_ess_xs("rasch_hmc");
    m.calc_ess_logpi("rasch_hmc");
    m.print_table(std::cout);
    m.save_samples("rasch_hmc");

    return 0;
}
