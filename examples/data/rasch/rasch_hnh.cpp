#include "mcmc_lib.hpp"
#include "mcmc_lib/states/Rasch.hpp"
#include "mcmc_lib/models/Rasch.hpp"

int main(int argc, char *argv[])
{
    const arma::uword nits = grab_arg<arma::uword>(1, argc, argv, 20000);
    const arma::uword burn = grab_arg<arma::uword>(2, argc, argv, nits/2);
    const arma::uword B = grab_arg<arma::uword>(3, argc, argv, 8);
    const double del = grab_arg<double>(4, argc, argv, 0.225);
    const double lam = grab_arg<double>(5, argc, argv, 1.444);
    const double kap = grab_arg<double>(6, argc, argv, 0.1);
    const double sig = grab_arg<double>(7, argc, argv, 1.0);
    const double tau = grab_arg<double>(8, argc, argv, 1.0);

    arma::mat Y;
    Y.load("rasch_Y.tab", arma::raw_ascii);

    arma::arma_rng::set_seed_random();

    auto [beta, eta] = model::rasch::approxStart(Y);

    state::rasch::Rasch s{beta, eta, Y, tau};

    DEBUG_COUT("s = " << s
	       << "\nx = " << arma::vec(s)
	       << "\ng = " << s.get_gradient()
	       << "\nh = " << s.get_negative_hessian()
	       << std::endl);

    arma::vec sigma{sig * arma::join_vert(double(s.n_tests())/double(s.n_students()) * arma::ones(s.n_tests()-1), arma::ones(s.n_students()))};

    kernel::Hug hug{B, del, matrices::DiagMatrix{sigma}, s};
    kernel::Hop hop{lam, kap, matrices::DiagMatrix{sigma}, s};
    kernel::Alternator kern{hug, hop};

    mcmc::MCMC m{s};

    m.run(kern, nits, burn);
    m.calc_ess_xs("rasch_hnh");
    m.calc_ess_logpi("rasch_hnh");
    m.print_table(std::cout);
    m.save_samples("rasch_hnh");

    return 0;
}
