#include "mcmc_lib.hpp"
#include "mcmc_lib/models/CauchitRegression.hpp"

using namespace model::cauchit_regression;
int main(int argc, char *argv[])
{
    const arma::uword nits = grab_arg<arma::uword>(1, argc, argv, 20000);
    const arma::uword burn = grab_arg<arma::uword>(2, argc, argv, nits/2);
    const arma::uword B = grab_arg<arma::uword>(3, argc, argv, 8);
    const double del = grab_arg<double>(4, argc, argv, 0.225);
    const double lam = grab_arg<double>(5, argc, argv, 1.444);
    const double kap = grab_arg<double>(6, argc, argv, 0.1);
    const double sig = grab_arg<double>(7, argc, argv, 1.0);
    const double tau = grab_arg<double>(8, argc, argv, 1.0);

    arma::vec Y; Y.load("cauchit_Y.tab", arma::raw_ascii);
    arma::mat X; X.load("cauchit_X.tab", arma::raw_ascii);

    arma::arma_rng::set_seed_random();

    state::modelled::Hessian s{std::make_shared<CauchitRegression>(Y, X, tau)};
    s = arma::randn(s.dimension())/sqrt(tau);

    DEBUG_COUT("s = " << s
	       << "\nl = " << s.get_logpi()
	       << "\ng = " << s.get_gradient()
	       << "\nh = " << s.get_negative_hessian()
	       << std::endl);

    kernel::HugHess hug{B, del, sig, s};
    kernel::HopHess hop{lam, kap, sig, s};
    kernel::Alternator kern{hug, hop};

    mcmc::MCMC m{s};
    const std::string fname{ "cauchit_hnh_hess" };
    m.run(kern, nits, burn);
    m.calc_ess_xs(fname);
    m.calc_ess_logpi(fname);
    m.print_table(std::cout);
    m.save_samples(fname);

    return 0;
}
