#include "mcmc_lib.hpp"
#include "mcmc_lib/models/CauchitRegression.hpp"

int main(int argc, char *argv[])
{
    const arma::uword dim = grab_arg<arma::uword>(1, argc, argv, 10);
    const arma::uword obs = grab_arg<arma::uword>(2, argc, argv, 100);
    const double tau = grab_arg<double>(3, argc, argv, 1.0);

    auto [beta, X, Y] = model::cauchit_regression::sim(obs, dim, tau);
    beta.save("cauchit_beta.tab", arma::raw_ascii);
    Y.save("cauchit_Y.tab", arma::raw_ascii);
    X.save("cauchit_X.tab", arma::raw_ascii);

    return 0;
}
