#pragma once

#include <armadillo>
#include <cmath>
#include "mcmc_lib/bases/Model.hpp"
#include "mcmc_lib/models/Normal.hpp"
#include "mcmc_lib/models/Mixture.hpp"
#include "mcmc_lib/util/Checks.hpp"
#include "mcmc_lib/debug.hpp"

namespace model{
namespace cauchit_regression{

arma::vec cdf(const arma::vec& x) { return 0.5 + atan(x)/arma::datum::pi; }
arma::vec pdf_recip(const arma::vec& x) { return arma::datum::pi * (1.0 + arma::square(x)); }
arma::vec pdf(const arma::vec& x) { return 1.0/pdf_recip(x); }
arma::vec pdf_div_cdf(const arma::vec& x) { return pdf(x)/cdf(x); }

/**
 * Regression for binary repsponces with a cauchit link function
 * Prior on beta is a N(0, tau^{-1}I) so tau is the precision for each dimension
 */
class CauchitRegression: public vec::Hessian<matrices::CholMatrix>{
private:
    arma::vec Y;
    arma::mat X;
    double tau;
public:
    CauchitRegression(arma::vec& y, arma::mat& x, double t=1.0) : Y{y}, X{x}, tau{t}
    {
	this->name = "CauchitRegression";
    }

    virtual arma::uword dimension(void) const override { return X.n_cols; }

    double logpi(const arma::vec& beta) const override
    {
	DEBUG_COUT("CR::logpi");
	arma::vec z{ Y % (X * beta) };
	return arma::sum(log(cdf(z))) - 0.5 * tau * arma::dot(beta, beta);
    }

    arma::vec gradient(const arma::vec& beta) const override
    {
	DEBUG_COUT("CR::gradient");
	arma::vec z{ Y % (X * beta) };
	return -tau * beta + X.t() * (Y % pdf_div_cdf(z));
    }

    matrices::CholMatrix negative_hessian(const arma::vec& beta) const override try
    {
	DEBUG_COUT("CR::negative_hessian");
	arma::vec z{ Y % (X * beta) };
	z = (1 + 2.0 * arma::datum::pi * z % cdf(z)) % arma::square(pdf_div_cdf(z));
	arma::mat h{ X.t() * arma::diagmat(z) * X };
	h.diag() += tau;
	return matrices::CholMatrix{h};
    } catch(std::exception& e){
	std::cerr << "CachitRegression::negative_hessian throws: beta:\n" << beta << std::endl;
	throw e;
    }
};

/**
 * Simulate data from the model
 */
std::tuple<arma::vec, arma::mat, arma::vec> sim(arma::uword N, arma::uword D, double tau)
{
    arma::vec beta {sqrt(tau) * arma::randn(D)};
    arma::mat X {arma::randn(N, D)};
    arma::vec Y {cdf(X * beta)};
    for(arma::uword i = 0; i < N; ++i)
	Y(i) = ( (arma::randu() < Y(i)) ? 1.0 : -1.0);
    return {beta, X, Y};
}

} // namespace cauchit_regression
} // namespace model
