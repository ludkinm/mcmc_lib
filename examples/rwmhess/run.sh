#!/bin/bash
DIM=20
NITS=100000
BURN=0
SAVE=0
TAIL=0
target=quartic

## equally spaced grid for beta
Bmin=0.1
Bmax=5.0
Bstp=0.1

beta=`Rscript --vanilla -e "cat(seq($Bmin, $Bmax, $Bstp))"`

## get table header
if [[ ! (-a "rwmhess_${target}_summary.tab") ]]
then
    echo "making the header"
    ./examples/rwmhess/${target} 5 1 | head -n1 > rwmhess_${target}_summary.tab;
fi

## run over a grid and get table body
for b in ${beta[@]}; do
    ./examples/rwmhess/${target} ${DIM} ${NITS} ${BURN} ${SAVE} ${TAIL} $b | tail -n1 >> rwmhess_${target}_summary.tab;
	echo $b
done
