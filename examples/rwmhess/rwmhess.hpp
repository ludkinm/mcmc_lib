#include "mcmc_lib/bases/MCMC.hpp"
#include "mcmc_lib/util/KStest.hpp"
#include "mcmc_lib/kernels/RWMHess.hpp"
#include "mcmc_lib/states/Modelled.hpp"
#include "mcmc_lib/util/grab_arg.hpp"

template<typename M, typename H = typename M::HessianType>
void do_rwmhess(const std::shared_ptr<M>& mod, int argc, char *argv[])
{
    const arma::uword nits {grab_arg<arma::uword> (2, argc, argv, 10000)};
    const arma::uword burn {grab_arg<arma::uword> (3, argc, argv, 0)};
    const bool save        {grab_arg<bool>        (4, argc, argv, true)};
    const double tail      {grab_arg<double>      (5, argc, argv, 0.0)};
    const double beta      {grab_arg<double>      (6, argc, argv, 1.0)};

    // make a state with a model
    state::modelled::Hessian<M,H> x{mod};

    // make a kernel
    kernel::RWMHess kern(beta, x);

    // draw init state from model
    if(tail > 0.0){
	x = tail * arma::ones(x.dimension());
    } else{
	x = x.modPtr->draw();
    }

    // do MCMC
    mcmc::MCMC myMCMC{x};
    myMCMC.table_add_column("tail", tail);
    myMCMC.table_add_column("beta", beta);
    myMCMC.run(kern, nits, burn);
    myMCMC.calc_ess_xs();
    myMCMC.calc_ess_logpi();
    kstest::KS(myMCMC, *(x.modPtr));
    myMCMC.print_table(std::cout);
    if(save){
	std::string fname{kern.name + "_" + x.modPtr->name};
	myMCMC.calc_ess_xs(fname);
	myMCMC.calc_ess_logpi(fname);
	myMCMC.save_samples(fname);
    }
}
