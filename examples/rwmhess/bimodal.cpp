#include "rwmhess.hpp"
#include "mcmc_lib/models/Bimodal.hpp"
#include "mcmc_lib/models/AddGaussian.hpp"
#include "mcmc_lib/models/model-factories.hpp"
#include "mcmc_lib/util/SquareMatrices.hpp"

int main(int argc, char *argv[])
{
    const arma::uword d{grab_arg<arma::uword> (1, argc, argv, 5)};

    if(d == 2){
	auto mod = model::make_unit_scaled<model::vec::Bimodal>(d);
	do_rwmhess<model::vec::Bimodal, matrices::AbsEigenMatrix>(mod, argc, argv);
    } else{
	auto mod = model::make_unit_scaled<model::vec::AddGaussian<model::vec::Bimodal>>(d);
	do_rwmhess<model::vec::AddGaussian<model::vec::Bimodal>, matrices::AbsEigenMatrix>(mod, argc, argv);
    }

    return 0;
}
