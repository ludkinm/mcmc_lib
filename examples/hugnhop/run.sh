#!/bin/bash
target=$1
if [ -z $target ]; then
    echo "must provide target as \$1"
    exit
fi

DIM=20
NITS=100000
BURN=0
SAVE=0
TAIL=0

## log2 scale
Bmin=1
Bmax=10
Bstp=1

Dmin=-3
Dmax=2
Dstp=1

Lmin=-2
Lmax=6
Lstp=1

Kmin=-4
Kmax=2
Kstp=1

B=`Rscript --vanilla -e "cat(seq($Bmin, $Bmax, $Bstp))"`
delta=`Rscript --vanilla -e "cat(2^(seq($Dmin, $Dmax, $Dstp)))"`
lambda=`Rscript --vanilla -e "cat(2^(seq($Lmin, $Lmax, $Lstp)))"`
kappa=`Rscript --vanilla -e "cat(2^(seq($Kmin, $Kmax, $Kstp)))"`

echo "B=${B[@]}"
echo "delta=${delta[@]}"
echo "lam=${lambda[@]}"
echo "kap=${kappa[@]}"

## get table header
if [[ ! (-a "hugnhop_${target}_summary.tab") ]]
then
    echo "making the header"
    ./examples/hugnhop/${target} 5 1 | head -n1 > hugnhop_${target}_summary.tab;
fi

## run over a grid and get table body
for b in ${B[@]}; do
    for d in ${delta[@]}; do
	for l in ${lambda[@]}; do
	    for k in ${kappa[@]}; do
		./examples/hugnhop/${target} ${DIM} ${NITS} ${BURN} ${SAVE} ${TAIL} $b $d $l $k | tail -n1 >> hugnhop_${target}_summary.tab;
		echo $b $d $l $k
	    done
	done
    done
done
