#!/bin/bash
DIM=8
NITS=100000

lam=`Rscript --vanilla -e "cat(seq(0.5, 10.0, 0.5))"`
kap=`Rscript --vanilla -e "cat(seq(0.25, 5.0, 0.25))"`

## get table header
if [[ ! (-a "hugnhop_quartic_from_tails_summary.tab") ]]
then
    echo "making the header"
    ./examples/quartic/hugnhop_from_tails 5 1 | head -n1 > hugnhop_quartic_from_tails_summary.tab;
fi

## run over a grid and get table body
for l in ${lam[@]}; do
    for k in ${kap[@]}; do
	echo $b $d $l $k
	./examples/quartic/hugnhop_from_tails ${DIM} ${NITS} $b $d $l $k | tail -n1 >> hugnhop_quartic_from_tails_summary.tab;
    done
done
