#!/bin/bash
DIM=8
NITS=10000

lam=`Rscript --vanilla -e "cat(seq(0.5, 10.0, 0.5))"`
kap=`Rscript --vanilla -e "cat(seq(0.25, 5.0, 0.25))"`

## get table header
if [[ ! (-a "hop_quartic_summary.tab") ]]
then
    echo "making the header"
    ./quartic/hop 5 1 | head -n1 > hop_quartic_summary.tab;
fi

## run over a grid and get table body
for l in ${lam[@]}; do
    for k in ${kap[@]}; do
	./quartic/hop ${DIM} ${NITS} $l $k | tail -n1 >> hop_quartic_summary.tab;
	echo $l $k
    done
done

lam=`Rscript --vanilla -e "cat(seq(0.5, 10.0, 0.5))"`
kap=`Rscript --vanilla -e "cat(seq(0.25, 5.0, 0.25))"`
D=`Rscript --vanilla -e "cat(seq(5, 25, 5))"`

## get table header
if [[ ! (-a "trunkhop_quartic_summary.tab") ]]
then
    echo "making the header"
    ./quartic/trunkhop 5 1 | head -n1 > trunkhop_quartic_summary.tab;
fi

## run over a grid and get table body
for l in ${lam[@]}; do
    for k in ${kap[@]}; do
	for d in ${D[@]}; do
	    ./quartic/trunkhop ${DIM} ${NITS} $l $k $d | tail -n1 >> trunkhop_quartic_summary.tab;
	    echo $l $k $d
	done
    done
done
