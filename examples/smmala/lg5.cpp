#include "smmala.hpp"
#include "mcmc_lib/models/LG.hpp"
#include "mcmc_lib/models/model-factories.hpp"

int main(int argc, char *argv[])
{
    const arma::uword d{grab_arg<arma::uword> (1, argc, argv, 5)};
    auto mod = model::make_unit_scaled<model::vec::LG5>(d);
    do_smmala(mod, argc, argv);
    return 0;
}
