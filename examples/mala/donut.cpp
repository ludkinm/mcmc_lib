#include "mala.hpp"
#include "mcmc_lib/models/Donut.hpp"
#include "mcmc_lib/models/model-factories.hpp"

int main(int argc, char *argv[])
{
    const arma::uword d{grab_arg<arma::uword> (1, argc, argv, 5)};
    auto mod = model::make_unit_scaled<model::vec::Donut>(d, 0.9);
    do_mala(mod, argc, argv);
    return 0;
}
