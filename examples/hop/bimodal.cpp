#include "hop.hpp"
#include "mcmc_lib/models/Bimodal.hpp"
#include "mcmc_lib/models/AddGaussian.hpp"
#include "mcmc_lib/models/model-factories.hpp"

int main(int argc, char *argv[])
{
    const arma::uword d{grab_arg<arma::uword> (1, argc, argv, 5)};

    std::shared_ptr<model::vec::DrawableGradient> mod;

    if(d == 2){
	mod = model::make_unit_scaled<model::vec::Bimodal>(d);
    } else{
	mod = model::make_unit_scaled<model::vec::AddGaussian<model::vec::Bimodal>>(d);
    }

    do_hop(mod, argc, argv);
    return 0;
}
