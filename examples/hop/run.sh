#!/bin/bash
target=$1
if [ -z $target ]; then
    echo "must provide target as \$1"
    exit
fi

DIM=20
NITS=100000
BURN=0
SAVE=0
TAIL=0

## log2 scale
Lmin=-2
Lmax=6
Lstp=1

Kmin=-4
Kmax=2
Kstp=1

lambda=`Rscript --vanilla -e "cat(2^(seq($Lmin, $Lmax, $Lstp)))"`
kappa=`Rscript --vanilla -e "cat(2^(seq($Kmin, $Kmax, $Kstp)))"`

echo "lam=${lambda[@]}"
echo "kap=${kappa[@]}"

## get table header
if [[ ! (-a "hop_${target}_summary.tab") ]]
then
    echo "making the header"
    ./examples/hop/${target} 5 1 | head -n1 > hop_${target}_summary.tab;
fi

## run over a grid and get table body
for l in ${lambda[@]}; do
    for k in ${kappa[@]}; do
	./examples/hop/${target} ${DIM} ${NITS} ${BURN} ${SAVE} ${TAIL} $l $k | tail -n1 >> hop_${target}_summary.tab;
	echo $l $k
    done
done
