g <- function(x, a) -0.5 * (x/a)^2 - 0.5 * log(2 * a * pi)
f <- function(x) -0.25 * (x)^4 + 0.5 * log(2) - lgamma(0.25)
l <- function(x, a) g(x,a) + f(x)

## f and g are normalised
a <- 1
integrate(function(x) exp(f(x)), -10, 10)
integrate(function(x) exp(g(x, a)), -10, 10)

## where is the mass?
integrate(function(x) exp(f(x)), -2, 2)
integrate(function(x) exp(g(x, a)), -3*a, 3*a)

plot_for_a <- function(a){
    xgrid <- seq(-10, 10, 0.01)
    plot(xgrid, exp(l(xgrid, a)), type='l', col=1, main=paste("target a=",a), ylim=c(0,0.5))
    lines(xgrid, exp(g(xgrid, a)), type='l', col=2)
    lines(xgrid, exp(f(xgrid)), type='l', col=3)
    plot(xgrid, -l(xgrid, a), type='l', col=1, main=paste("potential a=",a))
    lines(xgrid, -g(xgrid, a), type='l', col=2)
    lines(xgrid, -f(xgrid), type='l', col=3)
}

par(mfcol=c(2,3))
plot_for_a(1)
plot_for_a(2)
plot_for_a(5)

leapfrog <- function(eps, a, sigma=1, plot=TRUE){
    ## draw a start point from N(0,sigma)
    p <- rnorm(1)
    x <- rnorm(1,0,sigma)
    y <- x + eps * p - (eps/a)^2 * x - eps^2 * x^3
    if(plot){
        plot(xgrid, exp(l(xgrid,a)), type='l', col=1, main=a, ylim=c(0,0.5))
        lines(xgrid, exp(g(xgrid,a)), type='l', col=2)
        lines(xgrid, exp(f(xgrid)), type='l', col=3)
        points(x, exp(l(x,a)))
        points(y, exp(l(y,a)))
        arrows(x, exp(l(x,a)), y, exp(l(y,a)))
        ## very crude approximation to show where mass is
        abline(v =  2 * tanh(a), col=4, lty=2)
        abline(v = -2 * tanh(a), col=4, lty=2)
    }
    c(x, y, exp(l(x,a)), exp(l(y,a)))
}

xgrid <- seq(-4,4,0.01)
par(mfcol=c(1,3))
leapfrog(1, 1, 1)
leapfrog(1, 2, 1)
leapfrog(1/5, 1/5, 1/5)

res <- matrix(NA, 1000, 4)
for(i in 1:nrow(res)){
    res[i,] <- leapfrog(1/5, 1/5, 1/5, FALSE)
}

res <- matrix(NA, 1000, 4)
for(i in 1:nrow(res)){
    res[i,] <- leapfrog(1, 1/5, 1/5, FALSE)
}

res <- matrix(NA, 1000, 4)
for(i in 1:nrow(res)){
    res[i,] <- leapfrog(0.35, 1/5, 1/5, FALSE)
}
pairs(res)
