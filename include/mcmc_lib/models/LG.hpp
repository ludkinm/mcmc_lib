#pragma once

#include <armadillo>
#include "mcmc_lib/models/Product.hpp"
#include "mcmc_lib/models/Normal.hpp"
#include "mcmc_lib/models/Logistic.hpp"
#include "mcmc_lib/debug.hpp"

namespace model{
namespace vec{

/**
 * A product of a logistic and a Normal/Gaussian
 * `alpha` is the scaling on the Gaussian relative to the Logistic.
 * All the models defined in the package take a vector of standard deviations -- in this case a tuning parameter `beta` is used to ensure that an LG-alpha has s.d. 1
 */
class LG: public product::DrawableHessian<matrices::DiagMatrix, matrices::DiagMatrix>{
private:
    using MyBase = typename product::DrawableHessian<matrices::DiagMatrix, matrices::DiagMatrix>;
protected:
    arma::vec sig; // target standard deviations
    double alpha;  // scaling for Normal relative to Logistic
public:
    LG(const arma::vec& s, double a, double b) : LG{arma::zeros(s.n_elem), s, a, b} {}
    LG(const arma::vec& mu, const arma::vec& s, double a, double b) :
    MyBase{std::make_shared<Logistic>(mu, b * s), std::make_shared<Normal>(mu, a * b * s)}, sig{b * s}, alpha{a}
    {
	name = "LG";
    }

    inline arma::vec draw() const override
    {
	DEBUG_COUT("draw LG" << alpha);
	/**
	 * rejection sampling using one part of the product as the proposal distribution
	 * when alpha > 1, use the logistic as the proposal
	 * when alpha <= 1, use the normal as the proposal
	 * x ~ mod1. accept as x~mod w.p. mod.pi(x)/mod1.pi(x)/M = mod0.pi(x)/M
	 * - in this case M = 1 since mod0.pi(x) is the Logitic part is maximised at x = mu with value 1;
	 */
	arma::vec x{ arma::zeros(this->dimension()) };
	for(arma::uword i = 0; i < x.n_elem; ++i){
	    // make models for this sig(i)
	    Logistic logistic{arma::vec{sig(i)}};
	    Normal normal{arma::vec{alpha * sig(i)}};
	    bool accepted;
	    double A;
	    do{
		if(alpha > 1){ // use logistic as proposal
		    x(i) = logistic.draw().at(0);
		    A = normal.logpi(arma::vec{x(i)});
		} else{
		    x(i) = normal.draw().at(0);
		    A = logistic.logpi(arma::vec{x(i)});
		}
		accepted = (log(arma::randu()) < A);
	    } while(!accepted);
	}
	return x;
    }

    inline arma::vec get_sig(void) const { return arma::vec(sig); }
    inline double get_alpha(void) const { return alpha; }
};

// @see Logistc
// @see LG
class LG1 : public LG{
public:
    LG1() = default;
    LG1(const arma::vec& sig) : LG{sig, 1.0, 1.50364469633394} { name = "LG1"; }
};

class LG2 : public LG{
public:
    LG2() = default;
    LG2(const arma::vec& sig) : LG{sig, 2.0, 1.16536681316058} { name = "LG2"; }
};

class LG5 : public LG{
public:
    LG5() = default;
    LG5(const arma::vec& sig) : LG{sig, 5.0, 1.03072164342430} { name = "LG5"; }
};

class LG8 : public LG{
public:
    LG8() = default;
    LG8(const arma::vec& sig) : LG{sig, 8.0, 1.01229151436345} { name = "LG8"; }
};

} // namespace vec
} // namespace model
