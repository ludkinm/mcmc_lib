#pragma once

#include <iostream>
#include <armadillo>
#include "mcmc_lib/bases/Model.hpp"
#include "mcmc_lib/models/Normal.hpp"
#include "mcmc_lib/models/Cat.hpp"
#include "mcmc_lib/util/Checks.hpp"
#include "mcmc_lib/debug.hpp"

namespace model{
namespace vec{

/** The Banana(\f$a,c,b\f$) target has a centred Normal distribution with scale \f$a\f$ in
 * the first dimension (so \f$X_0 \sim \operatorname{Normal}{0, a^2}\f$).
 * Letting \f$r = b*c/a^2 * sqrt(2)/2\f$ and \f$s^2 = c^2(1-b^2)\f$, then the second dimension, conditional on the first has distribution:
 * \f$ X_1|(X_0=x_0) \sim Normal{r(x_0^2 - a^2), s^2} \f$
 * The \f$b\f$ is referred to is "Bananicity"
 * as b->0, it approaches a MVN, as b -> 1 the banana becomes very thin and curved aking sampling hard.
 */
class Banana: public DrawableHessian<matrices::SquareMatrix>{
protected:
    double A, C, B, R, S;
public:
    Banana(const arma::vec& ac, double b=0.85) : Banana{ac(0), ac(1), b} {}
    explicit Banana(double a=1.0, double c=1.0, double b=0.85): A{a}, C{c}, B{b}, R{b*c*sqrt(2.0)/2.0/a/a}, S{c * sqrt(1.0 - b*b)}
    {
	check::sign(A);
	check::sign(B);
	check::sign(1-B);		// 1-B >= 0 \implies B <= 1
	check::sign(C);
	check::sign(R);
	check::sign(S);
	name = "Banana";
	DEBUG_COUT( *this );
    }

    inline arma::uword dimension(void) const override
    {
	return 2;
    }

    inline double logpi(const arma::vec& x) const override
    {
	double lp0 = x(0)/A;
	double lp1 = (x(1) - R * (x(0) - A) * (x(0) + A))/S;
	return -0.5*lp0*lp0 -0.5*lp1*lp1;
    }

    inline arma::vec gradient(const arma::vec& x) const override
    {
	double g1 = (x(1) - R * (x(0) - A) * (x(0) + A))/S/S;
	arma::vec g(2);
	g(1) = -g1;
	g(0) = -x(0)/A/A + 2.0 * R * x(0) * g1;
	return g;
    }

    inline matrices::SquareMatrix negative_hessian(const arma::vec& x) const override
    {
	// https://www.wolframalpha.com/input/?i=d2%2Fdx2+-0.5+*+%28x%2Fa%29%5E2+-+0.5+*+%28%28y-+r*%28x%5E2-a%5E2%29%29%2Fs%29%5E2
	arma::mat H(2, 2);
	H(0,0) = -1.0/pow(A, 2) + 2.0 * pow(R*A/S, 2) + R/S/S * (2 * x(1) - 6 * R * pow(x(0), 2));
	H(1,1) = -1.0/S/S;
	H(1,0) = H(0,1) = 2.0*R*x(0)/S/S;
	return matrices::SquareMatrix{-H};
    }

    inline std::ostream& operator<<(std::ostream& os) const override
    {
	return os << name << "(" << A << ", " << C << ", " << B << ")";
    }

    inline arma::vec draw() const override
    {
	DEBUG_COUT( "draw Banana" );
	arma::vec x = arma::randn(2) % arma::vec{A, S};
	DEBUG_COUT( "x = " << x.t() );
	x(1) += (R * (x(0) - A) * (x(0) + A));
	DEBUG_COUT( "x(1) = " << x(1) );
	DEBUG_COUT( "x = " << x.t() );
	return x;
    }
};

}
}
