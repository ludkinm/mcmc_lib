#pragma once

#include <armadillo>
#include "mcmc_lib/models/Cat.hpp"
#include "mcmc_lib/models/Normal.hpp"

namespace model{
namespace vec{

template<typename M>
class AddGaussian: public cat::DrawableHessian<typename M::HessianType, typename Normal::HessianType>{
private:
    using MyBase = typename cat::DrawableHessian<typename M::HessianType, typename Normal::HessianType>;
public:
    template<typename... MoreArgs>
    AddGaussian(const arma::vec& sig, const MoreArgs&... ma) : MyBase{std::make_shared<M>(sig(0), sig(1), std::forward<MoreArgs>(ma)...), std::make_shared<Normal>(sig.subvec(2, sig.n_elem-1))} {}
};

}
}
