#pragma once

#include <armadillo>
#include "mcmc_lib/models/Normal.hpp"
#include "mcmc_lib/models/Mixture.hpp"
#include "mcmc_lib/models/Cat.hpp"
#include "mcmc_lib/util/Checks.hpp"
#include "mcmc_lib/debug.hpp"

namespace model{
namespace vec{

/** The Bimodal(\f$a,b,\lambda\f$) target is an equal mixture of two
 *    bi-variate Normal distributions. Each component has the same
 *    covariance matrix:
 *    \f$(1-\lambda)\begin{pmatrix}  a^2 & 0\\ 0 & b^2 \end{pmatrix}\f$
 *    and have means:
 *    \f$ \mu_1 = \sqrt{\lambda}(a,b), \mu_2 = -\mu_1 \f$
 *    Thus, E[x] = 0
 *    \f$ Var[x] = \mu \mu^\top + \Sigma =\lambda\begin{pmatrix}  a^2 & ab\\ ab & b^2 \end{pmatrix} + (1-\lambda)\begin{pmatrix}  a^2 & 0\\ 0 & b^2 \end{pmatrix}\f$
 *    ie: lam=0 easy to sample, lam=1 hard to sample
 */

namespace bimodal{
arma::vec mu(const arma::vec& sds, double lam)
{
    check::sign(lam);
    return sqrt(lam) * sds;
}

arma::vec sig(const arma::vec& sds, double lam)
{
    check::sign(1-lam);
    return sqrt(1-lam) * sds;
}
}

class Bimodal: public vec::mixture::DrawableHessian<Normal::HessianType, Normal::HessianType>{
private:
    using vec::mixture::DrawableHessian<Normal::HessianType, Normal::HessianType>::DrawableHessian;
public:
    Bimodal(double a, double b, double lam=0.85) : Bimodal{arma::vec{a,b}, lam} {}

    Bimodal(const arma::vec& sds, double lam=0.85) : Bimodal{bimodal::mu(sds, lam), bimodal::sig(sds, lam)} {}

    Bimodal(const arma::vec& mu, const arma::vec& sig) : DrawableHessian{std::make_shared<Normal>(mu, sig), std::make_shared<Normal>(arma::vec(-mu), sig)}
    {
	check::sign(sig);
	name = "Bimodal";
    }
};

} // namespace vec
} // namespace model
