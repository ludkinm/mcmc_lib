#pragma once

#include <armadillo>
#include "mcmc_lib/models/LocationScale.hpp"
#include "mcmc_lib/debug.hpp"

namespace model{
namespace vec{

/**
 * The Normal distribution
 * A model in $\f$d\f$ dimensions, each dimension is an independent \f$\operatorname{Normal}(\mu_i, \sig_i)\f$.
 * Parameterized with location \f$\mu\f$ and *scale* \f$\sigma\f$
 * @see [https://en.wikipedia.org/wiki/Multivariate_normal_distribution]
 */
class Normal : public LocationScale{
private:
    matrices::DiagMatrix negHess;
public:
    Normal() = default;

    Normal(const arma::vec& _sig) : Normal{arma::zeros(_sig.n_elem), _sig} {}

    Normal(const arma::vec& _mu, const arma::vec& _sig) : LocationScale{_mu, _sig}
    {
	name = "Normal";
	negHess = matrices::DiagMatrix(1.0/arma::vec(this->sig)/arma::vec(this->sig));
	DEBUG_COUT(*this);
    }

    inline double logpi(const arma::vec& x) const override
    {
	return -0.5 * arma::dot(rescale(x), rescale(x));
    }

    inline arma::vec gradient(const arma::vec& x) const override
    {
	return this->sig.solve(-rescale(x));
    }

    inline matrices::DiagMatrix negative_hessian([[maybe_unused]] const arma::vec& x) const override
    {
	return negHess;
    }

    inline arma::vec draw() const override
    {
	DEBUG_COUT( "draw Normal" );
	return mu + sig * arma::randn(dimension());
    }

};

} // namespace vec
} // namespace model
