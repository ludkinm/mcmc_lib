#pragma once

#include <armadillo>
#include "mcmc_lib/models/LocationScale.hpp"
#include "mcmc_lib/debug.hpp"

namespace model{
namespace vec{

/**
 * Distribution with density proportional to exp(-x^4/4)
 * with location and scale (\mu, \rho)
 * the normalising constant is \rho\Gamma(1/4) \sqrt(2)/2
 * we take the mean \mu and standard deviation \sig as argument
 * Note that \mu is indeed the mean, however for \sig:
 * \int \frac{\exp(-((x-\mu)/\rho)^4}{\rho\Gamma(1/4) \sqrt(2)/2} dx
 * = 2\rho^2\Gamm(3/4)/\Gamma(1/4)
 * Hence, letting \rho = \frac{\sig\sqrt(\Gamma(1/4)}{\sqrt( 2\Gamma(3/4))}
 * @see[https://www.wolframalpha.com/input/?i=integrate+x%5E2+exp(-(x%2Fa)%5E4%2F4)%2F(a*sqrt(2)*Gamm(1%2F4)%2F2)+from+x+%3D+-inf+to+inf+at+a+%3D3+*+sqrt(Gamma(1%2F4)%2F2%2FGamma(3%2F4))]
 */
class Quartic : public LocationScale{
public:
    // @see[https://www.wolframalpha.com/input/?i=sqrt(Gamma(1%2F4)%2F2%2FGamma(3%2F4))]
    constexpr static double scaling = 1.216280214257520283105211305;

    Quartic(const arma::vec& _sig) : Quartic{arma::zeros(_sig.n_elem), _sig} {}


    Quartic(const arma::vec& _mu, const arma::vec& _sig) : LocationScale{_mu, scaling * _sig}
    {
	name = "Quartic";
	DEBUG_COUT(*this);
    }

    inline double logpi(const arma::vec& x) const override
    {
	return arma::accu(-0.25 * pow(this->rescale(x), 4));
    }

    inline arma::vec gradient(const arma::vec& x) const override
    {
	return -this->sig.solve(pow(this->rescale(x), 3));
    }

    inline matrices::DiagMatrix negative_hessian(const arma::vec& x) const override
    {
	return matrices::DiagMatrix(3.0 * arma::square(this->rescale(x)/arma::vec(this->sig)));
    }

    inline arma::vec draw() const override
    {
	DEBUG_COUT( "draw Quartic" );
	/**
	 * mu is a location so can add at the end
	 * let f = Quartic(mu, sig)
	 * and g = N(mu, sig)
	 * let z ~ g and x = (z-mu)/sig
	 * So f(x)/Mg(x) = exp([0.5*x^2] * (1.0 - [0.5*x^2]))/M
	 * y(1-y) is maxed at y=0.5 so 0.5*x^2=0.5
	 * => x = +/- 1 => M = exp(0.25)
	 * => A = f(x)/Mg(x) = exp([0.5*x^2] * (1.0 - [0.5*x^2]) - 0.25)
	 */
	const arma::uword d = mu.n_elem;
	arma::vec x = arma::zeros(d);
	double a = 0.0;
	bool try_again = true;
	for(arma::uword i = 0; i < d; ++i){
	    try_again = true;
	    while(try_again){
		x(i) = arma::randn(); // draw from normal(mu, sig^2)
		DEBUG_COUT( "x(" << i << ") = " << x(i) );
		a = 0.5 * pow(x(i), 2);
		a = exp(a * (1.0-a) - 0.25);
		DEBUG_COUT("a = " << a );
		if(arma::randu() < a)
		    try_again = false;
	    }
	}
	DEBUG( x.t().raw_print("x") );
	x = sig * x;
	DEBUG( x.t().raw_print("x*sig") );
	x += mu;
	DEBUG( x.t().raw_print("mu + x*sig") );
	return x;
    }

};

} // namespace vec
} // namespace model
