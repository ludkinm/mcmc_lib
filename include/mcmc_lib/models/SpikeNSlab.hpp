#pragma once

#include <armadillo>
#include "mcmc_lib/models/Normal.hpp"
#include "mcmc_lib/models/Mixture.hpp"
#include "mcmc_lib/models/Cat.hpp"
#include "mcmc_lib/debug.hpp"

namespace model{
namespace vec{

/*** SpikeNSlab
 *   X \sim 1/2 MVN(0, sig0) + 1/2 MVN(0, sig1)
 *   with:
 *   sig0 = sqrt(1+lam) * sig
 *   sig1 = sqrt(1-lam) * sig
 *   Note: E[X] = 0 & Var[x] = sig
 *   lam=0 easy => sig0 = sig1 = sig and X~N(0,sig)
 *   lam->1 hard => sqrt(2) * sig0; sig1 = 0; so mixture of 0.5 N(0, 2 sig^2) + 0.5 Dirac_0
 */
class SpikeNSlab: public vec::mixture::DrawableHessian<Normal::HessianType, Normal::HessianType>{
private:
    using vec::mixture::DrawableHessian<Normal::HessianType, Normal::HessianType>::DrawableHessian;
public:
    SpikeNSlab(double a, double b, double lam=0.85) : SpikeNSlab{arma::vec{a,b}, lam} {}

    SpikeNSlab(const arma::vec& sig, double lam=0.85) : DrawableHessian{std::make_shared<Normal>(sqrt(1.0+lam) * sig), std::make_shared<Normal>(sqrt(1.0-lam) * sig)}
    {
	check::sign(lam);
	check::sign(1.0-lam);
	name = "SpikeNSlab";
    }
};

} // namespace vec
} // namespace model
