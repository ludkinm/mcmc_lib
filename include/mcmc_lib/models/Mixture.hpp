#pragma once

#include <memory>
#include <armadillo>
#include "mcmc_lib/util/SquareMatrices.hpp"
#include "mcmc_lib/bases/Model.hpp"
#include "mcmc_lib/debug.hpp"

namespace model{
namespace vec{
namespace mixture{

/**
 * A mixture of two logpi models
 */
template<typename M0, typename M1=M0>
class Mixture: public virtual base::Model{
protected:
    using ModPtr0 = std::shared_ptr<M0>;
    using ModPtr1 = std::shared_ptr<M1>;
    ModPtr0 mod0;
    ModPtr1 mod1;
    arma::vec2 weights;
    arma::vec2 logweights;
    arma::uword d;
    inline double pi0(const arma::vec& x) const { return exp(logweights(0) + mod0->logpi(x)); }
    inline double pi1(const arma::vec& x) const { return exp(logweights(1) + mod1->logpi(x)); }
public:
    Mixture(const ModPtr0& m0, const ModPtr1& m1) : Mixture{m0, m1, arma::vec2{0.5,0.5}} {}
    Mixture(const ModPtr0& m0, const ModPtr1& m1, const arma::vec& w) : mod0{m0}, mod1{m1},
									weights{w}, logweights{log(weights)},
									d{mod0->dimension()}
    {
	if(w.n_elem != 2)
	    throw std::logic_error("model::vec::mixture construction: w must be two elements!");
	if(mod0->dimension() != mod1->dimension())
	    throw std::logic_error("model::vec::mixture construction: models must have same dimension!");
	name = "Mixture" + m0->name + m1->name;
    }
    inline arma::uword dimension(void) const override { return d; }
};

/**
 * A mixture of two logpi models
 */
class Logpi: public Mixture<vec::Logpi>, public virtual vec::Logpi{
public:
    using Mixture<vec::Logpi>::Mixture;
    inline double logpi(const arma::vec& x) const override { return log(pi0(x) + pi1(x)); }
};

/**
 * A mixture of two drawable logpi models
 */
class DrawableLogpi: public Mixture<model::vec::DrawableLogpi>, public virtual vec::DrawableLogpi{
public:
    using Mixture<vec::DrawableLogpi>::Mixture;
    inline double logpi(const arma::vec& x) const override { return log(pi0(x) + pi1(x)); }
    inline arma::vec draw(void) const override
    {
	if(arma::randu() < weights(0)){
	    return mod0->draw();
	} else{
	    return mod1->draw();
	}
    }
};

/**
 * A mixture of two gradient models
 */
class Gradient: public Mixture<vec::Gradient>, public virtual vec::Gradient{
public:
    using Mixture<vec::Gradient>::Mixture;
    inline double logpi(const arma::vec& x) const override { return log(pi0(x) + pi1(x)); }
    inline arma::vec gradient(const arma::vec& x) const override { return (pi0(x) * mod0->gradient(x) + pi1(x) * mod1->gradient(x))/exp(logpi(x)); }
};

/**
 * A mixture of two drawable gradient models
 */
class DrawableGradient: public Mixture<model::vec::DrawableGradient>, public virtual vec::DrawableGradient{
public:
    using Mixture<vec::DrawableGradient>::Mixture;
    inline double logpi(const arma::vec& x) const override { return log(pi0(x) + pi1(x)); }
    inline arma::vec gradient(const arma::vec& x) const override { return (pi0(x) * mod0->gradient(x) + pi1(x) * mod1->gradient(x))/exp(logpi(x)); }
    inline arma::vec draw(void) const override
    {
	if(arma::randu() < weights(0)){
	    return mod0->draw();
	} else{
	    return mod1->draw();
	}
    }
};

/**
 * A mixture of two Hessian models - the returned hessian is Square in general since the hessian involves a sum of outer products
 */
template<typename H0, typename H1>
class Hessian: public Mixture<vec::Hessian<H0>, vec::Hessian<H1>>, public virtual vec::Hessian<matrices::SquareMatrix>{
public:
    using Mixture<vec::Hessian<H0>, vec::Hessian<H1>>::Mixture;
    inline double logpi(const arma::vec& x) const override { return log(this->pi0(x) + this->pi1(x)); }
    inline arma::vec gradient(const arma::vec& x) const override { return (this->pi0(x) * this->mod0->gradient(x) + this->pi1(x) * this->mod1->gradient(x))/exp(logpi(x)); }
    inline matrices::SquareMatrix negative_hessian(const arma::vec& x) const override
    {
	arma::vec g{this->gradient(x)};
	arma::vec g0{this->mod0->gradient(x)};
	arma::vec g1{this->mod1->gradient(x)};
	arma::mat h0{this->mod0->negative_hessian(x)};
	arma::mat h1{this->mod1->negative_hessian(x)};
	return matrices::SquareMatrix{(this->pi0(x) * (h0 - g0 * g0.t()) + this->pi1(x) * (h1 - g1 * g1.t()))/exp(logpi(x)) + g * g.t()};
    }
};

template<typename M0, typename M1>
Hessian(std::shared_ptr<M0> x, std::shared_ptr<M1> y) -> Hessian<typename M0::HessianType, typename M1::HessianType>;

// uses
/**
 * A mixture of two drawable Hessian models - the returned hessian is Square in general since the hessian involves a sum of outer products
 */
template<typename H0, typename H1>
class DrawableHessian: public Mixture<vec::DrawableHessian<H0>, vec::DrawableHessian<H1>>, public virtual vec::DrawableHessian<matrices::SquareMatrix>{
public:
    using Mixture<vec::DrawableHessian<H0>, vec::DrawableHessian<H1>>::Mixture;
    inline double logpi(const arma::vec& x) const override { return log(this->pi0(x) + this->pi1(x)); }
    inline arma::vec gradient(const arma::vec& x) const override { return (this->pi0(x) * this->mod0->gradient(x) + this->pi1(x) * this->mod1->gradient(x))/exp(logpi(x)); }
    inline matrices::SquareMatrix negative_hessian(const arma::vec& x) const override
    {
	arma::vec g{this->gradient(x)};
	arma::vec g0{this->mod0->gradient(x)};
	arma::vec g1{this->mod1->gradient(x)};
	arma::mat h0{this->mod0->negative_hessian(x)};
	arma::mat h1{this->mod1->negative_hessian(x)};
	return matrices::SquareMatrix{(this->pi0(x) * (h0 - g0 * g0.t()) + this->pi1(x) * (h1 - g1 * g1.t()))/exp(logpi(x)) + g * g.t()};
    }
    inline arma::vec draw(void) const override
    {
	if(arma::randu() < this->weights(0)){
	    return this->mod0->draw();
	} else{
	    return this->mod1->draw();
	}
    }
};

template<typename M0, typename M1>
DrawableHessian(std::shared_ptr<M0> x, std::shared_ptr<M1> y) -> DrawableHessian<typename M0::HessianType, typename M1::HessianType>;

} // namespace mixture
} // namespace vec
} // namespace model
