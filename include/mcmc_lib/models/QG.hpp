#pragma once

#include <armadillo>
#include "mcmc_lib/models/Product.hpp"
#include "mcmc_lib/models/Normal.hpp"
#include "mcmc_lib/models/Quartic.hpp"
#include "mcmc_lib/debug.hpp"

namespace model{
namespace vec{

/**
 * A product of a quartic and a Normal/Gaussian
 * `alpha` is the scaling on the Gaussian relative to the Quartic.
 * All the models defined in the package take a vector of standard deviations -- in this case a tuning parameter `beta` is used to ensure that an LG-alpha has s.d. 1
 */
class QG: public product::DrawableHessian<matrices::DiagMatrix, matrices::DiagMatrix>{
private:
    using MyBase = typename product::DrawableHessian<matrices::DiagMatrix, matrices::DiagMatrix>;
protected:
    arma::vec sig; // target standard deviations
    double alpha;  // scaling for Normal relative to Quartic
public:
    QG(const arma::vec& s, double a, double b) : QG{arma::zeros(s.n_elem), s, a, b} {}
    QG(const arma::vec& mu, const arma::vec& s, double a, double b) :
    MyBase{std::make_shared<Quartic>(mu, b * s), std::make_shared<Normal>(mu, a * b * s)}, sig{b * s}, alpha{a}
    {
	name = "QG";
    }

    arma::vec draw() const override
    {
	DEBUG_COUT( "draw LG" << alpha );
	/**
	 * rejection sampling using one part of the product as the proposal distribution
	 * when alpha > 1, use the quartic as the proposal
	 * when alpha <= 1, use the normal as the proposal
	 * x ~ mod1. accept as x~mod w.p. mod.pi(x)/mod1.pi(x)/M = mod0.pi(x)/M
	 * - in this case M = 1 since mod0.pi(x) is the Logitic part is maximised at x = mu with value 1;
	 */
	arma::vec x{ arma::zeros(this->dimension())}; // blank vector
	for(arma::uword i = 0; i < x.n_elem; ++i){
	    // make models for this sig(i)
	    Quartic quartic{arma::vec{sig(i)}};
	    Normal normal{arma::vec{alpha * sig(i)}};
	    bool accepted;
	    double A;
	    do{
		if(alpha > 1){ // use quartic as proposal
		    x(i) = quartic.draw().at(0);
		    A = normal.logpi(arma::vec{x(i)});
		} else{
		    x(i) = normal.draw().at(0);
		    A = quartic.logpi(arma::vec{x(i)});
		}
		accepted = (log(arma::randu()) < A);
	    } while(!accepted);
	}
	return x;
    }
};


/**
 * found beta values by sampling
 * @see Quartic
 * @see QG
 */
class QG1 : public QG{
public:
    QG1() = default;
    QG1(const arma::vec& sig) : QG{sig, 1.0, 1.29761384800146} { name = "QG1"; }
};

/**
 * found beta values by sampling
 * @see Quartic
 * @see QG
 */
class QG3 : public QG{
public:
    QG3() = default;
    QG3(const arma::vec& sig) : QG{sig, 3.0, 1.03308882908853} { name = "QG3"; }
};


} // namespace vec
} // namespace model
