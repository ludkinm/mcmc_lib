#pragma once

#include <memory>
#include <armadillo>
#include "mcmc_lib/bases/Model.hpp"
#include "mcmc_lib/util/SquareMatrices.hpp"
#include "mcmc_lib/debug.hpp"

namespace model{
namespace vec{
namespace product{

/**
 * A model of the form \pi(x) = \pi_0(x)\pi_1(x)
 */
template<typename M0, typename M1=M0>
class Product: public virtual base::Model{
protected:
    using ModPtr0 = std::shared_ptr<M0>;
    using ModPtr1 = std::shared_ptr<M1>;
    ModPtr0 mod0;
    ModPtr1 mod1;
    arma::vec2 weights;
    arma::vec2 logweights;
    arma::uword d;
public:
    Product(const ModPtr0& m0, const ModPtr1& m1) : mod0{m0}, mod1{m1}, d{mod0->dimension()}
    {
	if(mod0->dimension() != mod1->dimension())
	    throw std::logic_error("model::vec::product construction: models must have same dimension!");
	name = "Product" + m0->name + m1->name;
    }
    inline arma::uword dimension(void) const override { return d; }
};

/**
 * A product of two logpi models
 */
class Logpi: public Product<vec::Logpi>, public virtual vec::Logpi{
public:
    using Product<vec::Logpi>::Product;
    inline double logpi(const arma::vec& x) const override { return mod0->logpi(x) + mod1->logpi(x); }
};

/**
 * A product of two drawable logpi models
 */
class DrawableLogpi: public Product<model::vec::DrawableLogpi>, public virtual vec::DrawableLogpi{
public:
    using Product<vec::DrawableLogpi>::Product;
    inline double logpi(const arma::vec& x) const override { return mod0->logpi(x) + mod1->logpi(x); }
    using vec::DrawableLogpi::draw; // still to be overriden
};

/**
 * A product of two gradient models
 */
class Gradient: public Product<vec::Gradient>, public virtual vec::Gradient{
public:
    using Product<vec::Gradient>::Product;
    inline double logpi(const arma::vec& x) const override { return mod0->logpi(x) + mod1->logpi(x); }
    inline arma::vec gradient(const arma::vec& x) const override { return mod0->gradient(x) + mod1->gradient(x); }
};

/**
 * A product of two drawable gradient models
 */
class DrawableGradient: public Product<model::vec::DrawableGradient>, public virtual vec::DrawableGradient{
public:
    using Product<vec::DrawableGradient>::Product;
    inline double logpi(const arma::vec& x) const override { return mod0->logpi(x) + mod1->logpi(x); }
    inline arma::vec gradient(const arma::vec& x) const override { return mod0->gradient(x) + mod1->gradient(x); }
    using vec::DrawableGradient::draw; // still to be overriden
};


template<typename H0, typename H1>
using diag_or_sqaure = typename std::conditional_t<matrices::is_diagonal_v<H0> & matrices::is_diagonal_v<H1>, matrices::DiagMatrix, matrices::SquareMatrix>;

/**
 * A product of two Hessian models - the returned hessian is Diagonal if both models have a diagonal hessian
 */
template<typename H0, typename H1>
class Hessian: public Product<vec::Hessian<H0>, vec::Hessian<H1>>,
	       public virtual vec::Hessian<diag_or_sqaure<H0,H1>>{
public:
    using Product<vec::Hessian<H0>, vec::Hessian<H1>>::Product;
    inline double logpi(const arma::vec& x) const override { return this->mod0->logpi(x) + this->mod1->logpi(x); }
    inline arma::vec gradient(const arma::vec& x) const override { return this->mod0->gradient(x) + this->mod1->gradient(x); }
    inline diag_or_sqaure<H0,H1> negative_hessian(const arma::vec& x) const override { return this->mod0->negative_hessian(x) + this->mod1->negative_hessian(x); }
};

template<typename M0, typename M1>
Hessian(std::shared_ptr<M0> x, std::shared_ptr<M1> y) -> Hessian<typename M0::HessianType, typename M1::HessianType>;

/**
 * A product of two drawable Hessian models - the returned hessian is Diagonal if both models have a diagonal hessian
 */

template<typename H0, typename H1>
class DrawableHessian: public Product<vec::DrawableHessian<H0>, vec::DrawableHessian<H1>>,
	       public virtual vec::DrawableHessian<diag_or_sqaure<H0,H1>>{
public:
    using Product<vec::DrawableHessian<H0>, vec::DrawableHessian<H1>>::Product;
    inline double logpi(const arma::vec& x) const override { return this->mod0->logpi(x) + this->mod1->logpi(x); }
    inline arma::vec gradient(const arma::vec& x) const override { return this->mod0->gradient(x) + this->mod1->gradient(x); }
    inline diag_or_sqaure<H0,H1> negative_hessian(const arma::vec& x) const override { return this->mod0->negative_hessian(x) + this->mod1->negative_hessian(x); }
    using vec::DrawableHessian<diag_or_sqaure<H0,H1>>::draw; // still to be overriden
};

template<typename M0, typename M1>
DrawableHessian(std::shared_ptr<M0> x, std::shared_ptr<M1> y) -> DrawableHessian<typename M0::HessianType, typename M1::HessianType>;

} // namespace product
} // namespace vec
} // namespace model
