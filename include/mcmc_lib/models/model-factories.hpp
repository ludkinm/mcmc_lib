#pragma once

#include <armadillo>
#include <memory>
#include "mcmc_lib/bases/Model.hpp"

namespace model{

/**
 * All models provided with the library have a constructor taking a vector of standard deviations (sd.s) as first argument.
 * These factory methods make a model with unit sd.s or increasing sd.s
 */

template<typename M, class... Args>
M unit_scaled(const arma::uword& d, Args&&... args)
{
    M m{arma::ones(d), std::forward<Args>(args)...};
    m.name = "Unit" + m.name;
    return m;
}

template<typename M, class... Args>
M linear_increase_scaled(const arma::uword& d, double max, Args&&... args){
    M m{arma::linspace(1.0, max, d), std::forward<Args>(args)...};
    m.name = "LinearInc" + m.name;
    return m;
}


template<typename M, class... Args>
M linear_decrease_scaled(const arma::uword& d, double max, Args&&... args){
    M m{arma::linspace(max, 1.0, d), std::forward<Args>(args)...};
    m.name = "LinearDec" + m.name;
    return m;
}


template<typename M, class... Args>
std::shared_ptr<M> make_unit_scaled(const arma::uword& d, Args&&... args)
{
    return std::make_shared<M>(unit_scaled<M>(d, args...));
}

template<typename M, class... Args>
std::shared_ptr<M> make_linear_increase_scaled(const arma::uword& d, double max, Args&&... args)
{
    return std::make_shared<M>(linear_increase_scaled<M>(d, max, args...));
}

template<typename M, class... Args>
std::shared_ptr<M> make_linear_decrease_scaled(const arma::uword& d, double max, Args&&... args)
{
    return std::make_shared<M>(linear_decrease_scaled<M>(d, max, args...));
}

}
