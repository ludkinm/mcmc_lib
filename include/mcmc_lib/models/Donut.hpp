#pragma once

#include <armadillo>
#include "mcmc_lib/bases/Model.hpp"
#include "mcmc_lib/models/Bimodal.hpp"
#include "mcmc_lib/util/Checks.hpp"
#include "mcmc_lib/debug.hpp"

namespace model{
namespace vec{

/**
 * Donut distribution \f$ x^T \Sigma x ~ 1/2 N(mu, sig) + 1/2 N(-mu, sig) \f$
 */
class Donut: public DrawableGradient{
protected:
    arma::vec sigma;
    Bimodal mod_for_norm;
public:
    explicit Donut(const arma::vec& s, double lam=0.8) :
    sigma{s}, mod_for_norm{arma::vec{1.0}, lam}
    {
	name = "Donut";
	DEBUG_COUT(*this);
    }

    inline arma::uword dimension() const override { return sigma.n_elem; }

    inline double logpi(const arma::vec& x) const override { return mod_for_norm.logpi(arma::vec{arma::norm(x/sigma)}); }

    inline arma::vec gradient(const arma::vec& x) const override
    {
	double r{ arma::norm(x/sigma) };
	double g{ mod_for_norm.gradient(arma::vec{r}).at(0) }; // g is a 1 element vector
	return g/r * x/sigma;
    }

    inline arma::vec draw() const override
    {
	double R{ mod_for_norm.draw().at(0) };
	arma::vec x{ arma::randn(dimension()) };
	x *= R/norm(x);
	DEBUG_COUT("x=" << x);
	return x;
    }
};

} // namespace vec
} // namespace model
