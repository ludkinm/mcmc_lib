#pragma once

#include <armadillo>
#include "mcmc_lib/models/LocationScale.hpp"
#include "mcmc_lib/debug.hpp"

namespace model{
namespace vec{

/**
 * The Logistic distribution
 * A model in $\f$d\f$ dimensions, each dimension is an independent \f$\operatorname{Logistic}(\mu_i, \sig_i)\f$.
 * Parameterized with location \f$\mu\f$ and *scale* \f$\sigma\f$
 * @see [https://en.wikipedia.org/wiki/Logistic_distribution]
 */
class Logistic : public LocationScale{
public:
    constexpr static double scaling = 0.551328895421792049511326498312969441397386480366640652799; // = sqrt(3.0)/arma::datum::pi;

    Logistic(const arma::vec& _sig) : Logistic{arma::zeros(_sig.n_elem), _sig} {}

    Logistic(const arma::vec& _mu, const arma::vec& _sig) : LocationScale{_mu, scaling * _sig}
    {
	name = "Logistic";
	DEBUG_COUT(*this);
    }

    inline double logpi(const arma::vec& x) const override
    {
	return arma::accu(-2.0 * log(cosh(0.5 * rescale(x))));
    }

    inline arma::vec gradient(const arma::vec& x) const override
    {
	return this->sig.solve(-tanh(0.5 * rescale(x)));
    }

    inline matrices::DiagMatrix negative_hessian(const arma::vec& x) const override
    {
	return matrices::DiagMatrix(0.5 * pow(this->sig * cosh(0.5 * this->rescale(x)), -2));
    }

    inline arma::vec draw() const override
    {
	DEBUG_COUT( "draw Logistic" );
	arma::vec x = arma::randu(mu.n_elem);
	x = mu + sig * (log(x) - log(1.0-x));
	return x;
    }
};

} // namespace vec
} // namespace model
