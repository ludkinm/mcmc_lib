#pragma once

#include <iostream>
#include <armadillo>
#include "mcmc_lib/bases/Model.hpp"
#include "mcmc_lib/util/SquareMatrices.hpp"
#include "mcmc_lib/util/Checks.hpp"
#include "mcmc_lib/debug.hpp"

namespace model{

class LocationScale : public vec::DrawableHessian<matrices::DiagMatrix>{
protected:
    arma::vec mu;
    matrices::DiagMatrix sig;
    arma::vec rescale(const arma::vec& x) const { return sig.solve(x-mu); }
public:

    LocationScale(const arma::vec& _sig) : LocationScale{arma::zeros(_sig.n_elem), _sig} {}

    LocationScale(const arma::vec& _mu, const arma::vec& _sig) : mu{_mu}, sig{_sig}
    {
	if( any(arma::sign(_sig) != 1) )
	    throw std::logic_error("All sigmas should be positive");
	if(mu.n_elem != sig.dimension())
	    throw std::logic_error("mu and sigma are not the same length");
    }

    inline arma::uword dimension(void) const override { return mu.n_elem; }

    inline arma::vec get_mu(void) const { return mu; }

    inline arma::vec get_sig(void) const { return arma::vec(sig); }

    virtual std::ostream& operator<<(std::ostream& os) const override
    {
	return os << name;
    }
};

}
