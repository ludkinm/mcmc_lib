#pragma once

#include <armadillo>
#include <memory>
#include "mcmc_lib/util/SquareMatrices.hpp"
#include "mcmc_lib/bases/Model.hpp"
#include "mcmc_lib/debug.hpp"

namespace model{
namespace vec{
namespace cat{

/**
 * A conCATenation of two models
 */
template<typename M0, typename M1=M0>
class Cat: public virtual base::Model{
protected:
    using ModPtr0 = std::shared_ptr<M0>;
    using ModPtr1 = std::shared_ptr<M1>;
    ModPtr0 mod0;
    ModPtr1 mod1;
    arma::uword s;
    arma::uword d;
public:
    Cat(const ModPtr0& m0, const ModPtr1& m1) : mod0{m0}, mod1{m1}, s{m0->dimension()}, d{mod0->dimension() + mod1->dimension()}
    {
	name = "Cat" + m0->name + m1->name;
    }
    inline arma::uword dimension(void) const override { return d; }
};

/**
 * A logpi model made by conCATenating two Logpi models
 */
class Logpi: public Cat<vec::Logpi>, public virtual vec::Logpi{
public:
    using Cat<vec::Logpi>::Cat;
    inline double logpi(const arma::vec& x) const override { return logpi(x.subvec(0, s-1), x.subvec(s, d-1)); }
    inline double logpi(const arma::vec& x, const arma::vec& y) const { return mod0->logpi(x) + mod1->logpi(y); }
};

/**
 * A drawable logpi model made by conCATenating two drawable Logpi models
 */
class DrawableLogpi: public Cat<vec::DrawableLogpi>, public virtual vec::DrawableLogpi{
public:
    using Cat<vec::DrawableLogpi>::Cat;
    inline double logpi(const arma::vec& x) const override { return logpi(x.subvec(0, s-1), x.subvec(s, d-1)); }
    inline double logpi(const arma::vec& x, const arma::vec& y) const { return mod0->logpi(x) + mod1->logpi(y); }
    inline arma::vec draw(void) const { return arma::join_vert(mod0->draw(), mod1->draw()); }
};


/**
 * A gradient model made by conCATenating two Gradient models
 */
class Gradient: public Cat<vec::Gradient>, public virtual vec::Gradient{
public:
    using Cat<vec::Gradient>::Cat;
    inline double logpi(const arma::vec& x) const override { return logpi(x.subvec(0, s-1), x.subvec(s, d-1)); }
    inline double logpi(const arma::vec& x, const arma::vec& y) const { return mod0->logpi(x) + mod1->logpi(y); }
    inline arma::vec gradient(const arma::vec& x) const override { return gradient(x.subvec(0, s-1), x.subvec(s, d-1)); }
    inline arma::vec gradient(const arma::vec& x, const arma::vec& y) const { return arma::join_vert(mod0->gradient(x), mod1->gradient(y)); }
};

/**
 * A drawable gradient model made by conCATenating two drawable Gradient models
 */
class DrawableGradient: public Cat<vec::DrawableGradient>, public virtual vec::DrawableGradient{
public:
    using Cat<vec::DrawableGradient>::Cat;
    inline double logpi(const arma::vec& x) const override { return logpi(x.subvec(0, s-1), x.subvec(s, d-1)); }
    inline double logpi(const arma::vec& x, const arma::vec& y) const { return mod0->logpi(x) + mod1->logpi(y); }
    inline arma::vec gradient(const arma::vec& x) const override { return gradient(x.subvec(0, s-1), x.subvec(s, d-1)); }
    inline arma::vec gradient(const arma::vec& x, const arma::vec& y) const { return arma::join_vert(mod0->gradient(x), mod1->gradient(y)); }
    inline arma::vec draw(void) const { return arma::join_vert(mod0->draw(), mod1->draw()); }
};

/**
 * A Hessian model made by conCATenating two Hessian models - the hessian is a BlockMatrix
 */
template<typename H0, typename H1>
class Hessian: public Cat<vec::Hessian<H0>, vec::Hessian<H1>>, public virtual vec::Hessian<matrices::BlockMatrix<H0,H1>>{
public:
    using Cat<vec::Hessian<H0>, vec::Hessian<H1>>::Cat;
    using typename vec::Hessian<matrices::BlockMatrix<H0,H1>>::HessianType;
    inline double logpi(const arma::vec& x) const override { return logpi(x.subvec(0, this->s-1), x.subvec(this->s, this->d-1)); }
    inline double logpi(const arma::vec& x, const arma::vec& y) const { return this->mod0->logpi(x) + this->mod1->logpi(y); }
    inline arma::vec gradient(const arma::vec& x) const override { return gradient(x.subvec(0, this->s-1), x.subvec(this->s, this->d-1)); }
    inline arma::vec gradient(const arma::vec& x, const arma::vec& y) const { return arma::join_vert(this->mod0->gradient(x), this->mod1->gradient(y)); }
    inline HessianType negative_hessian(const arma::vec& x) const override { return negative_hessian(x.subvec(0, this->s-1), x.subvec(this->s, this->d-1)); }
    inline HessianType negative_hessian(const arma::vec& x, const arma::vec& y) const { return {this->mod0->negative_hessian(x), this->mod1->negative_hessian(y)}; }
};

template<typename M0, typename M1>
Hessian(std::shared_ptr<M0> x, std::shared_ptr<M1> y) -> Hessian<typename M0::HessianType, typename M1::HessianType>;

/**
 * A DrawableHessian model made by conCATenating two DrawableHessian models - the hessian is a BlockMatrix
 */
template<typename H0, typename H1>
class DrawableHessian: public Cat<vec::DrawableHessian<H0>, vec::DrawableHessian<H1>>, public virtual vec::DrawableHessian<matrices::BlockMatrix<H0,H1>>{
public:
    using Cat<vec::DrawableHessian<H0>, vec::DrawableHessian<H1>>::Cat;
    using typename vec::DrawableHessian<matrices::BlockMatrix<H0,H1>>::HessianType;
    inline double logpi(const arma::vec& x) const override { return logpi(x.subvec(0, this->s-1), x.subvec(this->s, this->d-1)); }
    inline double logpi(const arma::vec& x, const arma::vec& y) const { return this->mod0->logpi(x) + this->mod1->logpi(y); }
    inline arma::vec gradient(const arma::vec& x) const override { return gradient(x.subvec(0, this->s-1), x.subvec(this->s, this->d-1)); }
    inline arma::vec gradient(const arma::vec& x, const arma::vec& y) const { return arma::join_vert(this->mod0->gradient(x), this->mod1->gradient(y)); }
    inline HessianType negative_hessian(const arma::vec& x) const override { return negative_hessian(x.subvec(0, this->s-1), x.subvec(this->s, this->d-1)); }
    inline HessianType negative_hessian(const arma::vec& x, const arma::vec& y) const { return {this->mod0->negative_hessian(x), this->mod1->negative_hessian(y)}; }
    inline arma::vec draw(void) const { return arma::join_vert(this->mod0->draw(), this->mod1->draw()); }
};

template<typename M0, typename M1>
DrawableHessian(std::shared_ptr<M0> x, std::shared_ptr<M1> y) -> DrawableHessian<typename M0::DrawableHessianType, typename M1::DrawableHessianType>;

} // namespace cat
} // namespace vec
} // namespace model
