#pragma once

#include <armadillo>
#include "mcmc_lib/bases/Model.hpp"
#include "mcmc_lib/util/SquareMatrices.hpp"
#include "mcmc_lib/debug.hpp"
#include "mcmc_lib/util/Checks.hpp"

namespace model{
/**
 * A multivariate normal distribution using a *Precision matrix* the inverse of the covariance.
 */
class MVT: public vec::DrawableHessian<matrices::EigenMatrix>{
private:
    using MyBase = vec::DrawableHessian<matrices::EigenMatrix>;
protected:
    arma::vec mu;
    matrices::EigenMatrix precision;
    double nu;
public:
    using MyBase::XType;
    using MyBase::GradientType;
    using MyBase::HessianType;
    MVT() = default;
    MVT(const arma::vec& s, double n) : MVT{arma::zeros(s.n_rows), arma::diagmat(s), n} {} // A diagonal matrix
    MVT(const arma::vec& m, const arma::mat& s, double n) : mu{m}, precision{s}, nu{n}
    {
	check::sign(nu);
	name = "MVT";
    }
    arma::uword dimension(void) const override { return mu.n_elem; }
    double logpi(const arma::vec& x) const override
    {
	return -0.5 * (nu + double(dimension())) * log(1.0 + precision.squared_norm(x-mu)/nu);
    }
    arma::vec gradient(const arma::vec& x) const override
    {
	return -0.5 * (nu + double(dimension())) / (nu + precision.squared_norm(x-mu)/nu) * (precision * (x-mu));
    }
    matrices::EigenMatrix negative_hessian(const arma::vec& x) const override
    {
	double tmp{ nu + precision.squared_norm(x-mu) };
	return matrices::EigenMatrix{0.5 * (nu + double(dimension())) * (arma::mat(precision) - (precision * (x-mu)) * (precision * (x-mu)).t()/tmp)/tmp};
    }
    std::ostream& operator<<(std::ostream& os) const override { return os << name << "(mu, Sig, nu)";  }
    arma::vec draw() const override
    {
	arma::vec z{arma::randn(dimension())};
	double w{ arma::accu(arma::square(arma::randn(arma::uword(std::ceil(nu))))) };
	z = precision.sqrt_solve(z);
	z *= nu;
	z /= w;
	z += mu;
	return z;
    }
};

} // namespace model
