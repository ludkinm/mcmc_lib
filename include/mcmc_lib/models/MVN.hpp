#pragma once

#include <armadillo>
#include "mcmc_lib/bases/Model.hpp"
#include "mcmc_lib/util/SquareMatrices.hpp"
#include "mcmc_lib/debug.hpp"

namespace model{
/**
 * A multivariate normal distribution using a *Precision matrix* the inverse of the covariance.
 */
class MVN: public vec::DrawableHessian<matrices::EigenMatrix>{
private:
    using MyBase = vec::DrawableHessian<matrices::EigenMatrix>;
protected:
    arma::vec mu;
    matrices::EigenMatrix precision;
public:
    using MyBase::XType;
    using MyBase::GradientType;
    using MyBase::HessianType;
    MVN() = default;
    MVN(const arma::vec& m, const arma::mat& s) : mu{m}, precision{s} { name = "MVN"; }
    arma::uword dimension(void) const override { return mu.n_elem; }
    double logpi(const arma::vec& x) const override { return 0.5 * arma::dot(mu-x, precision * (x-mu)); }
    arma::vec gradient(const arma::vec& x) const override { return precision * (mu-x); }
    matrices::EigenMatrix negative_hessian([[maybe_unused]] const arma::vec& x) const override { return precision; }
    std::ostream& operator<<(std::ostream& os) const override { return os << name << "(mu, Sig)";  }
    arma::vec draw() const override
    {
	arma::vec z{arma::randn(dimension())};
	z = precision.sqrt_solve(z);
	z += mu;
	return z;
    }
};

} // namespace model
