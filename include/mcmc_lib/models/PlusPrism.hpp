#pragma once

#include <armadillo>
#include "mcmc_lib/models/Normal.hpp"
#include "mcmc_lib/models/Mixture.hpp"
#include "mcmc_lib/models/Cat.hpp"
#include "mcmc_lib/util/Checks.hpp"
#include "mcmc_lib/debug.hpp"

namespace model{
namespace vec{

/**
 *  X \sim 1/2 MVN(0, sig0) + 1/2 MVN(0, sig1)
 *  for the (a,b,lam) version:
 *    P = sqrt(1+lam)
 *    Q = sqrt(1-lam)
 *    sig0 = (Pa, Qb)
 *    sig1 = (Qa, Pb)
 * and E[X] = 0; Var[x] = (a^2, b^2)
 * lam=0 easy, lam=1 hard
 */
class PlusPrism: public vec::mixture::DrawableHessian<Normal::HessianType, Normal::HessianType>{
private:
    using vec::mixture::DrawableHessian<Normal::HessianType, Normal::HessianType>::DrawableHessian;
public:
    PlusPrism(const arma::vec& ab, double lam=0.85) : PlusPrism{ab(0), ab(1), lam} {}

    PlusPrism(double a=1.0, double b=1.0, double lam=0.85) : PlusPrism{arma::vec{sqrt(1.0+lam) * a, sqrt(1.0-lam) * b}, arma::vec{sqrt(1.0-lam) * a, sqrt(1.0+lam) * b}}
    {
	check::sign(lam);
	check::sign(1-lam);
    }

    PlusPrism(const arma::vec& sig0, const arma::vec& sig1) : DrawableHessian{std::make_shared<Normal>(sig0), std::make_shared<Normal>(sig1)}
    {
	check::sign(sig0);
	check::sign(sig1);
	name = "PlusPrism";
    }
};

} // namespace vec
} // namespace model
