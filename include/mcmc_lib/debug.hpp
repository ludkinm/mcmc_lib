#pragma once

#include <iostream>

#define DEBUG(x)
#define DEBUG_COUT(x)
#define DEBUG_LOC(x)

#ifndef MCMC_LIB_DEBUG
#define MCMC_LIB_DEBUG 0
#endif

#if MCMC_LIB_DEBUG
#undef DEBUG
#undef DEBUG_COUT
#undef DEBUG_LOC
#define DEBUG(x) do { x; } while (0)
#define DEBUG_COUT(x) do { std::cout << x << std::endl; } while (0)
#define DEBUG_LOC(x) do { std::cout << __FILE__ << "::" << __LINE__ << " " << __func__ << " -> " << x << std::endl; } while (0)
#endif
