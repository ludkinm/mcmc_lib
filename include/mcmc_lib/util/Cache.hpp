#pragma once
#include <iostream>
#include <iomanip>
#include <memory>

namespace cache{

/**
 * Store a value and a validity indicator
 * Client code manages update of value and validity
 */
template <typename T>
class Cache{
private:
    T value;
    bool validity;
public:
    Cache(): value{T{}}, validity{false} {}
    Cache(const T& t): value{t}, validity{true} {}

    explicit operator bool() const  // check validity
    {
	return validity;
    }

    operator T() const  // get value
    {
	return value;
    }

    Cache& operator=(const T& t) // assign new value
    {
	value = t;
	validity = true;
	return *this;
    }

    void invalidate(void) { validity = false; }

    std::ostream& operator<<(std::ostream& os) const
    {
	if(*this){
	    os << value;
	} else{
	    os << "null";
	}
	return os;
    }

    friend std::ostream& operator<<(std::ostream& os, const Cache& c) { return c.operator<<(os); }
};

} // namespace cache
