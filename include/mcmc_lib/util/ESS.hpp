#pragma once

#include <armadillo>

namespace ess{

/**
 * Computation of the Effective Sample Size for a Markov chain.
 * C++ Version of R library coda
 */

/**
 * ESS multivariate - Compute ESS of each *column* of a matrix
 *
 * @param arma::mat X - Matrix of values with chains stored columnwise
 * @return arma::vec - vector with X.n_cols number of elements with corresponding ESS stored.
 */
arma::vec ESSmv(const arma::mat& X);

/**
 * Compute ESS for a given vector
 *
 * @param arma::vec x - vector of samples of a Markov Chain
 * @return double - ESS
 */
double ESS(const arma::vec& x);

/**
 * Spectral value from an AR model.
 *
 * This fits an autoregressive model to find the spectral decomposition then computes
 * \f$ gamma_n/(1 - \sum_i \gamma_i)^2 \f$
 *
 * @param arma::vec x - vector of samples of a Markov Chain
 * @return double - the spectral value
 */
double specAR(const arma::vec& x);

/**
 * Lazy linear model
 *
 * @param arma::vec x - vector of samples of a Markov Chain
 * @return arma::vec -
 */
arma::vec lazyLM(const arma::vec& x);

/**
 * Fit an Autoregressive model via the Yule Walker equations
 *
 * @param arma::vec x - vector of samples of a Markov Chain
 * @return arma::vec -yule walker coefficients
 */
arma::vec arYW(const arma::vec& x);

/**
 * Magically code converted from the Base R Fortran
 * used to make the Yule Walker coefficients
 */
void eureka(const arma::uword& maxLag, const arma::vec& r, arma::mat& f, arma::vec& var); // from base R

arma::vec ESSmv(const arma::mat& X)
{
    arma::vec ans = arma::zeros(X.n_cols);
    for(arma::uword ii = 0; ii < X.n_cols; ++ii)
	ans(ii) = ESS(X.col(ii));
    return ans;
}

double ESS(const arma::vec& x)
{
    double ans = 0.0;
    double spec = specAR(x);
    if(spec)
	ans = double(x.n_elem) * arma::var(x)/spec;
    return ans;
}

double specAR(const arma::vec& x)
{
    double spec = 0.0;
    if(all(abs(lazyLM(x)) > 1e-15))
	{
	    arma::vec ar = arYW(x);
	    spec = ar(ar.n_elem-1)/pow((1 - accu(ar.subvec(0, ar.n_elem-2))),2);
	}
    return spec;
}

arma::vec lazyLM(const arma::vec& x)
{
    double n = double(x.n_elem);
    arma::vec z = arma::linspace(1,n,x.n_elem);
    double sz = sum(z);
    double sx = sum(x);
    double sxz = arma::dot(x,z);
    double tmp = pow(arma::norm(z)/sz, 2);
    double a = (sx*tmp - sxz/sz)/(n*tmp - 1);
    double b = (n*sxz/sz - sx)/(n*tmp - 1)/sz;
    return x - a - b*z;
}

arma::vec arYW(const arma::vec& x)
{
    double varPred;
    arma::uword order, ii;
    arma::vec vars, AICS, ar;
    arma::mat coeffs;
    arma::vec y{x - arma::mean(x)};
    arma::uword n{x.n_elem};
    arma::uword maxLag = floor(10.0 * log10(double(n)));
    if(maxLag > n - 1)
	maxLag = n-1;
    arma::vec r{arma::zeros(maxLag + 1)};
    for(ii = 0; ii < maxLag+1; ++ii)
	r(ii) = arma::dot(y.subvec(0, n-ii-1) , y.subvec(ii, n-1))/double(n);
    if(r(0) == 0.0)
	return arma::zeros(2);
    coeffs = arma::zeros(maxLag, maxLag);
    vars = arma::zeros(1+maxLag);
    vars(0) = r(0);
    // get coefficients and inovation variances for each AR models from ar(0) to ar(maxLag)
    eureka(maxLag, r, coeffs, vars);
    AICS = n * log(vars) + 2 * arma::linspace(0, maxLag, 1+maxLag) + 2;
    order = AICS.index_min();
    varPred = vars(order) * n/(n-order-1);
    ar = arma::vec({0.0, varPred});
    if(order){
	ar = arma::zeros(order+1);
	ar.subvec(0, order-1) = coeffs.submat(order-1, 0, order-1, order-1).t();
	ar(order) = varPred;
    }
    return ar;
}

void eureka(const arma::uword& lr, const arma::vec& r, arma::mat& f, arma::vec& var)
{
    arma::uword l,l1,l2,i,j,k;
    arma::vec a = arma::zeros(lr);
    double v, d, q, hold;
    v = r(0);
    d = r(1);
    a(0) = 1.0;
    f(0,0) = r(1)/v;
    q = f(0,0)*r(1);
    var(1) = (1 - pow(f(0,0), 2))*r(0);
    for(l = 2; l <= lr; ++l){
	a(l-1) = -d/v;
	if (l > 2){
	    l1 = (l - 2)/2;
	    l2 = l1 + 1;
	    for(j = 2; j <= l2; ++j){
		hold = a(j-1);
		k = l - j + 1;
		a(j-1) = a(j-1) + a(l-1)*a(k-1);
		a(k-1) = a(k-1) + a(l-1)*hold;
	    }
	    if (2*l1 != l - 2){
		a(l2) = a(l2)*(1.0 + a(l-1));
	    }
	}
	v = v + a(l-1)*d;
	f(l-1,l-1) = (r(l) - q)/v;
	for(j = 1; j <= l-1; ++j){
	    f(l-1,j-1) = f(l-2, j-1) + f(l-1, l-1)*a(l-j);
	}
	var(l) = var(l-1) * (1 - pow(f(l-1,l-1),2));
	if (l ==  lr)
	    break;
	d = 0.0;
	q = 0.0;
	for(i = 1; i <= l; ++i){
	    k = l-i+2;
	    d = d + a(i-1)*r(k-1);
	    q = q + f(l-1,i-1)*r(k-1);
	}
    }
}

}
