#pragma once

#include <armadillo>
#include <exception>

namespace check{
template<typename T>
void sign(const T& A)
{
    if( A <= T(0) )
	throw std::logic_error("Argument should be positive");
}

template<> void sign<arma::vec>(const arma::vec& sig)
{
    if( any(arma::sign(sig) != 1) )
	throw std::logic_error("All sigmas should be positive");
}
}
