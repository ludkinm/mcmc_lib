#pragma once

#include <armadillo>
#include <string>
#include <stdlib.h>

template<class T>
T grab_arg(char const * arg);

template<class T>
T grab_arg(int i, int argc, char **argv, T def)
{
    return (argc > i) ? grab_arg<T>(argv[i]) : def;
}

template<> arma::uword grab_arg(char const * arg) { return std::atoi(arg); }
template<> int grab_arg(char const * arg) { return std::atoi(arg); }
template<> bool grab_arg(char const * arg) { return std::atoi(arg); }
template<> double grab_arg(char const * arg) { return std::atof(arg); }
template<> std::string grab_arg(char const * arg) { return std::string(arg); }
