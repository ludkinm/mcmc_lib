#pragma once

#include <iostream>
#include <type_traits>
#include <armadillo>
#include "mcmc_lib/debug.hpp"

namespace matrices{

/**
 * A square matrix type
 * Its an abstract type so can't instantiated objects of type SquareMatrix
 * Derivatives of this class are used across the library for e.g. hessians, pre-conditioning matrices and MVN proposals
 * The operations can be specialised for certain types of matrix
 */
class Square{
public:
    Square() = default;
    virtual ~Square() = default;
    virtual operator arma::mat() const = 0;
    virtual arma::uword dimension() const = 0;
    virtual arma::vec operator*(const arma::vec& x) const = 0;
    virtual double log_det(void) const = 0;
    virtual arma::vec solve(const arma::vec& x) const = 0;
    virtual arma::vec sqrt_times(const arma::vec& x) const = 0;
    virtual arma::vec sqrt_solve(const arma::vec& x) const = 0;
    virtual double inner_prod(const arma::vec& x, const arma::vec& y) const { return arma::dot(x, (*this) * y); } // x^\top S y
    virtual double inner_prod_inv(const arma::vec& x, const arma::vec& y) const { return arma::dot(x, this->solve(y)); } // x^\top S^{-1} y
    virtual double squared_norm(const arma::vec& x) const { return inner_prod(x, x); }// x^\top S x
    virtual double squared_norm_inv(const arma::vec& x) const { return inner_prod_inv(x, x); }// x^\top S^{-1} x
    virtual std::ostream& operator<<(std::ostream& os) const = 0;
    friend std::ostream& operator<<(std::ostream& os, const Square& s) { return s.operator<<(os); }
};

template <typename T>
using is_square = std::is_base_of<Square, T>;

template<typename T>
inline constexpr bool is_square_v = is_square<T>::value;

/**
 * A diagonal matrix is a square matrix, we allow a conversion to a arma::vec
 */
class Diagonal : public Square{
public:
    using Square::operator arma::mat;
    virtual operator arma::vec() const = 0;
};

template <typename T>
using is_diagonal = std::is_base_of<Diagonal, T>;

template<typename T>
inline constexpr bool is_diagonal_v = is_diagonal<T>::value;

/**
 * A scalar multiplied by identity matrix, lots of operations are O(d) instead of O(d^2) or O(d^3)...
 */
class IDMatrix: public Diagonal{
protected:
    double sigma;
    double sqrtSigma;
    arma::uword dim;
    double logDet;
public:
    IDMatrix() = default;
    explicit IDMatrix(arma::uword d) : IDMatrix{1.0, d} {}
    IDMatrix(double s, arma::uword d) : Diagonal{}, sigma{s}, sqrtSigma{sqrt(sigma)}, dim{d}, logDet{double(dim) * log(sigma)} {}
    inline explicit operator double() const { return sigma; }
    inline operator arma::mat() const override { return sigma * arma::eye(dim,dim); }
    inline operator arma::vec() const override { return sigma * arma::ones(dim); }
    inline arma::uword dimension() const override { return dim; }
    inline arma::vec operator*(const arma::vec& x) const override { return sigma * x; }
    inline double log_det(void) const override { return logDet; }
    inline arma::vec solve(const arma::vec& x) const override { return x/sigma; }
    inline arma::vec sqrt_times(const arma::vec& x) const override { return sqrtSigma*x; }
    inline arma::vec sqrt_solve(const arma::vec& x) const override { return x/sqrtSigma; }
    virtual double inner_prod(const arma::vec& x, const arma::vec& y) const override { return sigma * arma::dot(x,y); }
    virtual double inner_prod_inv(const arma::vec& x, const arma::vec& y) const override { return arma::dot(x,y)/sigma; }
    inline std::ostream& operator<<(std::ostream& os) const override { return os << "IDMatrix: " << sigma << ", " << dim; }
    // double arithmetic
    IDMatrix operator*(const double& a) const { return IDMatrix{sigma*a, dim}; }
    IDMatrix operator/(const double& a) const { return IDMatrix{sigma/a, dim}; }
};
IDMatrix operator*(const double& a, const IDMatrix& s) { return s*a; }

/**
 * A diagonal matrix, lots of operations are O(d) instead of O(d^2) or O(d^3)...
 */
class DiagMatrix: public Diagonal{
protected:
    arma::vec v;
    arma::vec sqrtv;
    double logDet;
    DiagMatrix(const arma::vec& m, const arma::vec& sqrtm) : Diagonal{}, v{m}, sqrtv{sqrtm}, logDet{sum(log(v))} {}
public:
    DiagMatrix() = default;
    explicit DiagMatrix(const arma::vec& m) : DiagMatrix{m, arma::sqrt(m)} {}
    inline operator arma::mat() const override { return arma::diagmat(v); }
    inline operator arma::vec() const { return v; }
    inline arma::uword dimension() const override { return v.n_elem; }
    inline arma::vec operator*(const arma::vec& x) const override { return v % x; }
    inline double log_det(void) const override { return logDet; }
    inline arma::vec solve(const arma::vec& x) const override { return x/v; }
    inline arma::vec sqrt_times(const arma::vec& x) const override { return sqrtv % x; }
    inline arma::vec sqrt_solve(const arma::vec& x) const override { return x/sqrtv; }
    inline std::ostream& operator<<(std::ostream& os) const override { return os << "DiagMatrix: " << v.t(); }
    // double arithmetic
    DiagMatrix operator*(const double& a) const { return DiagMatrix{a*v, sqrt(a)*sqrtv}; }
    DiagMatrix operator/(const double& a) const { return DiagMatrix{v/a, sqrtv/sqrt(a)}; }
};
DiagMatrix operator*(const double& a, const DiagMatrix& s) { return s*a; }

/**
 * A full matrix -- thin wrapper for arma::mat
 */
class SquareMatrix: public Square{
protected:
    arma::mat m;
public:
    SquareMatrix() = default;
    explicit SquareMatrix(const arma::mat& m_) : m{m_} {}
    inline operator arma::mat() const override { return m; }
    inline arma::uword dimension() const override { return m.n_rows; }
    inline arma::vec operator*(const arma::vec& x) const override { return m * x; }
    inline double log_det(void) const override { return real(arma::log_det(m)); }
    inline arma::vec solve(const arma::vec& x) const override { return arma::solve(m, x); }
    inline arma::vec sqrt_times(const arma::vec& x) const override
    {
	try{
	    return arma::chol(m).t() * x;
	} catch(std::exception& e){
	    std::cerr << "Failed to cholsky with m:\n" << m << std::endl;
	    throw(e);
	}
    }
    inline arma::vec sqrt_solve(const arma::vec& x) const override
    {
	try{
	    return arma::solve(arma::chol(m), x);
	} catch(std::exception& e){
	    std::cerr << "Failed to cholsky with m:\n" << m << std::endl;
	    throw(e);
	}
    }
    inline std::ostream& operator<<(std::ostream& os) const override { return os << "m=" << m; }
    // double arithmetic
    SquareMatrix operator*(const double& a) const { return SquareMatrix{a*m}; }
    SquareMatrix operator/(const double& a) const { return SquareMatrix{m/a}; }
};
SquareMatrix operator*(const double& a, const SquareMatrix& s) { return s*a; }

/**
 * A full matrix -- we cache the cholesky so sqrt(m) * x is stored as c.
 * Cholesky is such that m = c^T c
 * If and Z ~ MVN(0,I) and X = c^T Z then X ~ MVN(0, c^T c) => X~MVN(0, m) so m is the covariance matrix
 * example: `CholMatrix M; X = M.sqrt_times(Z);`
 * If and Z ~ MVN(0,I) and X = c^{-1} Z then X ~ MVN(0, c^{-1} (c^{-1})^T) => X ~ MVN(0,  (c^T c)^{-1}) => X ~ MVN(0,  m^{-1}) so m is the precision matrix
 * Internally: `X = solve(c, Z);` but with  `CholMatrix M; X = M.sqrt_solve(Z);`
 * @see [https://en.wikipedia.org/wiki/Multivariate_normal_distribution#Affine_transformation]
 */
class CholMatrix: public Square{
protected:
    arma::mat m;
    arma::mat c;
    CholMatrix(const arma::mat& m, const arma::mat& c) : Square{}, m{m}, c{c} {}
public:
    CholMatrix() = default;
    explicit CholMatrix(const arma::mat& _m) : CholMatrix{}
    {
	m = _m;
	try{
	    c = arma::chol(_m);
	} catch(std::exception& e){
	    std::cerr << "error in CholMatrix(_m) construction. _m = " << _m << std::endl;
	    throw e;
	}
    }
    inline operator arma::mat() const override { return m; }
    inline arma::uword dimension() const override { return m.n_rows; }
    inline arma::vec operator*(const arma::vec& x) const override { return m * x; }
    inline double log_det(void) const override { return real(arma::log_det(m)); }
    inline arma::vec solve(const arma::vec& x) const override { return arma::solve(m, x); }
    inline arma::vec sqrt_times(const arma::vec& x) const override { return c.t() * x; }
    inline arma::vec sqrt_solve(const arma::vec& x) const override { return arma::solve(c, x); }
    inline std::ostream& operator<<(std::ostream& os) const override { return os << "m=" << m << "chol(m)=" << c; }
    // double arithmetic
    CholMatrix operator*(const double& a) const { return CholMatrix{a*m, sqrt(a)*c}; }
    CholMatrix operator/(const double& a) const { return CholMatrix{m/a, c/sqrt(a)}; }
};
CholMatrix operator*(const double& a, const CholMatrix& s) { return s*a; }

/**
 * A full matrix with stored Eigen decomposition
 * If and Z ~ MVN(0,I) and X = c^T Z then X ~ MVN(0, c^T c) => X~MVN(0, m) so m is the covariance matrix
 * example: `EigenMatrix M; X = M.sqrt_times(Z);`
 * If and Z ~ MVN(0,I) and X = c^{-1} Z then X ~ MVN(0, c^{-1} (c^{-1})^T) => X ~ MVN(0,  (c^T c)^{-1}) => X ~ MVN(0,  m^{-1}) so m is the precision matrix
 * Internally: `X = solve(c, Z);` but with  `EigenMatrix M; X = M.sqrt_solve(Z);`
 * @see [https://en.wikipedia.org/wiki/Multivariate_normal_distribution#Affine_transformation]
 */
class EigenMatrix: public Square{
protected:
    arma::mat m;
    arma::mat vecs;
    arma::vec vals;
    arma::vec sqrtVals;
    double logDet;
    EigenMatrix(const arma::mat& m, const arma::mat& c, const arma::vec& l, const arma::vec& sqrtl) : Square{}, m{m}, vecs{c}, vals{l}, sqrtVals{sqrtl}, logDet{arma::sum(log(vals))} {}
public:
    EigenMatrix() = default;
    explicit EigenMatrix(const arma::mat& _m) try: Square{}, m{_m}
    {
	// if m has nan values, them eig_sym won't work.
	// lets set the vecs as nan and vals as 1s.
	if(m.has_nan()){
	    vecs = m;
	    vals = arma::ones(m.n_rows);
	    sqrtVals = arma::ones(m.n_rows);
	    logDet = arma::datum::nan;
	} else{ // try to eig_sym
	    bool success{arma::eig_sym(vals, vecs, m)};
	    if(!success){
		throw std::logic_error("eigen decom failed");
	    }
	    sqrtVals = sqrt(vals);
	    logDet = arma::sum(log(vals));
	}
    } catch(const std::exception& e){
	std::cerr << e.what() << " in EigenMatrix construction while taking sym eigen decomposition. Is the provided matrix symmetric? m=" << _m;
	throw e;
    }
    inline operator arma::mat() const override { return vecs * arma::diagmat(vals) * vecs.t(); }
    inline arma::uword dimension() const override { return vals.n_elem; }
    inline arma::vec operator*(const arma::vec& x) const override { return m * x; }
    inline double log_det(void) const override { return logDet; }
    inline arma::vec solve(const arma::vec& x) const override { return vecs * ((vecs.t() * x)/vals); }
    inline arma::vec sqrt_times(const arma::vec& x) const override { return vecs * (sqrtVals % x); }
    inline arma::vec sqrt_solve(const arma::vec& x) const override { return vecs * (x/sqrtVals); }
    inline std::ostream& operator<<(std::ostream& os) const override { return os << "m=" << m << "vals=" << vals.t(); }
    // double arithmetic
    EigenMatrix operator*(const double& a) const { return EigenMatrix{a*m, vecs, a*vals, sqrt(a)*vals}; }
    EigenMatrix operator/(const double& a) const { return EigenMatrix{m/a, vecs, vals/a, vals/sqrt(a)}; }
};
EigenMatrix operator*(const double& a, const EigenMatrix& s) { return s*a; }

/**
 * A full matrix with storing the "Absolute Valued" Eigen decomposition where the absolute value of the eigenvalues are taken.
 */
class AbsEigenMatrix: public EigenMatrix{
protected:
    using EigenMatrix::vals;
    using EigenMatrix::vecs;
    using EigenMatrix::logDet;
    using EigenMatrix::sqrtVals;
    void abs_eig(void)
    {
	vals = abs(vals);
	vals = clamp(vals, 1e-10, arma::datum::inf); // ensure positive
	sqrtVals = sqrt(vals);
	logDet = arma::accu(log(vals));
    }
    AbsEigenMatrix(const arma::mat& m, const arma::mat& c, const arma::vec& l, const arma::vec& sqrtl) : EigenMatrix{m, c, l, sqrtl}
    {
	abs_eig();
    }
public:
    AbsEigenMatrix() = default;
    explicit AbsEigenMatrix(const arma::mat& m) : EigenMatrix{m}
    {
	abs_eig();
    }
    inline arma::vec operator*(const arma::vec& x) const override { return vecs * (vals % (vecs.t() * x)); }
    inline arma::vec sqrt_times(const arma::vec& x) const override { return vecs * (sqrtVals % (vecs.t() * x)); }
    inline arma::vec sqrt_solve(const arma::vec& x) const override { return vecs * ((vecs.t() * x)/sqrtVals); }
    AbsEigenMatrix operator*(const double& a) const { return AbsEigenMatrix{abs(a)*m, vecs, abs(a)*vals, sqrt(abs(a))*vals}; }
    AbsEigenMatrix operator/(const double& a) const { return AbsEigenMatrix{m/abs(a), vecs, vals/abs(a), vals/sqrt(abs(a))}; }
};
AbsEigenMatrix operator*(const double& a, const AbsEigenMatrix& s) { return s*a; }

/**
 * A block matrix consisting of two square matrices S0, S1, such that S = [[S0, 0],[0, S1]]
 */
template<typename S0, typename S1>
class BlockMatrix: public Square{
protected:
    static_assert(is_square_v<S0>, "BlockMatrix template parameters must be square! S0 isn't");
    static_assert(is_square_v<S1>, "BlockMatrix template parameters must be square! S1 isn't");
    S0 s0;
    S1 s1;
    arma::vec first_half(const arma::vec& x) const { return x.subvec(0, s0.dimension()-1); }
    arma::vec second_half(const arma::vec& x) const { return x.subvec(s0.dimension(), dimension()); }
public:
    BlockMatrix(const S0& a, const S1& b) : s0{a}, s1{b} {}
    inline operator arma::mat() const override
    {
	return arma::join_vert(arma::join_horiz(arma::mat(s0), arma::zeros(s0.dimension(), s1.dimension())),
			       arma::join_horiz(arma::zeros(s1.dimension(), s0.dimension()), arma::mat(s1)));
    }
    inline arma::uword dimension() const override { return s0.dimension() + s1.dimension(); }
    inline arma::vec operator*(const arma::vec& x) const override { return arma::join_vert(s0 * first_half(x), s1 * second_half(x)); }
    inline double log_det(void) const override { return s0.log_det() + s1.log_det(); }
    inline arma::vec solve(const arma::vec& x) const override { return arma::join_vert(s0.solve(first_half(x)), s1.solve(second_half(x))); }
    inline arma::vec sqrt_times(const arma::vec& x) const override { return arma::join_vert(s0.sqrt_times(first_half(x)), s1.sqrt_times(second_half(x))); }
    inline arma::vec sqrt_solve(const arma::vec& x) const override { return arma::join_vert(s0.sqrt_solve(first_half(x)), s1.sqrt_solve(second_half(x))); }
    inline std::ostream& operator<<(std::ostream& os) const override { return os << "Block matrix: Block1:\n" << s0 << "Block2:" << s1; }
    // double arithmetic
    BlockMatrix operator*(const double& a) const { return BlockMatrix{a*s0, a*s1}; }
    BlockMatrix operator/(const double& a) const { return BlockMatrix{s0/a, s1/a}; }
};
template<typename S0, typename S1>
BlockMatrix<S0,S1> operator*(const double& a, const BlockMatrix<S0,S1>& s) { return s*a; }

template<typename S0, typename S1>
class BlockDiagMatrix: public BlockMatrix<S0, S1>, public virtual Diagonal{
protected:
    static_assert(is_diagonal_v<S0>, "BlockDiagMatrix must be built from diagonal matrices! S0 isn't");
    static_assert(is_diagonal_v<S1>, "BlockDiagMatrix must be built from diagonal matrices! S1 isn't");
    S0 s0;
    S1 s1;
    arma::vec first_half(const arma::vec& x) const { return x.subvec(0, s0.dimension()-1); }
    arma::vec second_half(const arma::vec& x) const { return x.subvec(s0.dimension(), dimension()); }
public:
    using BlockMatrix<S0,S1>::BlockMatrix;
    inline operator arma::vec() const override { return arma::join_vert(arma::vec(s0), arma::vec(s1)); }
    BlockDiagMatrix operator*(const double& a) const { return BlockDiagMatrix{a*s0, a*s1}; }
    BlockDiagMatrix operator/(const double& a) const { return BlockDiagMatrix{s0/a, s1/a}; }
};
template<typename S0, typename S1>
BlockDiagMatrix<S0,S1> operator*(const double& a, const BlockDiagMatrix<S0,S1>& s) { return s*a; }

/**
 * Inter-Square operations
 */
DiagMatrix operator+(const DiagMatrix& x, const DiagMatrix& y) { return DiagMatrix{arma::vec(x) + arma::vec(y)}; }
DiagMatrix operator+(const IDMatrix& x,   const DiagMatrix& y) { return DiagMatrix{arma::vec(x) + arma::vec(y)}; }

SquareMatrix operator+(const IDMatrix& x,     const SquareMatrix& y) { arma::mat z{y}; z.diag() += arma::vec(x); return SquareMatrix{z}; }
SquareMatrix operator+(const DiagMatrix& x,   const SquareMatrix& y) { arma::mat z{y}; z.diag() += arma::vec(x); return SquareMatrix{z}; }
SquareMatrix operator+(const SquareMatrix& x, const SquareMatrix& y) { return SquareMatrix{arma::mat(x) + arma::mat(y)}; }

}
