#pragma once

#include <iomanip>
#include <armadillo>
#include "mcmc_lib/debug.hpp"
#include "mcmc_lib/bases/Model.hpp"
#include "mcmc_lib/bases/MCMC.hpp"

namespace kstest{

double exact_p(double stat, arma::uword n);
double approx_p_small_stat(double x);
double approx_p_large_stat(double x);
double approx_p(double stat, arma::uword n);
arma::vec pvalues(const arma::vec& stats, arma::uword numberOfSamples);
arma::vec statistics(const arma::mat& xs, const arma::mat& ys);
arma::vec statistics(const model::vec::DrawableLogpi& mod, const arma::mat& xs, arma::uword numberOfSamples);
bool KS(const model::vec::DrawableLogpi& mod, const arma::mat& xs, arma::uword numberOfSamples, double alpha);

double exact_p(double stat, arma::uword n)
{
    const double q{0.5/n + (stat * n - 1)};
    DEBUG_COUT("q = " << q);
    arma::vec u{ arma::linspace(0.0, double(n), n+1) };
    u.transform( [&](double val) { return (val >=q ? 0.0 : 1.0); } );
    DEBUG_COUT("u = " << u.t());
    for(arma::uword i=1; i <= n; ++i){
	double w = double(i)/double(i + n);
	DEBUG_COUT("w = " << w);
	u(0) = (i > q ? 0.0 : w * u(0));
	for(arma::uword j=1; j <= n; ++j){
	    u(j) = (fabs(double(i)-double(j)) > q ? 0.0 : w * u(j) + u(j-1));
	}
	DEBUG_COUT("u = " << u.t());
    }
    return 1 - u(n);
}

double approx_p_small_stat(double x)
{
    // from R::src/library/stats/src/ks.c:pkstwo
    // k_max = (int) sqrt(2 - log(tol));
    // which for the defaul tol = 1e-6 yields k_max=4
    arma::uword k_max{ 4 };
    double z{ -0.125 * pow(arma::datum::pi/x, 2) };
    double w{ log(x) };
    double s{0};
    for(arma::uword k = 1; k < k_max; k += 2) {
	s += exp(k * k * z - w);
    }
    return sqrt(2.0 * arma::datum::pi) * s;
}

double approx_p_large_stat(double x)
{
    double z{-2.0 * x * x };
    arma::sword s{-1};
    arma::uword k{1};
    double oldx{0};
    double newx{1};
    while(fabs(oldx - newx) > 1e-6) {
	oldx = newx;
	newx += 2.0 * s * exp(z * k * k);
	s *= -1;
	k++;
    }
    return newx;
}

double approx_p(double stat, arma::uword n)
{
    double x{ sqrt(0.5 * double(n)) * stat };
    return ( (x < 1) ? approx_p_small_stat(x) : approx_p_large_stat(x) );
}

/**
 * Kolomagorov-Smirnov test p-value calculator
 * @param arma::vec stats - a vector of test statistic from eg stats
 * @param double numberOfSamples - the number of samples used when computing the test statistic
 * @return arma::vec of p-values for the stats
 */
arma::vec pvalues(const arma::vec& stats, arma::uword numberOfSamples)
{
    arma::vec pvals{ arma::zeros(stats.n_elem) };
    // convert KStest statistics into p-values
    if(numberOfSamples < 1000){
	DEBUG_COUT("numberOfSamples = " << numberOfSamples << " < 1000 so calculating exact p-values.");
	for(arma::uword i=0; i < stats.n_elem; ++i){
	    pvals(i) = exact_p(stats(i), numberOfSamples);
	}
    } else{
	DEBUG_COUT("numberOfSamples = " << numberOfSamples << " >= 1000 so calculating approximate p-values.");
	for(arma::uword i=0; i < stats.n_elem; ++i){
	    pvals(i) = approx_p(stats(i), numberOfSamples);
	}
    }
    return pvals;
}

/**
 * Kolomagorov-Smirnov test statistic for comparing two-samples
 * This is how ks.test in R works to get a statistic:
 * For X ~ MCMC; Y ~ Model
 * for each dimension:
 * join the y and x samples
 * sort the combined set
 * make a vector W with:
 * - a 1 if the value is less than numberOfSamples (this means it came from x)
 * - a -1 if the value is greater than numberOfSamples (this means it came from y)
 * take the cumulative sum of W, this gives the number of consecutive points which are in the *same* sample
 * and the maximum over (the absolute value) of this is the difference in the unnormalised CDFs
 * finally, dividing by numberOfSamples (the size of each sample) normalises the CDFs and thus yields the KS.stat
 * @param mat xs -- first matrix of samples
 * @param mat ys -- second matrix of samples
 * @return vec of test statistics for each column
 * @see https://en.wikipedia.org/wiki/Kolmogorov%E2%80%93Smirnov_test#Two-sample_Kolmogorov%E2%80%93Smirnov_test
 */
arma::vec statistics(const arma::mat& xs, const arma::mat& ys)
{
    const arma::uword dim{ xs.n_cols };
    const arma::uword numberOfSamples{ xs.n_rows };
    if(size(xs) != size(ys))
	throw std::logic_error("KS::stats only works with equal sized matrices!");
    DEBUG_COUT("X:\n" << xs);
    DEBUG_COUT("Y:\n" << ys);
    arma::vec K{ arma::zeros(dim) };
    arma::vec W;
    arma::uvec S;
    arma::ivec I;
    for(arma::uword ii = 0; ii < dim; ++ii){
	W = arma::join_cols(xs.col(ii), ys.col(ii));
	DEBUG( W.raw_print("W = X join Y") );
	S = arma::sort_index(W);
	DEBUG( S.t().raw_print("S = sort_index(W)") );
	I = arma::conv_to<arma::ivec>::from(S);
	I.transform( [&](arma::sword val) { return ((val < arma::sword(numberOfSamples)) ? 1 : -1); } );
	DEBUG_COUT("I:\n" << I);
	K(ii) = max(abs(arma::cumsum(I)))/double(numberOfSamples);
    }
    DEBUG_COUT("Test statistics:\n " << K);
    return K;
}

/**
 * Kolomagorov-Smirnov test statistic for comparing samples drawn from a model and from an MCMC algorithm.
 * @param mod a drawable model
 * @param mat xs matrix of samples from MCMC
 * @param uword numberOfSamples for the test -- xs will be thinned to every numberOfSamples -- a safe number to use is 2*ESS
 * @return the p-value
 * @see https://en.wikipedia.org/wiki/Kolmogorov%E2%80%93Smirnov_test#Two-sample_Kolmogorov%E2%80%93Smirnov_test
 */
arma::vec statistics(const model::vec::DrawableLogpi& mod, const arma::mat& xs, arma::uword numberOfSamples)
{
    DEBUG_COUT("numberOfSamples = " << numberOfSamples);
    const arma::uword dim{ xs.n_cols };
    // thin by taking numberOfSamples equally spaced samples from xs.
    arma::uvec ind{ arma::linspace<arma::uvec>(0, xs.n_rows-1, numberOfSamples) };
    DEBUG_COUT("kstest::statistics indexing into X: " << ind.t());
    const arma::mat X{ xs.rows(ind) };
    // draw from the model
    arma::mat Y{ arma::zeros(numberOfSamples, dim) };
    for(arma::uword ii = 0; ii < numberOfSamples; ++ii)
	Y.row(ii) = mod.draw().t();
    return statistics(X, Y);
}

/**
 * Kolomagorov-Smirnov test statistic for comparing samples drawn from a model and from an MCMC algorithm.
 * @param mod a drawable model
 * @param mat xs matrix of samples from MCMC
 * @param uword numberOfSamples to take from the Model -- a safe amount is 2*ESS
 * @param doduble alpha significance level
 * @return bool - true if KS test passed and all dimensions are from the Model
 * @see https://en.wikipedia.org/wiki/Kolmogorov%E2%80%93Smirnov_test
 * @see https://en.wikipedia.org/wiki/Bonferroni_correction
 * @see KS::test
 */
bool KS(const model::vec::DrawableLogpi& mod, const arma::mat& xs, arma::uword numberOfSamples, double alpha = 0.05)
{
    // get the KS p-values from KS statistics
    arma::vec stats{ statistics(mod, xs, numberOfSamples) };
    DEBUG_COUT("kstest::KS stats = " << stats.t());
    arma::vec pvals{ pvalues(stats, numberOfSamples) };
    DEBUG_COUT("kstest::KS pvals = " << pvals.t());
    // perform the tests by comparing each kstat to \alpha / numberOfTests (Bonferroni)
    // recall that if the test pval < alpha is true then we reject the null which is that the two dists are the same.
    // so if pval < alpha we claim different distributions.
    DEBUG_COUT("kstest::KS FWER alpha = " << alpha);
    alpha /= double(pvals.n_elem);
    DEBUG_COUT("kstest::KS Bonferroni alpha = " << alpha);
    arma::uvec diffDists{ pvals < alpha * arma::ones(pvals.n_elem) };
    DEBUG_COUT("kstest::diff_dists = " << std::boolalpha << diffDists.t());
    // KS passes if all are from the dist
    return !arma::any(diffDists);
}

/**
 * Kolomagorov-Smirnov test statistic for MCMC output.
 * The number of samples for the KS test is taken as at most 25000 at least 250
 * @param mod a drawable model
 * @param mc an mcmc object
 * @param alpha the significance level
 */
template<typename State>
void KS(mcmc::MCMC<State>& mc, const model::vec::DrawableLogpi& mod, double alpha = 0.05)
{
    const arma::uword essX( floor(mc.calc_ess_xs()).min());
    const arma::uword nits{ mc.get_xs().n_rows };
    const arma::uword minSamples{  250 };
    const arma::uword maxSamples{ 2500 };
    const arma::uword numSamples{ std::min(nits, std::max(minSamples, std::min(2*essX, maxSamples))) };

    bool samplingTarget{ KS(mod, mc.get_xs(), numSamples, alpha) };
    mc.table_add_column("KS_sampling_target", (samplingTarget ? "true" : "false"));
}

} // namespace kstest
