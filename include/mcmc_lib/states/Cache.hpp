#include <type_traits>
#include "mcmc_lib/util/Cache.hpp"
#include "mcmc_lib/bases/State.hpp"

namespace state{
namespace state_cache{

/**
 * Cache the logpi value
 */
template<typename Base>
class Logpi: public Base{
private:
    cache::Cache<double> cachedLogpi;
public:
    Logpi() = default;
    Logpi(const Base& s): Base{s}, cachedLogpi{} {} // Copy a state and init logpi cache

    virtual inline double get_logpi(void) override
    {
	// if the cache is out of date, update the value
	if(!cachedLogpi) cachedLogpi = Base::get_logpi();
	return double(cachedLogpi);
    }

    virtual inline Logpi& operator=(const arma::vec& y) override
    {
	Base::operator=(y);
	cachedLogpi.invalidate();
	return *this;
    }

    std::ostream& operator<<(std::ostream& os) const override
    {
	return Base::operator<<(os) << " l = " << cachedLogpi;
    }
};

/**
 * Cache logpi and gradient value
 */
template<typename Base>
class Gradient: public Base{
private:
    cache::Cache<double> cachedLogpi;
    cache::Cache<arma::vec> cachedGradient;
public:
    Gradient() = default;
    Gradient(const Base& s): Base{s}, cachedLogpi{}, cachedGradient{} {}

    virtual inline double get_logpi(void) override
    {
	// if the cache is out of date, update the value
	if(!cachedLogpi) cachedLogpi = Base::get_logpi();
	return double(cachedLogpi);
    }

    virtual inline arma::vec get_gradient(void) override
    {
	// if the cache is out of date, update the value
	if(!cachedGradient) cachedGradient = Base::get_gradient();
	return arma::vec(cachedGradient);
    }

    virtual inline Gradient& operator=(const arma::vec& y) override
    {
	Base::operator=(y);
	cachedLogpi.invalidate();
	cachedGradient.invalidate();
	return *this;
    }

    std::ostream& operator<<(std::ostream& os) const override
    {
	return Base::operator<<(os)
			     << " logpi = " << cachedLogpi
			     << " gradient = " << cachedLogpi;
    }
};

/**
 * Cache logpi, gradient and negative_hessian value
 * H is the cached hessian type, converted from State.get_hessian
 */
template<typename State, typename H=typename State::HessianType>
class Hessian: public state::base::Hessian<typename State::XType, typename State::GradientType, H>{
private:
    using Base = state::base::Hessian<typename State::XType, typename State::GradientType, H>;
public:
    using typename Base::GradientType;
    using typename Base::HessianType;
    static_assert(interface::has_logpi_v<State>, "State should provide a logpi");
    static_assert(interface::has_gradient_v<State>, "State should provide a gradient");
    static_assert(interface::has_hessian_v<State>, "State should provide a hessian");
private:
    State baseState;
    cache::Cache<double> cachedLogpi;
    cache::Cache<GradientType> cachedGradient;
    cache::Cache<HessianType> cachedHessian;
public:
    Hessian() = default;
    Hessian(const State& s): Base{}, baseState{s}, cachedLogpi{}, cachedGradient{}, cachedHessian{} {}

    arma::uword dimension(void) const override { return baseState.dimension(); }

    explicit operator arma::vec() const override { return arma::vec(baseState); }

    virtual inline double get_logpi(void) override
    {
	// if the cache is out of date, update the value
	if(!cachedLogpi) cachedLogpi = baseState.get_logpi();
	return cachedLogpi;
    }

    virtual inline GradientType get_gradient(void) override
    {
	// if the cache is out of date, update the value
	if(!cachedGradient) cachedGradient = baseState.get_gradient();
	return cachedGradient;
    }

    virtual inline HessianType get_negative_hessian(void) override
    {
	// if the cache is out of date, update the value
	if(!cachedHessian) cachedHessian = H(baseState.get_negative_hessian());
	return cachedHessian;
    }

    virtual inline Hessian& operator=(const arma::vec& y) override
    {
	baseState = y;
	cachedLogpi.invalidate();
	cachedGradient.invalidate();
	cachedHessian.invalidate();
	return *this;
    }

    std::ostream& operator<<(std::ostream& os) const override
    {
	return os << baseState
		  << " logpi = " << cachedLogpi
		  << " gradient = " << cachedLogpi
		  << " hessian = " << cachedHessian;
    }
};

} // namespace vec_cache
} // namespace state
