#pragma once

#include <memory>
#include <iostream>
#include <armadillo>
#include "mcmc_lib/debug.hpp"
#include "mcmc_lib/bases/Model.hpp"
#include "mcmc_lib/bases/State.hpp"
#include "mcmc_lib/util/Cache.hpp"

namespace state{
namespace modelled{

/**
 * A state that interfaces to a model.
 * Store a pointer to a model
 * We assume that XType can be assigned by a vec and converted to vec.
 */

template<typename Model>
struct Logpi: public base::Logpi<typename Model::XType>{
    // import types
    using typename base::Logpi<typename Model::XType>::XType;

    std::shared_ptr<const Model> modPtr;

    XType x{};

    // construction
    Logpi() = default;
    Logpi(const std::shared_ptr<const Model>& m): x{}
    {
	attach_model(m);
    }

    void attach_model(const std::shared_ptr<const Model>& m)
    {
	modPtr = m;
	this->name = modPtr->name;
    }

    // methods
    std::ostream& operator<<(std::ostream& os) const override
    {
	os << x;
	if(modPtr){
	    os << " with model " << *modPtr << " at address " << modPtr;
	} else{
	    os << " with model at address " << modPtr;
	}
	return os;
    }

    Logpi& operator=(const arma::vec& y) override { x = y; return *this; }

    explicit operator arma::vec() const override { return arma::vec(x);}

    double get_logpi(void) override { return modPtr->logpi(x); }

    arma::uword dimension(void) const override { return modPtr->dimension(); }
};

template<typename Model>
struct Gradient: public base::Gradient<typename Model::XType, typename Model::GradientType>{
    // import types
    using typename base::Gradient<typename Model::XType, typename Model::GradientType>::XType;
    using typename base::Gradient<typename Model::XType, typename Model::GradientType>::GradientType;

    // data members
    XType x{};
    std::shared_ptr<const Model> modPtr;

    // construction
    Gradient() = default;
    Gradient(const std::shared_ptr<const Model>& m): x{}
    {
	attach_model(m);
    }

    void attach_model(const std::shared_ptr<const Model>& m)
    {
	modPtr = m;
	this->name = modPtr->name;
    }

    // methods
    std::ostream& operator<<(std::ostream& os) const override
    {
	os << x;
	if(modPtr){
	    os << " with model " << *modPtr << " at address " << modPtr;
	} else{
	    os << " with model at address " << modPtr;
	}
	return os;
    }
    Gradient& operator=(const arma::vec& y) override { x = y; return *this; }
    explicit operator arma::vec() const override { return arma::vec(x);}
    double get_logpi(void) override { return modPtr->logpi(x); }
    GradientType get_gradient(void) override { return modPtr->gradient(x); }
    arma::uword dimension(void) const override { return modPtr->dimension(); }
};

template<typename Model, typename H = typename Model::HessianType>
struct Hessian: public base::Hessian<typename Model::XType, typename Model::GradientType, H>{
    // import types
    using typename base::Hessian<typename Model::XType, typename Model::GradientType, H>::XType;
    using typename base::Hessian<typename Model::XType, typename Model::GradientType, H>::GradientType;
    using typename base::Hessian<typename Model::XType, typename Model::GradientType, H>::HessianType;

    // data members
    XType x{};
    std::shared_ptr<const Model> modPtr;

    // construction
    Hessian() = default;
    Hessian(const std::shared_ptr<const Model>& m): x{}
    {
	attach_model(m);
    }

    void attach_model(const std::shared_ptr<const Model>& m)
    {
	modPtr = m;
	this->name = modPtr->name;
    }

    // methods
    std::ostream& operator<<(std::ostream& os) const override
    {
	os << x;
	if(modPtr){
	    os << " with model " << *modPtr << " at address " << modPtr;
	} else{
	    os << " with model at address " << modPtr;
	}
	return os;
    }
    Hessian& operator=(const arma::vec& y) override { x = y; return *this; }
    explicit operator arma::vec() const override { return arma::vec(x);}
    double get_logpi(void) override { return modPtr->logpi(x); }
    GradientType get_gradient(void) override { return modPtr->gradient(x); }
    HessianType get_negative_hessian(void) override { return H(modPtr->negative_hessian(x)); }
    arma::uword dimension(void) const override { return modPtr->dimension(); }
};

} //namespace store_with_model
} // namespace state
