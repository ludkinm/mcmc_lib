#pragma once
#include <iostream>
#include <type_traits>

namespace interface{

struct Printable{
    virtual ~Printable() {};
    virtual std::ostream& operator<<(std::ostream& os) const = 0;
    friend inline std::ostream& operator<<(std::ostream& os, const  Printable& p) { return p.operator<<(os); }
};

struct Name: public Printable{
    std::string name{"NoName"};
    std::ostream& operator<<(std::ostream& os) const override { return os << name; }
    // construct
    Name() = default;
    Name(const std::string& s): name{s} {}

    // destruct
    ~Name() = default;

    // copy
    Name(const Name& other) = default;
    Name& operator=(const Name& other) = default;

    // move
    Name(Name&& other) = delete;
    Name& operator=(Name&& other) = delete;
};

template<typename X>
struct GetLogpi{
    using XType = X;
    virtual ~GetLogpi() {};
    virtual double get_logpi(void) = 0;
};

template<typename G>
struct GetGradient{
    using GradientType = G;
    virtual ~GetGradient() {};
    virtual G get_gradient(void) = 0;
};

template<typename H>
struct GetHessian{
    using HessianType = H;
    virtual ~GetHessian() {};
    virtual H get_negative_hessian(void) = 0;
};

template<typename X>
struct HasLogpi{
    using XType = X;
    virtual ~HasLogpi() {};
    virtual double logpi(const X& x) const = 0;
};

template<typename X, typename G>
struct HasGradient{
    using XType = X;
    using GradientType = G;
    virtual ~HasGradient() {};
    virtual G gradient(const X& x) const = 0;
};

template<typename X, typename H>
struct HasHessian{
    using XType = X;
    using HessianType = H;
    virtual ~HasHessian() {};
    virtual H negative_hessian(const X& x) const = 0;
};

template<typename X>
struct HasDraw{
    using XType = X;
    virtual ~HasDraw() {};
    virtual X draw(void) const = 0;
};


/**
 * Traits
*/

// check that S::XType is a type and that GetLogpi<typename S> is a base for S
template <typename S, typename = void>
struct has_logpi : std::false_type {};

template<typename S>
struct has_logpi<S, std::void_t<
		    typename S::XType,
		    std::enable_if_t<std::is_base_of_v<GetLogpi<typename S::XType>, S>>
		    >
>: std::true_type{};

template<typename T>
inline constexpr bool has_logpi_v = has_logpi<T>::value;

// Check that S::XType is a type and that GetGradient is a base
template <typename S, typename = void>
struct has_gradient : std::false_type {};

template<typename S>
struct has_gradient<S, std::void_t<
		       typename S::GradientType,
		       std::enable_if_t<std::is_base_of_v<GetGradient<typename S::GradientType>, S>>
		       >
>: std::true_type{};

template<typename T>
inline constexpr bool has_gradient_v = has_gradient<T>::value;


// Check that S::XType is a type and that GetHessian is a base
template <typename S, typename = void>
struct has_hessian : std::false_type {};

template<typename S>
struct has_hessian<S, std::void_t<
		      typename S::HessianType,
		      std::enable_if_t<std::is_base_of_v<GetHessian<typename S::HessianType>, S>>
		      >
>: std::true_type{};

template<typename T>
inline constexpr bool has_hessian_v = has_hessian<T>::value;

}
