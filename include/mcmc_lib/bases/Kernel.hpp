#pragma once

#include <armadillo>
#include <string>
#include "mcmc_lib/bases/State.hpp"
#include "mcmc_lib/bases/Interfaces.hpp"
#include "mcmc_lib/debug.hpp"

namespace kernel{

/**
 * A kernel does transitions
 */
template<typename State>
struct Kernel: public virtual interface::Name{
    using StateType = State;
    std::string param_names{""};
    std::string param_values{""};
    std::string info_names{""};
    arma::uword info_length{0};
    // Inplace update state to the next state
    virtual arma::vec transition(StateType& state) = 0;
};

} // namespace kernel
