#pragma once

#include <armadillo>
#include <fstream>
#include <sstream>
#include <ostream>
#include <string>

#include "mcmc_lib/util/ESS.hpp"
#include "mcmc_lib/bases/Kernel.hpp"
#include "mcmc_lib/debug.hpp"

namespace mcmc{

/**
 * An MCMC method on logpi states with logpi kernels
 */
template<typename State, bool StoreXs=true, bool StoreLs=true, bool StoreRs=true>
class MCMC{
protected:
    arma::mat res;
    arma::mat xs;
    arma::vec ls;
    arma::vec essx;
    arma::vec essl;
    std::stringstream table_head;
    std::stringstream table_body;
public:

    State state;
    MCMC() = default;
    // start an mcmc with kernel k and state s
    explicit MCMC(const State& s) : state{s} {}

    template<typename U = const arma::mat&>
    std::enable_if_t<StoreRs, U>
    get_res(void) const { return res; }

    template<typename U = const arma::mat&>
    std::enable_if_t<StoreXs, U>
    get_xs(void) const { return xs; }

    template<typename U = const arma::vec&>
    std::enable_if_t<StoreLs, U>
    get_ls(void) const { return ls; }

    void reset(void)
    {
	if constexpr (StoreRs){ res.set_size(0,0); }
	if constexpr (StoreXs){ xs.set_size(0,0); }
	if constexpr (StoreLs){ ls.set_size(0); }
	{
	    std::stringstream temp;
	    table_head.swap(temp);
	}
	{
	    std::stringstream temp;
	    table_body.swap(temp);
	}
    }

    void run(kernel::Kernel<State>& kernel, arma::uword nits, arma::uword burns=0)
    {
	table_add_column("Name", state.name);
	table_add_column("Dimension", state.dimension());
	table_add_column("Kernel", kernel.name);
	table_add_column(kernel.param_names, kernel.param_values);

	const arma::uword stores{nits-burns};
	if constexpr (StoreRs){ res.set_size(stores, kernel.info_length); }
	if constexpr (StoreXs){ xs.set_size(stores, state.dimension()); }
	if constexpr (StoreLs){ ls.set_size(stores); }

	arma::wall_clock timer;
	timer.tic();
	DEBUG_COUT("Starting mcmc...");
	// do burn-in transitions
	for(arma::uword i = 0; i < burns; ++i){
	    kernel.transition(state);
	}
	DEBUG_COUT("...finished burnin...");
	if constexpr (StoreRs){ res.row(0).zeros(); }
	if constexpr (StoreXs){ xs.row(0) = arma::vec(state).t(); }
	if constexpr (StoreLs){ ls(0) = state.get_logpi(); }
	// do storing transitions -- Note the 1!
	for(arma::uword i = 1; i < stores; ++i){
	    if constexpr (StoreRs){
		res.row(i) = kernel.transition(state).t();
	    } else{
		kernel.transition(state);
	    }
	    if constexpr (StoreXs){ xs.row(i) = arma::vec(state).t(); }
	    if constexpr (StoreLs){ ls(i) = state.get_logpi(); }
	}
	double timeTaken{timer.toc()};
	DEBUG_COUT("...finished mcmc, took " << timeTaken << " seconds.");
	table_add_column("time", timeTaken);
	table_add_column("nits", nits);
	table_add_column("burn", burns);
	if constexpr (StoreRs){
		table_add_column(kernel.info_names + " ", mean(res,0));
		// armadillo adds a superfluous newline which we overwrite with a space
		table_body.seekp(-2, table_body.cur);// delete newline and space
		table_body << " ";// add space
	    }
    }

    template<typename U = arma::vec>
    std::enable_if_t<StoreXs, U>
    calc_ess_xs(void)
    {
	if(essx.n_elem == 0){
	    essx = ess::ESSmv(xs);
	    table_add_column("min_ess_x", essx.min());
	}
	return essx;
    }

    template<typename U = arma::vec>
    std::enable_if_t<StoreXs, U>
    calc_ess_xs(const std::string& fname)
    {
	calc_ess_xs();
	essx.save(fname + "_xs_ess.tab", arma::raw_ascii);
	return essx;
    }

    template<typename U = arma::vec>
    std::enable_if_t<StoreLs, U>
    calc_ess_logpi()
    {
	if(essl.n_elem == 0){
	    essl = ess::ESS(ls);
	    table_add_column("ess_logpi", essl(0));
	}
	return essl;
    }

    template<typename U = arma::vec>
    std::enable_if_t<StoreLs, U>
    calc_ess_logpi(const std::string& fname)
    {
	calc_ess_logpi();
	essl.save(fname + "_logpi_ess.tab", arma::raw_ascii);
	return essl;
    }

    void save_samples(const std::string& fname)
    {
	if constexpr(StoreRs){ res.save(fname+"_res.tab", arma::raw_ascii); }
	if constexpr(StoreXs){ xs.save(fname+"_xs.tab", arma::raw_ascii); }
	if constexpr(StoreLs){ ls.save(fname+"_ls.tab", arma::raw_ascii); }
    }

    // table functions
    template<typename T>
    void table_add_column(const std::string& h, const T& b)
    {
	table_head << h << " ";
	table_body << b << " ";
    }

    std::string get_table_head(void) const
    {
	return table_head.str();
    }

    std::string get_table_body(void) const
    {
	return table_body.str();
    }

    void print_table(std::ostream& os) const
    {
	os << table_head.str() << "\n" << table_body.str() << std::endl;
    }

    void print_table_body(std::ostream& os) const
    {
	os << table_body.str() << std::endl;
    }

    void save_table(const std::string& fname)
    {
	std::ofstream os;
	os.open(fname + "_summary.tab");
	os << table_head.str() << "\n" << table_body.str() << std::endl;
	os.close();
    }

};

} // namespace mcmc
