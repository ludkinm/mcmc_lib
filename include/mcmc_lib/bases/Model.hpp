#pragma once

#include <iostream>
#include <armadillo>
#include "mcmc_lib/bases/Interfaces.hpp"

namespace model{
namespace base{

using namespace interface;

struct Model: public virtual Name{
    virtual arma::uword dimension(void) const = 0;
    using Name::operator<<;
};

template<typename X>
struct Logpi: public virtual Model, public virtual HasLogpi<X>{
    using HasLogpi<X>::XType;
    using HasLogpi<X>::logpi;
};

template<typename X, typename G>
class Gradient : public virtual Logpi<X>, public virtual HasGradient<X, G>
{
public:
    using Logpi<X>::XType;
    using HasGradient<X, G>::GradientType;
};

template<typename X, typename G, typename H>
class Hessian : public virtual Gradient<X,G>, public virtual HasHessian<X, H>
{
public:
    using Gradient<X,G>::XType;
    using Gradient<X,G>::GradientType;
    using HasHessian<X,H>::HessianType;
};

template<typename X>
class DrawableLogpi : public virtual Logpi<X>, public virtual HasDraw<X>
{
public:
    using Logpi<X>::XType;
};

template<typename X, typename G>
class DrawableGradient : public virtual Gradient<X, G>, public virtual DrawableLogpi<X>
{
public:
    using Gradient<X, G>::XType;
    using Gradient<X, G>::GradientType;
};

template<typename X, typename G, typename H>
class DrawableHessian : public virtual Hessian<X, G, H>, public virtual DrawableGradient<X, G>
{
public:
    using Hessian<X, G, H>::XType;
    using Hessian<X, G, H>::GradientType;
    using Hessian<X, G, H>::HessianType;
};

} // namespace base

/**
 * For models over a single vector
 */
namespace vec{
using Logpi = base::Logpi<arma::vec>;
using DrawableLogpi = base::DrawableLogpi<arma::vec>;
using Gradient = base::Gradient<arma::vec, arma::vec>;
using DrawableGradient = base::DrawableGradient<arma::vec, arma::vec>;
template<typename H> using Hessian = base::Hessian<arma::vec, arma::vec, H>;
template<typename H> using DrawableHessian = base::DrawableHessian<arma::vec, arma::vec, H>;
} // namespace vec

// /**
//  * for models over a pair of vectors
//  */
// namespace cat{
// using XType = typename std::pair<const arma::vec&, const arma::vec&>;
// using GradientType = typename std::pair<arma::vec, arma::vec>;
// using Logpi = base::Logpi<XType>;
// using DrawableLogpi = base::DrawableLogpi<XType>;
// using Gradient = base::Gradient<XType, GradientType>;
// using DrawableGradient = base::DrawableGradient<XType, GradientType>;
// template<typename H> using Hessian = base::Hessian<XType, GradientType, H>;
// template<typename H> using DrawableHessian = base::DrawableHessian<XType, GradientType, H>;
// } // namespace cat

} //namespace model
