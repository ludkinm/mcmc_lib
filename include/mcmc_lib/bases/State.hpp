#pragma once

#include <type_traits>
#include <armadillo>
#include "mcmc_lib/bases/Interfaces.hpp"
#include "mcmc_lib/debug.hpp"

namespace state{
namespace base{

using namespace interface;

/**
 * Minimum Interface for a state
 * A state should know its dimension
 * Be convertable to a vector
 * Be assignable by a vector
 * Be printable to a std::ostream
 */
template<typename X>
struct State: public virtual Name{
    using Name::operator<<;

    virtual State& operator=(const arma::vec& y) = 0;

    virtual explicit operator arma::vec() const = 0;

    virtual arma::uword dimension(void) const = 0;
};

/**
 * Can get the logpi value of the state
 * An interface class
 */
template<typename X>
struct Logpi: public State<X>, public virtual GetLogpi<X>{
    // import types
    using GetLogpi<X>::XType;
    // import methods
    using State<X>::operator<<;
    using State<X>::operator=;
    using State<X>::operator arma::vec;
    using State<X>::dimension;
    using GetLogpi<X>::get_logpi;
};

/**
 * A state that can get the gradient
 */
template<typename X, typename G>
class Gradient: public Logpi<X>, public virtual GetGradient<G>{
public:
    // import types
    using Logpi<X>::XType;
    using GetGradient<G>::GradientType;
    // import methods
    using Logpi<X>::operator<<;
    using Logpi<X>::operator=;
    using Logpi<X>::operator arma::vec;
    using Logpi<X>::dimension;
    using Logpi<X>::get_logpi;
    using GetGradient<G>::get_gradient;
};

/**
 * A state that can get logpi, gradient and the negative hessian
 */
template<typename X, typename G, typename H>
class Hessian : public Gradient<X,G>, public virtual GetHessian<H>{
public:
    // import types
    using Gradient<X,G>::XType;
    using Gradient<X,G>::GradientType;
    using GetHessian<H>::HessianType;
    // import methods
    using Gradient<X,G>::operator<<;
    using Gradient<X,G>::operator=;
    using Gradient<X,G>::operator arma::vec;
    using Gradient<X,G>::dimension;
    using Gradient<X,G>::get_logpi;
    using Gradient<X,G>::get_gradient;
    using GetHessian<H>::get_negative_hessian;
};

} // namespace base
} // namespace state
