#pragma once

#include <iostream>
#include <armadillo>
#include "mcmc_lib/util/SquareMatrices.hpp"
#include "mcmc_lib/kernels/MetHast.hpp"
#include "mcmc_lib/debug.hpp"

namespace kernel{

/**
 * Random walk metropolis kernel.
 * Its a Metropolis-Hastings style kernel.
 * The proposal mechanism is draw from a MVN(x, Sigma)
 * where Sigma is the proposal variance
 * Sigma should be one of the square matrices in /util/SquareMatrices.hpp
 */
template<typename Sigma, typename State>
class RWM: public MetHast<State>{
protected:
    static_assert(interface::has_logpi_v<State>, "RWM needs a state which uses the GetLogpi interface!");
    static_assert(matrices::is_square_v<Sigma>, "RWM::Sigma must be a square matrix!");
    Sigma sigma;
public:
    using typename MetHast<State>::StateType;

    RWM(const Sigma& sig, const State& s):
    MetHast<State>{s}, sigma{sig}
    {
	this->name = "RWM";
	this->param_names = "";
	this->param_values = "";
	this->info_names = this->proposal_added_names + " logR";
	this->info_length = this->proposal_info_length + 1;
    }

    arma::vec propose(StateType& sProp, StateType& sCurr) override
    {
	DEBUG_COUT("RWM::propose\ncurr = "<< sCurr);
	arma::vec z{arma::randn(sCurr.dimension())};
	DEBUG_COUT("z ~MVN(0,I) = " << z.t());
	z = sigma.sqrt_times(z);
	DEBUG_COUT("z ~MVN(0,sigma) = " << z.t());
	sProp = arma::vec(sCurr) + z;
	DEBUG_COUT("prop = " << sProp);
	double logR {sProp.get_logpi() - sCurr.get_logpi()};
	DEBUG_COUT("logR = " << logR);
	return arma::vec{ logR };
    }

    std::ostream& operator<<(std::ostream& os) const override
    {
	return MetHast<StateType>::operator<<(os) << " with proposal matrix: " << sigma;
    }
};

} // namespace kernel
