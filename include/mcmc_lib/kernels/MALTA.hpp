#pragma once

#include <iostream>
#include <armadillo>
#include "mcmc_lib/util/SquareMatrices.hpp"
#include "mcmc_lib/kernels/MetHast.hpp"
#include "mcmc_lib/debug.hpp"

namespace kernel{

/**
 * MALTA - The Metropolis-adjusted Langevin truncated algorithm
 * @see Exponential convergence of Langevin distributions and their discrete approximations (Roberts and Tweedie 1996)
 * @see https://projecteuclid.org/euclid.bj/1178291835
 * Its a Metropolis-Hastings style kernel.
 * where Sigma is the proposal variance
 * Sigma should be one of the square matrices in /util/SquareMatrices.hpp
 */
template<typename Sigma, typename State>
class MALTA: public MetHast<State>{
protected:
    static_assert(interface::has_logpi_v<State>, "MALTA needs a state which uses the GetLogpi interface!");
    static_assert(interface::has_gradient_v<State>, "MALTA needs a state which uses the GetGradient interface!");
    static_assert(matrices::is_square_v<Sigma>, "MALTA: Sigma must be a square matrix");
    Sigma sigma;
    double D;
public:
    using typename MetHast<State>::StateType;

    MALTA(const Sigma& sig, double d, const State& s):
    MetHast<State>{s}, sigma{sig}, D{d}
    {
	this->name = "MALTA";
	this->param_names = "D";
	this->param_values = std::to_string(D);
	this->info_names = this->proposal_added_names + " logR delta_logpi delta_log_proposal_density ";
	this->info_length = this->proposal_info_length + 3;
    }

    arma::vec propose(StateType& sProp, StateType& sCurr) override
    {
	arma::vec z{arma::randn(sCurr.dimension())};
	double logProbYGivenX{ -0.5 * arma::dot(z,z) };
	arma::vec Sg{ sigma * sCurr.get_gradient() };
	z = 0.5 * D/std::max(D, arma::norm(Sg)) * Sg + sigma.sqrt_times(z);
	// proposal:
	sProp = arma::vec(sCurr) + z;
	Sg = sigma * sProp.get_gradient();
	double logProbXGivenY{ -0.5 * sigma.squared_norm_inv(z + 0.5 * D/std::max(D, arma::norm(Sg)) * Sg) };
	double diffLogpi{ sProp.get_logpi() - sCurr.get_logpi()};
	double diffProb{ logProbXGivenY - logProbYGivenX};
	DEBUG_COUT("logProbYGivenX = " << logProbYGivenX);
	DEBUG_COUT("logProbXGivenY = " << logProbXGivenY);
	DEBUG_COUT("diffLogpi = " << diffLogpi << "\ndiffProb = " << diffProb);
	return arma::vec{ diffLogpi+diffProb, diffLogpi, diffProb};
    }

    std::ostream& operator<<(std::ostream& os) const override { return MetHast<StateType>::operator<<(os) << " with proposal matrix: " << sigma; }
};

} // namespace kernel
