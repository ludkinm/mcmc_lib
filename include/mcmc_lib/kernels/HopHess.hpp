#pragma once

#include <iostream>
#include <armadillo>
#include "mcmc_lib/kernels/MetHast.hpp"
#include "mcmc_lib/util/SquareMatrices.hpp"
#include "mcmc_lib/util/Checks.hpp"
#include "mcmc_lib/debug.hpp"

namespace kernel{
/**
 * Simplified Manifold MALA - MALA with local curvature information
 * Its a Metropolis-Hastings kernel.
 * The proposal mechanism is draw from a MVN(x+\beta^2/2 Sigma(x)*g(x), \beta * Sigma(x))
 * where x is the current state and Sigma is the negative_inverse_hessian.
 * state
 */

template<typename State>
class HopHess: public MetHast<State>{
    static_assert(interface::has_logpi_v<State>,    "HopHess needs a state which uses the GetLogpi interface!");
    static_assert(interface::has_gradient_v<State>, "HopHess needs a state which uses the GetGradient interface!");
    static_assert(interface::has_hessian_v<State>,  "HopHess needs a state which uses the GetHessian interface!");
protected:
    double lambda;
    double kappa;
    double mu;
    double beta;
    double LdivKm1;
    double kByl;
    double K2divL2m1;
public:
    using typename MetHast<State>::StateType;

    HopHess(double l, double k, double bet, const State& sProp):
    MetHast<State>{sProp}, lambda{l}, kappa{k}, mu{sqrt(l*k)}, beta{bet}, LdivKm1{mu/kappa - 1.0}, K2divL2m1{kappa*kappa/mu/mu - 1.0}
    {
	check::sign(lambda);
	check::sign(kappa);
	check::sign(beta);
	this->name = "HopHess";
	this->param_names = "lambda kappa beta ";
	this->param_values = std::to_string(lambda) + " " + std::to_string(kappa) + " " + std::to_string(beta) + " ";
	this->info_names = this->proposal_added_names + "logR proposed_logpi current_logpi qxy_log_rho qxy_neg_hess_log_det qxy_qf qyx_log_rho qyx_neg_hess_log_det qyx_qf ";
	this->info_length = this->proposal_info_length + 9;
    }

    arma::vec propose(StateType& sProp, StateType& sCurr) override
    {
	DEBUG_COUT("HopHess: curr = " << sCurr);

	// draw velocity from N(0, inv(negative_hessian(sCurr)))
	double d{ double(sCurr.dimension()) };
	arma::vec z{arma::randn(sCurr.dimension())};
	typename StateType::GradientType gCurr{ sCurr.get_gradient() };
	typename StateType::HessianType nCurr{ sCurr.get_negative_hessian()/beta/beta };
	double rhoCurr{ nCurr.squared_norm_inv(gCurr) };
	arma::vec3 logProbYGivenX{ 0.5 * d * log(1.0 + rhoCurr), 0.5 * nCurr.log_det(), -0.5 * arma::dot(z,z) };

	z = nCurr.sqrt_solve(z);
	z += LdivKm1/rhoCurr * arma::dot(gCurr, z) * nCurr.solve(gCurr);
	sProp = arma::vec(sCurr) + mu/sqrt(1.0 + rhoCurr) * z;
	typename StateType::GradientType gProp{ sProp.get_gradient() };
	typename StateType::HessianType nProp{ sProp.get_negative_hessian()/beta/beta };
	double rhoProp{ nProp.squared_norm_inv(gProp) };
	double reverseQF{ -0.5 * (1.0 + rhoProp)/(1.0 + rhoCurr) * (nProp.squared_norm(z) + K2divL2m1 * pow(arma::dot(gProp, z), 2)/rhoProp) };
	DEBUG_COUT("HopHess: prop = " << sProp);

	arma::vec3 logProbXGivenY{ 0.5 * d * log(1.0 + rhoProp), 0.5 * nProp.log_det(), reverseQF };
	arma::vec2 logpi{ sProp.get_logpi(), -sCurr.get_logpi() };
	arma::vec logR { arma::sum(logpi) + arma::sum(logProbXGivenY) - arma::sum(logProbYGivenX) };
	DEBUG_COUT("HopHess: logR = " << logR);
	return arma::join_vert(logR, logpi, logProbXGivenY, logProbYGivenX);
    };

    virtual std::ostream& operator<<(std::ostream& os) const override
    {
	return MetHast<StateType>::operator<<(os) <<  "(" << lambda << ", " << kappa << ") with negative hessian precision matrix scaled by " << 1.0/beta/beta;
    }
};
} // namespace kernel
