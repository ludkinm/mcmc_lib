#pragma once

#include <iostream>
#include <armadillo>
#include "mcmc_lib/kernels/MetHast.hpp"
#include "mcmc_lib/util/SquareMatrices.hpp"
#include "mcmc_lib/util/Checks.hpp"
#include "mcmc_lib/debug.hpp"

namespace kernel{

/**
 * HMC - Hamiltonian/Hybrid Monte Carlo
 * Its a Metropolis-Hastings style kernel.
 * The proposal mechanism is simulate hamiltonian dynamics with kinetic energy p ~ MVN(0,mass)
 * where mass is one of the square matrices as defined in /util/SquareMatrices.hpp
 * we use the position leapfrog method
 */
template<typename Mass, typename State>
class HMC: public MetHast<State>{
private:
    static_assert(interface::has_logpi_v<State>, "HMC needs a state which uses the GetLogpi interface!");
    static_assert(interface::has_gradient_v<State>, "HMC needs a state which uses the GetGradient interface!");
    static_assert(matrices::is_square_v<Mass>, "HMC: Mass must be a square matrix");
    Mass mass;
    arma::uword numSteps;
    double stepSize;
    double halfStepSize;
public:
    using typename MetHast<State>::StateType;
    HMC(arma::uword b, double delta, const Mass& m, const State& s) :
    MetHast<State>{s}, mass{m}, numSteps{b}, stepSize{delta}, halfStepSize{0.5*delta}
    {
	check::sign(b+1); // b >= 1
	check::sign(delta);
	this->name = "HMC";
	this->param_names = "numSteps stepSize ";
	this->param_values = std::to_string(numSteps) + " " + std::to_string(stepSize) + " ";
	this->info_names = this->proposal_added_names + "logR prop_logpi curr_logpi q_curr_given_prop q_prop_given_curr norm_diff_propx_currx ";
	this->info_length = this->proposal_info_length + 6;
    }

    arma::vec propose(StateType& sProp, StateType& sCurr) override
    {
	// draw momenta from N(0, mass)
	arma::vec p{arma::randn(sCurr.dimension())};
	arma::vec6 ret = arma::zeros(6);
	ret[2] = -sCurr.get_logpi();
	ret[4] = 0.5 * arma::dot(p,p);
	p = mass.sqrt_times(p); // p ~ MVN(0, mass)
	DEBUG_COUT("p = " << p);
	// run leapfrog
	sProp = arma::vec(sCurr) + halfStepSize * mass.solve(p);   // move half step dx/dt = \nabla_p H = mass^{-1} p
	DEBUG_COUT("x = " << arma::vec(sProp));
	p += stepSize * sProp.get_gradient();
	DEBUG_COUT("p = " << p);
	if(numSteps > 1){                                          // if numSteps is more than 1
	    for(arma::uword b = 1; b < numSteps; ++b){             // do more steps
		sProp = arma::vec(sProp) + stepSize * mass.solve(p); // move full step
		DEBUG_COUT("x = " << arma::vec(sProp));
		p += stepSize * sProp.get_gradient();
		DEBUG_COUT("p = " << p);
	    }
	}
	sProp = arma::vec(sProp) + halfStepSize * mass.solve(p);     // move final half step
	DEBUG_COUT("x = " << arma::vec(sProp));
	// reverse probabilities
	ret[1] = sProp.get_logpi();
	ret[3] = -0.5 * mass.squared_norm_inv(p);
	ret[0] = arma::sum(ret);
	ret[5] = arma::norm(arma::vec(sProp) - arma::vec(sCurr));
	DEBUG_COUT(this->info_names << "\nNA NA " << ret);
	return ret;
    }

    std::ostream& operator<<(std::ostream& os) const override
    {
	return MetHast<StateType>::operator<<(os) << "(" << numSteps << ", " << stepSize << ") with proposal matrix: " << mass;
    }
};

} // namespace kernel
