#pragma once

#include <iostream>
#include <armadillo>
#include "mcmc_lib/kernels/MetHast.hpp"
#include "mcmc_lib/util/Checks.hpp"
#include "mcmc_lib/util/SquareMatrices.hpp"
#include "mcmc_lib/debug.hpp"

namespace kernel{

namespace internal{
inline void reflect(arma::vec& v, const arma::vec& g, const matrices::Square& sig)
{
    v -= 2*arma::dot(v, g)/sig.squared_norm(g) * (sig * g);
}
} // namespace internal

/**
 * Hug transition kernel.
 * Its a Metropolis-Hastings style kernel.
 * The proposal mechanism is to draw a velocity do{move by `stepSize` * velocity, reflect velocity through the gradient of log-target} for `numSteps` iterations
 * Sigma is the proposal variance for the velocity and a preconditioner for the reflections
 * Sigma should be one of the square matrices in /util/SquareMatrices.hpp
 */
template<typename State, typename Sigma>
class Hug: public MetHast<State>{
protected:
    static_assert(interface::has_logpi_v<State>, "Hug needs a state which uses the GetLogpi interface!");
    static_assert(interface::has_gradient_v<State>, "Hug needs a state which uses the GetGradient interface!");
    static_assert(matrices::is_square_v<Sigma>, "Hug: Sigma must be a square matrix");
    Sigma sigma;
    arma::uword numSteps;
    double stepSize;
    double halfStepSize;
public:
    using typename MetHast<State>::StateType;

    Hug(arma::uword b, double delta, const Sigma& sig, const State& s):
    MetHast<State>{s}, sigma{sig}, numSteps{b}, stepSize{delta}, halfStepSize{0.5*delta}
    {
	check::sign(delta);
	this->name = "Hug";
	this->param_names = "numSteps stepSize ";
	this->param_values = std::to_string(numSteps) + " " + std::to_string(stepSize) + " ";
	this->info_names = this->proposal_added_names + "logR delta_logpi delta_log_proposal_density norm2_x_xprop ";
	this->info_length = this->proposal_info_length + 4;
    }

    arma::vec propose(StateType& sProp, StateType& sCurr) override
    {
	if(numSteps > 0){
	    // draw velocity from N(0, sigma)
	    arma::vec v{arma::randn(sCurr.dimension())};
	    double logProbYGivenX{ -0.5 * arma::dot(v,v) };
	    v = sigma.sqrt_times(v);
	    sProp = arma::vec(sCurr) + halfStepSize * v;               // move half step
	    internal::reflect(v, sProp.get_gradient(), sigma);         // reflect velocity at new gradient
	    if(numSteps > 1){                                          // if numSteps is more than 1
		for(arma::uword b = 1; b < numSteps; ++b){             // do more steps
		    sProp = arma::vec(sProp) + stepSize * v;           // move full step
		    internal::reflect(v, sProp.get_gradient(), sigma); // reflect velocity at new gradient
		}
	    }
	    sProp = arma::vec(sProp) + halfStepSize * v;               // move final half step
	    double logProbXGivenY{ -0.5 * sigma.squared_norm_inv(v) };
	    double diffLogpi{ sProp.get_logpi() - sCurr.get_logpi()};
	    double diffProb{ logProbXGivenY - logProbYGivenX};
	    DEBUG_COUT("logProbYGivenX = " << logProbYGivenX);
	    DEBUG_COUT("logProbXGivenY = " << logProbXGivenY);
	    DEBUG_COUT("diffLogpi = " << diffLogpi << "\ndiffProb = " << diffProb);
	    return arma::vec{ diffLogpi+diffProb, diffLogpi, diffProb, arma::norm(arma::vec(sProp) - arma::vec(sCurr)) };
	} else{
	    return arma::zeros(4);
	}
    }

    std::ostream& operator<<(std::ostream& os) const override
    {
	return MetHast<StateType>::operator<<(os) << "(" << numSteps << ", " << stepSize << ") with proposal matrix: " << sigma;
    }
};


template<typename State>
class HugIDMult: public Hug<State, matrices::IDMatrix>{
public:
    HugIDMult(arma::uword b, double d, double scale, const State& s) : Hug<State, matrices::IDMatrix>{b, d, matrices::IDMatrix{scale, s.dimension()}, s} {}
    HugIDMult(arma::uword b, double d, const State& s) : HugIDMult{b, d, 1.0, s} {}
};

template<typename State>
class HugDiag: public Hug<State, matrices::DiagMatrix>{
public:
    HugDiag(arma::uword b, double d, double scale, const State& s) : Hug<State, matrices::DiagMatrix>{b, d, matrices::IDMatrix{scale, s.dimension()}, s} {}
    HugDiag(arma::uword b, double d, const State& s) : HugDiag{b, d, 1.0, s} {}
};

} // namespace kernel
