#pragma once

#include <iostream>
#include <armadillo>
#include "mcmc_lib/kernels/MetHast.hpp"
#include "mcmc_lib/util/SquareMatrices.hpp"
#include "mcmc_lib/debug.hpp"

namespace kernel{

/**
 * Hop kernel.
 * Its a Metropolis-Hastings kernel.
 * The proposal mechanism uses a multivariate normal with a covariance matrix:
 * \f$\mu^2\Sigma + (\lambda^2 - \mu^2)\Sigma g g^\top \Sigma\f$ where \f$\Sigma\f$ is a symmetric matrix and \f$g\f$ is a vector
 *
 * Alternatively, write as
 * Then \f$ Y| X=x \sim \operatorname{MVN}\left(x, \frac{\mu^2}{1+g_x^\top \Sigma g_x}\left(\Sigma + \left(\frac{\mu^2}{\kappa^2} - 1\right) \frac{\Sigma g_x g_x^\top \Sigma}{g_x^\top \Sigma g_x}\right)\right) \f$
 */
template<typename State, typename Sigma>
class Hop: public MetHast<State>{
protected:
    static_assert(interface::has_logpi_v<State>, "Hop needs a state which uses the GetLogpi interface!");
    static_assert(interface::has_gradient_v<State>, "Hop needs a state which uses the GetGradient interface!");
    static_assert(matrices::is_square_v<Sigma>, "Hug: Sigma must be a square matrix");
    Sigma sigma;
    double lambda;
    double kappa;
    double mu;
    double MdivKm1;
    double K2divM2m1;

    void set_sigma(const Sigma& sig)
    {
	sigma = sig;
    }

    void set_params(double l, double k)
    {
	lambda = l;
	kappa = k;
	mu = sqrt(lambda*kappa);
	MdivKm1 = mu/kappa - 1.0;
	K2divM2m1 = kappa/lambda - 1.0;
	if(lambda < 0.0) throw std::logic_error("lambda must be positive");
	if(kappa < 0.0) throw std::logic_error("kappa must be positive");
	this->param_values = std::to_string(lambda) + " " + std::to_string(kappa) + " " + std::to_string(mu) + " ";
    }

public:
    using typename MetHast<State>::StateType;

    Hop(double l, double k, const Sigma& sig, const State& s) :
    MetHast<State>{s}
    {
	set_sigma(sig);
	set_params(l,k);
	this->name = "Hop";
	this->param_names = "lambda kappa mu ";
	this->info_names = this->proposal_added_names + "logR proposed_logpi current_logpi qxy_log_rho qxy_qf qyx_log_rho qyx_qf ";
	this->info_length = this->proposal_info_length + 7;
    }

    arma::vec propose(StateType& sProp, StateType& sCurr) override
    {
	// draw velocity from N(0, sigma)
	double d{ double(sCurr.dimension()) };
	arma::vec z{arma::randn(sCurr.dimension())};
	double rhoCurr{ sigma.squared_norm(sCurr.get_gradient()) };
	arma::vec2 logProbYGivenX{ 0.5 * d * log(1.0 + rhoCurr), -0.5 * arma::dot(z,z) };

	z = sigma.sqrt_times(z);
	z += MdivKm1 * arma::dot(sCurr.get_gradient(), z) * (sigma * sCurr.get_gradient())/rhoCurr;
	sProp = arma::vec(sCurr) + mu/sqrt(1.0 + rhoCurr) * z;
	double rhoProp{ sigma.squared_norm(sProp.get_gradient()) };
	double reverseQF{ -0.5 * (1.0 + rhoProp)/(1.0 + rhoCurr) * (sigma.squared_norm_inv(z) + K2divM2m1 * pow(arma::dot(sProp.get_gradient(), z), 2)/rhoProp) };

	arma::vec2 logProbXGivenY{ 0.5 * d * log(1.0 + rhoProp), reverseQF };
	arma::vec2 logpi{ sProp.get_logpi(), -sCurr.get_logpi() };
	arma::vec logR { arma::sum(logpi) + arma::sum(logProbXGivenY) - arma::sum(logProbYGivenX) };
	return arma::join_vert(logR, logpi, logProbXGivenY, logProbYGivenX);
    }

    std::ostream& operator<<(std::ostream& os) const override
    {
	return MetHast<StateType>::operator<<(os) << "(" << lambda << ", " << kappa << ") with proposal matrix: " << sigma;
    }
};

template<typename State>
class HopIDMult: public Hop<State, matrices::IDMatrix>{
public:
    HopIDMult(double lam, double kap, double scale, const State& s) : Hop<State, matrices::IDMatrix>{lam, kap, matrices::IDMatrix{scale, s.dimension()}, s} {}
    HopIDMult(double lam, double kap, const State& s) : HopIDMult{lam, kap, 1.0, s} {}
};

template<typename State>
class HopDiag: public Hop<State, matrices::DiagMatrix>{
public:
    HopDiag(double lam, double kap, double scale, const State& s) : Hop<State, matrices::DiagMatrix>{lam, kap, matrices::IDMatrix{scale, s.dimension()}, s} {}
    HopDiag(double lam, double kap, const State& s) : HopDiag{lam, kap, 1.0, s} {}
};


} // namespace kernel
