#pragma once

#include <iostream>
#include <armadillo>
#include "mcmc_lib/kernels/MetHast.hpp"
#include "mcmc_lib/util/Checks.hpp"
#include "mcmc_lib/debug.hpp"

namespace kernel{

/**
 * Random walk metropolis kernel using local curvature information.
 * Its a Metropolis-Hastings kernel.
 * The proposal mechanism is draw from a MVN(x, \beta * Sigma(x))
 * where x is the current state and Sigma is the negative_inverse_hessian.
 * state
 */
template<typename State>
class RWMHess: public MetHast<State>{
private:
    static_assert(interface::has_logpi_v<State>,    "RWMHess needs a state which uses the GetLogpi interface!");
    static_assert(interface::has_gradient_v<State>, "RWMHess needs a state which uses the GetGradient interface!");
    static_assert(interface::has_hessian_v<State>,  "RWMHess needs a state which uses the GetHessian interface!");
    double beta;
public:
    using typename MetHast<State>::StateType;

    // for decduction of X,G,H you can construct with a state:
    RWMHess(double b, const State& sProp):
    MetHast<State>{sProp}, beta{b}
    {
	check::sign(beta);
	this->name = "RWMHess";
	this->param_names = "beta";
	this->param_values = std::to_string(beta) + " ";
	this->info_names = this->proposal_added_names + " logR proposed_logpi current_logpi qxy_log_det qxy_qf qyx_log_det qyx_qf ";
	this->info_length = this->proposal_info_length + 7;
    }

    arma::vec propose(StateType& sProp, StateType& sCurr) override
    {
	arma::vec z{ arma::randn(sCurr.dimension()) };
	arma::vec2 logProbYGivenX{ 0.5 * sCurr.get_negative_hessian().log_det(), -0.5 * arma::dot(z,z) };

	z = sCurr.get_negative_hessian().sqrt_solve(z);
	sProp = arma::vec(sCurr) + beta * z;

	arma::vec2 logProbXGivenY{ 0.5 * sProp.get_negative_hessian().log_det(), -0.5 * sProp.get_negative_hessian().squared_norm(z) };
	arma::vec2 logpi{ sProp.get_logpi(), -sCurr.get_logpi() };
	arma::vec logR { arma::sum(logpi) + arma::sum(logProbXGivenY) - arma::sum(logProbYGivenX) };
	return arma::join_vert(logR, logpi, logProbXGivenY, logProbYGivenX);
    };

    virtual std::ostream& operator<<(std::ostream& os) const override
    {
	return MetHast<StateType>::operator<<(os) << " with negative hessian precision matrix scaled by " << 1.0/beta/beta;
    }
};
} // namespace kernel
