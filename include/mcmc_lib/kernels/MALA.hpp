#pragma once

#include <iostream>
#include <armadillo>
#include "mcmc_lib/util/SquareMatrices.hpp"
#include "mcmc_lib/kernels/MetHast.hpp"
#include "mcmc_lib/debug.hpp"

namespace kernel{

/**
 * MALA - Metropolis Adjusted Langevin Algorithm
 * Its a Metropolis-Hastings style kernel.
 * The proposal mechanism is draw from a MVN(x + 1/2 Sigma g, Sigma)
 * where Sigma is the proposal variance
 * Sigma should be one of the square matrices in /util/SquareMatrices.hpp
 */
template<typename Sigma, typename State>
class MALA: public MetHast<State>{
protected:
    static_assert(interface::has_logpi_v<State>, "MALA needs a state which uses the GetLogpi interface!");
    static_assert(interface::has_gradient_v<State>, "MALA needs a state which uses the GetGradient interface!");
    static_assert(matrices::is_square_v<Sigma>, "MALA: Sigma must be a square matrix");
    Sigma sigma;
public:
    using typename MetHast<State>::StateType;

    MALA(const Sigma& sig, const State& s):
    MetHast<State>{s}, sigma{sig}
    {
	this->name = "MALA";
	this->param_names = "";
	this->param_values = "";
	this->info_names = this->proposal_added_names + " logR delta_logpi delta_log_proposal_density ";
	this->info_length = this->proposal_info_length + 3;
    }

    arma::vec propose(StateType& sProp, StateType& sCurr) override
    {
	arma::vec z{arma::randn(sCurr.dimension())};
	double logProbYGivenX{ -0.5 * arma::dot(z,z) };
	z = 0.5 * sigma * sCurr.get_gradient() + sigma.sqrt_times(z);
	// proposal:
	sProp = arma::vec(sCurr) + z;
	double logProbXGivenY{ -0.5 * sigma.squared_norm_inv(z + sigma * sProp.get_gradient()) };
	double diffLogpi{ sProp.get_logpi() - sCurr.get_logpi()};
	double diffProb{ logProbXGivenY - logProbYGivenX};
	DEBUG_COUT("logProbYGivenX = " << logProbYGivenX);
	DEBUG_COUT("logProbXGivenY = " << logProbXGivenY);
	DEBUG_COUT("diffLogpi = " << diffLogpi << "\ndiffProb = " << diffProb);
	return arma::vec{ diffLogpi+diffProb, diffLogpi, diffProb};
    }

    std::ostream& operator<<(std::ostream& os) const override { return MetHast<StateType>::operator<<(os) << " with proposal matrix: " << sigma; }
};

} // namespace kernel
