#pragma once

#include <memory>
#include <iomanip>
#include "mcmc_lib/bases/Kernel.hpp"
#include "mcmc_lib/debug.hpp"

/**
 * A generic metropolis hastings. * Override BaseKernel::transition with (propose and accept/reject).
 * Make propose virtual and concrete proposals inherit from this class.
 */
namespace kernel{

template<typename State>
struct MetHast: public Kernel<State>{
private:
    State prop;
public:

    MetHast(const State& s) : prop{s} {}

    using typename Kernel<State>::StateType;

    virtual arma::vec propose(StateType& next, StateType& curr) = 0;

    std::string proposal_added_names{"accepted alpha "};
    arma::uword proposal_info_length{2};

    arma::vec transition(StateType& s) override
    {
	// Make a copy into the local variable (is this cheaper)
	prop = s;
	arma::vec propRes{propose(prop, s)};
	DEBUG_COUT("propRes = " << propRes.t());
	DEBUG_COUT("logR = " << propRes[0]);
	double A{ propRes.subvec(0,0).is_finite() ? std::min(1.0, exp(propRes(0))) : 0.0};
	DEBUG_COUT("A = " << A);
	DEBUG_COUT("MetHast: curr = " << s);
	DEBUG_COUT("MetHast: prop = " << prop);
	bool accepted{ arma::randu() < A };
	DEBUG_COUT("accepted? = " << std::boolalpha << accepted);
	if(accepted){
	    DEBUG_COUT("assigning s = curr");
	    s = prop;
	}
	DEBUG_COUT("MetHast: next = " << s << "-----------------------");
	return arma::join_vert(arma::vec{double(accepted), A}, propRes);
    };

};

} // namespace kernel
