#pragma once

#include <iostream>
#include <armadillo>
#include "mcmc_lib/bases/Kernel.hpp"

namespace kernel{

template<typename K0, typename K1>
class Alternator: public Kernel<typename K0::StateType>{
private:
    K0 k0;
    K1 k1;
public:
    using typename Kernel<typename K0::StateType>::StateType;

    Alternator(const K0& a, const K1& b) : k0{a}, k1{b}
    {
	this->name = k0.name + "N" + k1.name;
	this->param_names = k0.param_names + " " + k1.param_names;
	this->param_values = k0.param_values + " " + k1.param_values;
	this->info_names = k0.info_names + " " + k1.info_names;
	this->info_length = k0.info_length + k1.info_length;
    }

    virtual arma::vec transition(StateType& s) override
    {
	arma::vec r0 = k0.transition(s);
	arma::vec r1 = k1.transition(s);
	return arma::join_vert(r0, r1);
    }
};

} // namespace kernel
