#pragma once

#include <iostream>
#include <armadillo>
#include "mcmc_lib/kernels/MetHast.hpp"
#include "mcmc_lib/util/Checks.hpp"
#include "mcmc_lib/util/SquareMatrices.hpp"
#include "mcmc_lib/debug.hpp"

namespace kernel{

/**
 * TrunkHop kernel.
 * It's a Metropolis-Hastings kernel.
 * It's a Hop with a truncated gradient term as in MALTA.
 */
template<typename State, typename Sigma>
class TrunkHop: public MetHast<State>{
protected:
    static_assert(interface::has_logpi_v<State>, "TrunkHop needs a state which uses the GetLogpi interface!");
    static_assert(interface::has_gradient_v<State>, "TrunkHop needs a state which uses the GetGradient interface!");
    static_assert(matrices::is_square_v<Sigma>, "Hug: Sigma must be a square matrix");
    Sigma sigma;
    double lambda;
    double kappa;
    double D;
    double mu;
    double LdivKm1;
    double K2divL2m1;
public:
    using typename MetHast<State>::StateType;

    TrunkHop(double l, double k, double d,
	     const Sigma& sig, [[maybe_unused]] const State& s) : sigma{sig}, lambda{l}, kappa{k}, D{d}, mu{sqrt(l*k)},
								  LdivKm1{mu/kappa - 1.0},
								  K2divL2m1{kappa*kappa/mu/mu - 1.0}
    {
	check::sign(mu);
	check::sign(kappa);
	this->name = "TrunkHop";
	this->param_names = "lambda kappa mu D ";
	this->param_values = std::to_string(lambda) + " " + std::to_string(kappa) + " " + std::to_string(mu) + " " + std::to_string(d) + " ";
	this->info_names = this->proposal_added_names + "logR proposed_logpi current_logpi qxy_log_rho qxy_qf qyx_log_rho qyx_qf ";
	this->info_length = this->proposal_info_length + 7;
    }

    arma::vec propose(StateType& sProp, StateType& sCurr) override
    {
	// draw velocity from N(0, sigma)
	double d{ double(sCurr.dimension()) };
	arma::vec z{arma::randn(sCurr.dimension())};
	double rhoCurr{ sigma.squared_norm(sCurr.get_gradient()) };
	double multiplierCurr{ 1.0 + std::max(rhoCurr, D) };
	arma::vec2 logProbYGivenX{ 0.5 * d * log(multiplierCurr), -0.5 * arma::dot(z,z) };

	z = sigma.sqrt_times(z);
	z += LdivKm1 * arma::dot(sCurr.get_gradient(), z) * (sigma * sCurr.get_gradient())/rhoCurr;
	sProp = arma::vec(sCurr) + mu/sqrt(multiplierCurr) * z;
	double rhoProp{ sigma.squared_norm(sProp.get_gradient()) };
	double multiplierProp{ 1.0 + std::max(rhoProp, D) };
	double reverseQF{ -0.5 * multiplierProp/multiplierCurr * (sigma.squared_norm_inv(z) + K2divL2m1 * pow(arma::dot(sProp.get_gradient(), z), 2)/rhoProp) };

	arma::vec2 logProbXGivenY{ 0.5 * d * log(multiplierProp), reverseQF };
	arma::vec2 logpi{ sProp.get_logpi(), -sCurr.get_logpi() };
	arma::vec logR { arma::sum(logpi) + arma::sum(logProbXGivenY) - arma::sum(logProbYGivenX) };
	return arma::join_vert(logR, logpi, logProbXGivenY, logProbYGivenX);
    }

    std::ostream& operator<<(std::ostream& os) const override
    {
	return MetHast<StateType>::operator<<(os) << "(" << lambda << ", " << kappa << ") with proposal matrix: " << sigma;
    }
};

} // namespace kernel
