#pragma once

#include <iostream>
#include <armadillo>
#include "mcmc_lib/kernels/MetHast.hpp"
#include "mcmc_lib/util/Checks.hpp"
#include "mcmc_lib/debug.hpp"

namespace kernel{

/**
 * Simplified Manifold MALA - MALA with local curvature information
 * @see Riemann manifold Langevin and Hamiltonian Monte Carlo methods (Mark Girolami  Ben Calderhead) https://doi.org/10.1111/j.1467-9868.2010.00765.x
 * Its a Metropolis-Hastings kernel.
 * The proposal mechanism is draw from a MVN(x+\beta^2/2 Sigma(x)*g(x), \beta * Sigma(x))
 * where x is the current state and Sigma is the negative_inverse_hessian.
 * state
 */
template<typename State>
class SMMALA: public MetHast<State>{
private:
    static_assert(interface::has_logpi_v<State>,    "SMMALA needs a state which uses the GetLogpi interface!");
    static_assert(interface::has_gradient_v<State>, "SMMALA needs a state which uses the GetGradient interface!");
    static_assert(interface::has_hessian_v<State>,  "SMMALA needs a state which uses the GetHessian interface!");
    double beta;
public:
    using typename MetHast<State>::StateType;

    // for decduction of X,G,H you can construct with a state:
    SMMALA(double b, const State& sProp) :
    MetHast<State>{sProp}, beta{b}
    {
	check::sign(beta);
	this->name = "SMMALA";
	this->param_names = "beta";
	this->param_values = std::to_string(beta) + " ";
	this->info_names = this->proposal_added_names + " logR proposed_logpi current_logpi qxy_log_det qxy_qf qyx_log_det qyx_qf ";
	this->info_length = this->proposal_info_length + 7;
    }

    arma::vec propose(StateType& sProp, StateType& sCurr) override
    {
	arma::vec z{ arma::randn(sCurr.dimension()) };
	arma::vec2 logProbYGivenX{ 0.5 * sCurr.get_negative_hessian().log_det(), -0.5 * arma::dot(z,z) };

	z = 0.5 * beta * sCurr.get_negative_hessian().solve(sCurr.get_gradient()) + sCurr.get_negative_hessian().sqrt_solve(z);
	sProp = arma::vec(sCurr) + beta * z;
	auto negh = sProp.get_negative_hessian();

	arma::vec2 logProbXGivenY{ 0.5 * sProp.get_negative_hessian().log_det(),
   -0.5 * sProp.get_negative_hessian().squared_norm(z + 0.5 * beta * sProp.get_negative_hessian().solve(sProp.get_gradient())) };
	arma::vec2 logpi{ sProp.get_logpi(), -sCurr.get_logpi() };
	arma::vec logR { arma::sum(logpi) + arma::sum(logProbXGivenY) - arma::sum(logProbYGivenX) };
	return arma::join_vert(logR, logpi, logProbXGivenY, logProbYGivenX);
    };

    virtual std::ostream& operator<<(std::ostream& os) const override
    {
	return MetHast<StateType>::operator<<(os) << " with negative hessian precision matrix scaled by " << 1.0/beta/beta;
    }
};

} // namespace kernel
