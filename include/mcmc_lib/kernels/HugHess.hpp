#pragma once

#include <iostream>
#include <armadillo>
#include "mcmc_lib/kernels/MetHast.hpp"
#include "mcmc_lib/kernels/Hug.hpp"
#include "mcmc_lib/util/SquareMatrices.hpp"
#include "mcmc_lib/util/Checks.hpp"
#include "mcmc_lib/debug.hpp"

namespace kernel{

/**
 * HugHess transition kernel - Hug with local curvature information
 * Its a Metropolis-Hastings style kernel.
 * The proposal mechanism is to draw a velocity do{move by `stepSize` * velocity, reflect velocity through the gradient of log-target} for `numSteps` iterations
 * the proposal variance for the velocity and a preconditioner for the reflections is the negative inverse hessian scaled by 1/beta^2
 */
template<typename State>
class HugHess: public MetHast<State>{
    static_assert(interface::has_logpi_v<State>,    "HugHess needs a state which uses the GetLogpi interface!");
    static_assert(interface::has_gradient_v<State>, "HugHess needs a state which uses the GetGradient interface!");
    static_assert(interface::has_hessian_v<State>,  "HugHess needs a state which uses the GetHessian interface!");
protected:
    arma::uword numSteps;
    double stepSize;
    double halfStepSize;
    double beta;
public:
    using typename MetHast<State>::StateType;

    HugHess(arma::uword b, double d, double bet, const State& sProp): MetHast<State>{sProp}, numSteps{b}, stepSize{d}, halfStepSize{0.5*d}, beta{bet}
    {
	check::sign(numSteps+1); // numSteps >= 1
	check::sign(stepSize);
	check::sign(beta);
	this->name = "HugHess";
	this->param_names = "numSteps stepSize beta ";
	this->param_values = std::to_string(numSteps) + " " + std::to_string(stepSize) + " " + std::to_string(beta) + " ";
	this->info_names = this->proposal_added_names + "logR proposed_logpi current_logpi qxy_log_det qxy_qf qyx_log_det qyx_qf ";
	this->info_length = this->proposal_info_length + 7;
    }

    arma::vec propose(StateType& sProp, StateType& sCurr) override
    {
	DEBUG_COUT("HugHess::propose curr = " << sCurr);
	// draw velocity
	arma::vec v{arma::randn(sCurr.dimension())};
	arma::vec2 logProbYGivenX{ 0.5 * sCurr.get_negative_hessian().log_det(), -0.5 * arma::dot(v,v) };
	DEBUG_COUT("HugHess::propose N(0,I) = " << v);
	// proposal
	v = beta * sCurr.get_negative_hessian().sqrt_solve(v);
	sProp = arma::vec(sCurr) + halfStepSize * v; // move half step
	DEBUG_COUT("HugHess::propose - 0 prop = " << sProp << "\nv = " << v);
	internal::reflect(v, sProp.get_gradient(), sProp.get_negative_hessian());         // reflect velocity at new gradient
	if(numSteps > 1){                                                                 // if numSteps is more than 1
	    for(arma::uword b = 1; b < numSteps; ++b){                                    // do more steps
		DEBUG_COUT("HugHess::propose - " << b << " prop = " << sProp << "\nv = " << v);
		sProp = arma::vec(sProp) + stepSize * v;                                  // move full step
		internal::reflect(v, sProp.get_gradient(), sProp.get_negative_hessian()); // reflect velocity at new gradient
	    }
	}
	sProp = arma::vec(sProp) + halfStepSize * v;                                      // move final half step
	DEBUG_COUT("HugHess::propose - " << numSteps << " prop = " << sProp << "\nv = " << v);
	arma::vec2 logProbXGivenY{ 0.5 * sProp.get_negative_hessian().log_det(), -0.5 * sProp.get_negative_hessian().squared_norm(v) };
	arma::vec2 logpi{ sProp.get_logpi(), -sCurr.get_logpi() };
	arma::vec logR { arma::sum(logpi) + arma::sum(logProbXGivenY) - arma::sum(logProbYGivenX) };
	DEBUG_COUT("HhugHess: logR = " << logR);
	return arma::join_vert(logR, logpi, logProbXGivenY, logProbYGivenX);
    };

    virtual std::ostream& operator<<(std::ostream& os) const override
    {
	return MetHast<StateType>::operator<<(os) <<  "(" << numSteps << ", " << stepSize << ") with negative hessian precision matrix scaled by " << 1.0/beta/beta;
    }
};

} // namespace kernel
