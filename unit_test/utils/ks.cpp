#include <iostream>
#include "../tol.hpp"
#include "UnitTest++/UnitTest++.h"
#include "mcmc_lib/util/KStest.hpp"

SUITE(KS_STATS)
{

    TEST(CORRELATED)
    {
	arma::vec x{ arma::linspace(1, 10, 10) };
	arma::vec y{ x+0.5 };
	arma::vec s{ kstest::statistics(x,y) };
	arma::vec p{ kstest::pvalues(s, 10) };
	CHECK_CLOSE(0.1, s(0), tol);
	CHECK_CLOSE(1.0, p(0), tol);
    }

    TEST(NEGATIVE)
    {
	arma::vec x{ arma::linspace(1, 10, 10) };
	arma::vec y{ -x };
	arma::vec s{ kstest::statistics(x,y) };
	arma::vec p{ kstest::pvalues(s, 10) };
	CHECK_CLOSE(1.0, s(0), tol);
	CHECK_CLOSE(1.082508822469741e-5, p(0), tol);
    }

    TEST(FAIL)
    {
	arma::vec x{ arma::linspace(1, 10, 10) };
	arma::vec y{ arma::join_vert(x.subvec(0,4) + 0.5, -x.subvec(5,9)) };
	arma::vec s{ kstest::statistics(x,y) };
	arma::vec p{ kstest::pvalues(s, 10) };
	CHECK_CLOSE(0.5, s(0), tol);
	CHECK_CLOSE(0.1678213427439437, p(0), tol);
    }
}

int main(void)
{
    return UnitTest::RunAllTests();
}
