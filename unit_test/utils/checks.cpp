#include "UnitTest++/UnitTest++.h"
#include "mcmc_lib/util/Checks.hpp"

using namespace check;
SUITE(Int)
{
    TEST(neg)
    {
	CHECK_THROW(sign(-1), std::logic_error);
    }
    TEST(zero)
    {
	CHECK_THROW(sign(0), std::logic_error);
    }
}

SUITE(Double)
{
    TEST(neg)
    {
	CHECK_THROW(sign(-1.0), std::logic_error);
    }
    TEST(zero)
    {
	CHECK_THROW(sign(0.0), std::logic_error);
    }
}


SUITE(vec)
{
    TEST(neg)
    {
	CHECK_THROW(check::sign(arma::vec{1.0, -1.0}), std::logic_error);
    }
    TEST(zero)
    {
	CHECK_THROW(check::sign(arma::vec{0.0, 0.0}), std::logic_error);
    }
}

int main(void)
{
    return UnitTest::RunAllTests();
}
