#include "UnitTest++/UnitTest++.h"
#include "mcmc_lib/util/Cache.hpp"

using namespace cache;
SUITE(Int)
{

    TEST(default_construct)
    {
	Cache<int> x{};
	CHECK_EQUAL(0, int(x)); // test converting to int
	CHECK(!x);              // test converting to bool
    }

    TEST(explicit_construct)
    {
	Cache<int> x(42);
	CHECK_EQUAL(42, int(x)); // test converting to int
	CHECK(x);                // test converting to bool
    }

}

int main(void)
{
    return UnitTest::RunAllTests();
}
