#include <iostream>
#include "../tol.hpp"
#include "UnitTest++/UnitTest++.h"
#include "mcmc_lib/util/SquareMatrices.hpp"

// Test the identity matrix scaled by scalar
const arma::uword d{ 5 };
const double scalar { 4.0 };
const arma::vec sig_vec{ scalar * arma::ones(d) };
const arma::mat sig_mat{ arma::diagmat(sig_vec) };

const arma::vec x{ arma::linspace(1.0, double(d), d) };
const double multiplier { 3.0 };
const double log_det{ log(scalar) * d };
const arma::mat double_mult { multiplier * sig_mat };
const arma::mat div_double { sig_mat / multiplier };
const arma::vec mult_vec{ scalar * x };
const arma::vec solve{ x / scalar };
const arma::vec sqrt_times { sqrt(scalar) * x };
const arma::vec sqrt_solve { x / sqrt(scalar) };

SUITE(IDMat)
{
    const matrices::IDMatrix square_mat{scalar, d};
    DEBUG(TEST(PRINT) { std::cout << square_mat << std::endl; })
    TEST(dimension)   { CHECK_EQUAL(square_mat.dimension()            , d); }
    TEST(is_square)   { CHECK(matrices::is_square_v<decltype(square_mat)>); }
    TEST(is_diagonal) { CHECK(matrices::is_diagonal_v<decltype(square_mat)>); }
    TEST(log_det)     { CHECK_CLOSE(square_mat.log_det()              , log_det             , tol); }
    TEST(arma_vec)    { CHECK_ARRAY_CLOSE(arma::vec(square_mat)       , sig_vec       , d   , tol); }
    TEST(mult_vec)    { CHECK_ARRAY_CLOSE(square_mat * x              , mult_vec      , d   , tol); }
    TEST(solve)       { CHECK_ARRAY_CLOSE(square_mat.solve(x)         , solve         , d   , tol); }
    TEST(sqrt_times)  { CHECK_ARRAY_CLOSE(square_mat.sqrt_times(x)    , sqrt_times    , d   , tol); }
    TEST(sqrt_solve)  { CHECK_ARRAY_CLOSE(square_mat.sqrt_solve(x)    , sqrt_solve    , d   , tol); }
    TEST(double_mult) { CHECK_ARRAY_CLOSE(arma::mat(multiplier * square_mat) , double_mult   , d*d , tol); }
    TEST(mult_double) { CHECK_ARRAY_CLOSE(arma::mat(square_mat * multiplier) , double_mult   , d*d , tol); }
    TEST(div_double)  { CHECK_ARRAY_CLOSE(arma::mat(square_mat/multiplier)   , div_double    , d*d , tol); }
    TEST(arma_mat)    { CHECK_ARRAY_CLOSE(arma::mat(square_mat)       , sig_mat       , d*d , tol); }
}

SUITE(DiagMat)
{
    const matrices::DiagMatrix square_mat{sig_vec};
    TEST(dimension)   { CHECK_EQUAL(square_mat.dimension()            , d); }
    TEST(is_square)   { CHECK(matrices::is_square_v<decltype(square_mat)>); }
    TEST(is_diagonal) { CHECK(matrices::is_diagonal_v<decltype(square_mat)>); }
    TEST(log_det)     { CHECK_CLOSE(square_mat.log_det()              , log_det             , tol); }
    TEST(arma_vec)    { CHECK_ARRAY_CLOSE(arma::vec(square_mat)       , sig_vec       , d   , tol); }
    TEST(mult_vec)    { CHECK_ARRAY_CLOSE(square_mat * x              , mult_vec      , d   , tol); }
    TEST(solve)       { CHECK_ARRAY_CLOSE(square_mat.solve(x)         , solve         , d   , tol); }
    TEST(sqrt_times)  { CHECK_ARRAY_CLOSE(square_mat.sqrt_times(x)    , sqrt_times    , d   , tol); }
    TEST(sqrt_solve)  { CHECK_ARRAY_CLOSE(square_mat.sqrt_solve(x)    , sqrt_solve    , d   , tol); }
    TEST(double_mult) { CHECK_ARRAY_CLOSE(arma::mat(multiplier * square_mat) , double_mult   , d*d , tol); }
    TEST(mult_double) { CHECK_ARRAY_CLOSE(arma::mat(square_mat * multiplier) , double_mult   , d*d , tol); }
    TEST(div_double)  { CHECK_ARRAY_CLOSE(arma::mat(square_mat/multiplier)   , div_double    , d*d , tol); }
    TEST(arma_mat)    { CHECK_ARRAY_CLOSE(arma::mat(square_mat)       , sig_mat       , d*d , tol); }
}

SUITE(SquareMat)
{
    const matrices::SquareMatrix square_mat{sig_mat};
    TEST(dimension)   { CHECK_EQUAL(square_mat.dimension()            , d); }
    TEST(is_square)   { CHECK(matrices::is_square_v<decltype(square_mat)>); }
    TEST(is_diagonal) { CHECK(!matrices::is_diagonal_v<decltype(square_mat)>); }
    TEST(log_det)     { CHECK_CLOSE(square_mat.log_det()              , log_det             , tol); }
    TEST(mult_vec)    { CHECK_ARRAY_CLOSE(square_mat * x              , mult_vec      , d   , tol); }
    TEST(solve)       { CHECK_ARRAY_CLOSE(square_mat.solve(x)         , solve         , d   , tol); }
    TEST(sqrt_times)  { CHECK_ARRAY_CLOSE(square_mat.sqrt_times(x)    , sqrt_times    , d   , tol); }
    TEST(sqrt_solve)  { CHECK_ARRAY_CLOSE(square_mat.sqrt_solve(x)    , sqrt_solve    , d   , tol); }
    TEST(double_mult) { CHECK_ARRAY_CLOSE(arma::mat(multiplier * square_mat) , double_mult   , d*d , tol); }
    TEST(mult_double) { CHECK_ARRAY_CLOSE(arma::mat(square_mat * multiplier) , double_mult   , d*d , tol); }
    TEST(div_double)  { CHECK_ARRAY_CLOSE(arma::mat(square_mat/multiplier)   , div_double    , d*d , tol); }
    TEST(arma_mat)    { CHECK_ARRAY_CLOSE(arma::mat(square_mat)       , sig_mat       , d*d , tol); }
}

SUITE(EigenMat)
{
    const matrices::EigenMatrix square_mat{sig_mat};
    TEST(dimension)   { CHECK_EQUAL(square_mat.dimension()            , d); }
    TEST(is_square)   { CHECK(matrices::is_square_v<decltype(square_mat)>); }
    TEST(is_diagonal) { CHECK(!matrices::is_diagonal_v<decltype(square_mat)>); }
    TEST(log_det)     { CHECK_CLOSE(square_mat.log_det()              , log_det             , tol); }
    TEST(mult_vec)    { CHECK_ARRAY_CLOSE(square_mat * x              , mult_vec      , d   , tol); }
    TEST(solve)       { CHECK_ARRAY_CLOSE(square_mat.solve(x)         , solve         , d   , tol); }
    TEST(sqrt_times)  { CHECK_ARRAY_CLOSE(square_mat.sqrt_times(x)    , sqrt_times    , d   , tol); }
    TEST(sqrt_solve)  { CHECK_ARRAY_CLOSE(square_mat.sqrt_solve(x)    , sqrt_solve    , d   , tol); }
    TEST(double_mult) { CHECK_ARRAY_CLOSE(arma::mat(multiplier * square_mat) , double_mult   , d*d , tol); }
    TEST(mult_double) { CHECK_ARRAY_CLOSE(arma::mat(square_mat * multiplier) , double_mult   , d*d , tol); }
    TEST(div_double)  { CHECK_ARRAY_CLOSE(arma::mat(square_mat/multiplier)   , div_double    , d*d , tol); }
    TEST(arma_mat)    { CHECK_ARRAY_CLOSE(arma::mat(square_mat)       , sig_mat       , d*d , tol); }
}

SUITE(AbsEigenMat)
{
    const matrices::AbsEigenMatrix square_mat{sig_mat};
    TEST(dimension)   { CHECK_EQUAL(square_mat.dimension()            , d); }
    TEST(is_square)   { CHECK(matrices::is_square_v<decltype(square_mat)>); }
    TEST(is_diagonal) { CHECK(!matrices::is_diagonal_v<decltype(square_mat)>); }
    TEST(log_det)     { CHECK_CLOSE(square_mat.log_det()              , log_det     , tol); }
    TEST(mult_vec)    { CHECK_ARRAY_CLOSE(square_mat * x              , mult_vec    , d   , tol); }
    TEST(solve)       { CHECK_ARRAY_CLOSE(square_mat.solve(x)         , solve       , d   , tol); }
    TEST(sqrt_times)  { CHECK_ARRAY_CLOSE(square_mat.sqrt_times(x)    , sqrt_times  , d   , tol); }
    TEST(sqrt_solve)  { CHECK_ARRAY_CLOSE(square_mat.sqrt_solve(x)    , sqrt_solve  , d   , tol); }
    TEST(double_mult) { CHECK_ARRAY_CLOSE(arma::mat(multiplier * square_mat) , double_mult , d*d , tol); }
    TEST(mult_double) { CHECK_ARRAY_CLOSE(arma::mat(square_mat * multiplier) , double_mult , d*d , tol); }
    TEST(div_double)  { CHECK_ARRAY_CLOSE(arma::mat(square_mat/multiplier)   , div_double  , d*d , tol); }
    TEST(arma_mat)    { CHECK_ARRAY_CLOSE(arma::mat(square_mat)       , sig_mat     , d*d , tol); }
}

SUITE(AbsEigenMatNeg)
{
    const matrices::AbsEigenMatrix square_mat{-sig_mat};
    TEST(dimension)   { CHECK_EQUAL(square_mat.dimension()            , d); }
    TEST(is_square)   { CHECK(matrices::is_square_v<decltype(square_mat)>); }
    TEST(is_diagonal) { CHECK(!matrices::is_diagonal_v<decltype(square_mat)>); }
    TEST(log_det)     { CHECK_CLOSE(square_mat.log_det()              , log_det     , tol); }
    TEST(mult_vec)    { CHECK_ARRAY_CLOSE(square_mat * x              , mult_vec    , d   , tol); }
    TEST(solve)       { CHECK_ARRAY_CLOSE(square_mat.solve(x)         , solve       , d   , tol); }
    TEST(sqrt_times)  { CHECK_ARRAY_CLOSE(square_mat.sqrt_times(x)    , sqrt_times  , d   , tol); }
    TEST(sqrt_solve)  { CHECK_ARRAY_CLOSE(square_mat.sqrt_solve(x)    , sqrt_solve  , d   , tol); }
    TEST(double_mult) { CHECK_ARRAY_CLOSE(arma::mat(multiplier * square_mat) , double_mult , d*d , tol); }
    TEST(mult_double) { CHECK_ARRAY_CLOSE(arma::mat(square_mat * multiplier) , double_mult , d*d , tol); }
    TEST(div_double)  { CHECK_ARRAY_CLOSE(arma::mat(square_mat/multiplier)   , div_double  , d*d , tol); }
    TEST(arma_mat)    { CHECK_ARRAY_CLOSE(arma::mat(square_mat)       , sig_mat     , d*d , tol); }
}

int main(void)
{
    return UnitTest::RunAllTests();
}
