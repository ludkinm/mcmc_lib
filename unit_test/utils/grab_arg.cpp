#include "UnitTest++/UnitTest++.h"
#include "mcmc_lib/util/grab_arg.hpp"

SUITE(Double)
{
    TEST(Double_1)
    {
	char const  * input = "1.0";
	double x = grab_arg<double>(input);
	CHECK_CLOSE(x, 1.0, 1e-10);
    }
}

int main(void)
{
    return UnitTest::RunAllTests();
}
