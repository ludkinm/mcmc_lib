#include "../tol.hpp"
#include "UnitTest++/UnitTest++.h"
#include "mcmc_lib/models/MVN.hpp"

SUITE(normal)
{

    const arma::uword d = 5;
    const arma::vec x = arma::ones(d);

    TEST(normal01)
    {
	const model::MVN normal{arma::zeros(d), arma::eye(d,d)};
	CHECK_CLOSE(-2.5, normal.logpi(x), tol);
	CHECK_ARRAY_CLOSE(-arma::ones(d),
			  normal.gradient(x), d, tol);
	CHECK_ARRAY_CLOSE(arma::eye(d,d),
			  arma::mat(normal.negative_hessian(x)), d*d, tol);
    }

    TEST(normal11)
    {
	const model::MVN normal{arma::ones(d), arma::eye(d,d)};
	CHECK_CLOSE(0.0, normal.logpi(x), tol);
	CHECK_ARRAY_CLOSE(arma::zeros(d),
			  normal.gradient(x), d, tol);
	CHECK_ARRAY_CLOSE(arma::eye(d,d),
			  arma::mat(normal.negative_hessian(x)), d*d, tol);
    }

    TEST(normal_lin)
    {
	const model::MVN normal{arma::zeros(d), arma::diagmat(1.0/arma::square(arma::linspace(1, d, d)))};
	// -0.5 * (x/s)^2
	CHECK_CLOSE(-0.73180555555556, normal.logpi(x), tol);
	CHECK_ARRAY_CLOSE(-1.0/arma::square(arma::linspace(1, d, d)),
			  normal.gradient(x), d, tol);
	CHECK_ARRAY_CLOSE(arma::mat(arma::diagmat(1.0/arma::square(arma::linspace(1, d, d)))),
			  arma::mat(normal.negative_hessian(x)), d*d, tol);
    }

    TEST(normal_lin_noncentre)
    {
	const model::MVN normal{3 * arma::ones(d), arma::diagmat(1.0/arma::square(arma::linspace(1, d, d)))};
	CHECK_CLOSE(-2.9272222222222, normal.logpi(x), tol);
	CHECK_ARRAY_CLOSE(2.0/arma::square(arma::linspace(1, d, d)),
			  normal.gradient(x), d, tol);
	CHECK_ARRAY_CLOSE(arma::mat(arma::diagmat(1.0/arma::square(arma::linspace(1, d, d)))),
			  arma::mat(normal.negative_hessian(x)), d*d, tol);
    }

}


int main(void)
{
    return UnitTest::RunAllTests();
}
