#include "../tol.hpp"
#include "UnitTest++/UnitTest++.h"

#include "mcmc_lib/models/model-factories.hpp"
#include "mcmc_lib/models/Normal.hpp"
#include "mcmc_lib/models/Logistic.hpp"
#include "mcmc_lib/models/Quartic.hpp"
#include "mcmc_lib/models/LG.hpp"
#include "mcmc_lib/models/QG.hpp"
#include "mcmc_lib/models/Banana.hpp"
#include "mcmc_lib/models/Bimodal.hpp"
#include "mcmc_lib/models/PlusPrism.hpp"
#include "mcmc_lib/models/SpikeNSlab.hpp"

using namespace arma;
using namespace std;

const uword d{ 2 };
const uword n{ 100000 };
const vec expectedUnitScales{ ones(d) };
const vec expectedLinearIncreaseScales{ linspace(1,d,d) };
const vec expectedLinearDecreaseScales{ linspace(d,1,d) };

vec sdMonteCarlo(const model::vec::DrawableLogpi& m, uword d, uword n)
{
    mat samples(n, d);
    for(uword i = 0; i < samples.n_rows; ++i)
	samples.row(i) = m.draw().t();
    return stddev(samples).t();
}

TEST(unit_norm)
{
    auto mod = model::unit_scaled<model::vec::Normal>(d);
    CHECK_ARRAY_CLOSE(expectedUnitScales, sdMonteCarlo(mod, d, n), d, mctol);
}

TEST(unit_lg5)
{
    auto mod = model::unit_scaled<model::vec::LG5>(d);
    CHECK_ARRAY_CLOSE(expectedUnitScales, sdMonteCarlo(mod, d, n), d, mctol);
}

TEST(unit_qg3)
{
    auto mod = model::unit_scaled<model::vec::QG3>(d);
    CHECK_ARRAY_CLOSE(expectedUnitScales, sdMonteCarlo(mod, d, n), d, mctol);
}

TEST(unit_banana)
{
    auto mod = model::unit_scaled<model::vec::Banana>(d);
    CHECK_ARRAY_CLOSE(expectedUnitScales, sdMonteCarlo(mod, d, n), d, mctol);
}

TEST(unit_bimodal)
{
    auto mod = model::unit_scaled<model::vec::Bimodal>(d);
    CHECK_ARRAY_CLOSE(expectedUnitScales, sdMonteCarlo(mod, d, n), d, mctol);
}

TEST(unit_plusprism)
{
    auto mod = model::unit_scaled<model::vec::PlusPrism>(d);
    CHECK_ARRAY_CLOSE(expectedUnitScales, sdMonteCarlo(mod, d, n), d, mctol);
}

TEST(unit_spikenslab)
{
    auto mod = model::unit_scaled<model::vec::SpikeNSlab>(d);
    CHECK_ARRAY_CLOSE(expectedUnitScales, sdMonteCarlo(mod, d, n), d, mctol);
}




TEST(linear_inc_norm)
{
    auto mod = model::linear_increase_scaled<model::vec::Normal>(d, d);
    CHECK_ARRAY_CLOSE(expectedLinearIncreaseScales, sdMonteCarlo(mod, d, n), d, mctol);
}

TEST(linear_inc_lg5)
{
    auto mod = model::linear_increase_scaled<model::vec::LG5>(d, d);
    CHECK_ARRAY_CLOSE(expectedLinearIncreaseScales, sdMonteCarlo(mod, d, n), d, mctol);
}

TEST(linear_inc_qg3)
{
    auto mod = model::linear_increase_scaled<model::vec::QG3>(d, d);
    CHECK_ARRAY_CLOSE(expectedLinearIncreaseScales, sdMonteCarlo(mod, d, n), d, mctol);
}

TEST(linear_inc_banana)
{
    auto mod = model::linear_increase_scaled<model::vec::Banana>(d, d);
    CHECK_ARRAY_CLOSE(expectedLinearIncreaseScales, sdMonteCarlo(mod, d, n), d, mctol);
}

TEST(linear_inc_bimodal)
{
    auto mod = model::linear_increase_scaled<model::vec::Bimodal>(d, d);
    CHECK_ARRAY_CLOSE(expectedLinearIncreaseScales, sdMonteCarlo(mod, d, n), d, mctol);
}

TEST(linear_inc_plusprism)
{
    auto mod = model::linear_increase_scaled<model::vec::PlusPrism>(d, d);
    CHECK_ARRAY_CLOSE(expectedLinearIncreaseScales, sdMonteCarlo(mod, d, n), d, mctol);
}

TEST(linear_inc_spikenslab)
{
    auto mod = model::linear_increase_scaled<model::vec::SpikeNSlab>(d, d);
    CHECK_ARRAY_CLOSE(expectedLinearIncreaseScales, sdMonteCarlo(mod, d, n), d, mctol);
}




TEST(linear_dec_norm)
{
    auto mod = model::linear_decrease_scaled<model::vec::Normal>(d, d);
    CHECK_ARRAY_CLOSE(expectedLinearDecreaseScales, sdMonteCarlo(mod, d, n), d, mctol);
}

TEST(linear_dec_lg5)
{
    auto mod = model::linear_decrease_scaled<model::vec::LG5>(d, d);
    CHECK_ARRAY_CLOSE(expectedLinearDecreaseScales, sdMonteCarlo(mod, d, n), d, mctol);
}

TEST(linear_dec_qg3)
{
    auto mod = model::linear_decrease_scaled<model::vec::QG3>(d, d);
    CHECK_ARRAY_CLOSE(expectedLinearDecreaseScales, sdMonteCarlo(mod, d, n), d, mctol);
}

TEST(linear_dec_banana)
{
    auto mod = model::linear_decrease_scaled<model::vec::Banana>(d, d);
    CHECK_ARRAY_CLOSE(expectedLinearDecreaseScales, sdMonteCarlo(mod, d, n), d, mctol);
}

TEST(linear_dec_bimodal)
{
    auto mod = model::linear_decrease_scaled<model::vec::Bimodal>(d, d);
    CHECK_ARRAY_CLOSE(expectedLinearDecreaseScales, sdMonteCarlo(mod, d, n), d, mctol);
}

TEST(linear_dec_plusprism)
{
    auto mod = model::linear_decrease_scaled<model::vec::PlusPrism>(d, d);
    CHECK_ARRAY_CLOSE(expectedLinearDecreaseScales, sdMonteCarlo(mod, d, n), d, mctol);
}

TEST(linear_dec_spikenslab)
{
    auto mod = model::linear_decrease_scaled<model::vec::SpikeNSlab>(d, d);
    CHECK_ARRAY_CLOSE(expectedLinearDecreaseScales, sdMonteCarlo(mod, d, n), d, mctol);
}

int main(void)
{
    return UnitTest::RunAllTests();
}
