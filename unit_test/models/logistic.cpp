#include "../tol.hpp"
#include "UnitTest++/UnitTest++.h"
#include "mcmc_lib/models/Logistic.hpp"

/**
    From wolfram:
    https://www.wolframalpha.com/input/?i=d%2Fdx+-2*log%28cosh%280.5+*+x%2Fa%29%29
    https://www.wolframalpha.com/input/?i=d2%2Fdx2+-2*log%28cosh%280.5+*+x%2Fa%29%29
    in R for more precision
    options(digits=18)
    a = sqrt(3.0)/pi
    x = 1.0
    > 5 * (-2 * log(cosh(0.5 * x / a)))
    -3.64784209434280582
    > -tanh(0.5*x/a)/a
    -1.30528415301358125
    > -0.5/cosh(0.5*x/a)^2/a/a
    -0.793050706794035221
*/

SUITE(logistic)
{

    const arma::uword d{5};
    const arma::vec x{arma::ones(d)};

    TEST(logistic01)
    {
	const model::vec::Logistic logistic{arma::zeros(d), arma::ones(d)};
	CHECK_CLOSE(-3.6478420943428, logistic.logpi(x), tol);
	CHECK_ARRAY_CLOSE(-1.30528415301358125 * arma::ones(d), logistic.gradient(x), d, tol);
	CHECK_ARRAY_CLOSE( 0.793050706794035221 * arma::ones(d), arma::vec(logistic.negative_hessian(x)), d, tol);
    }

}


int main(void)
{
    return UnitTest::RunAllTests();
}
