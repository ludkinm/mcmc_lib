#include "../tol.hpp"
#include "UnitTest++/UnitTest++.h"
#include "mcmc_lib/models/Mixture.hpp"
#include "mcmc_lib/models/Normal.hpp"
#include <armadillo>

using namespace model::vec;
SUITE(mixture1d)
{
    const arma::uword d{1};
    TEST(logpi_mixture)
    {
	arma::arma_rng::set_seed_random();
	auto n0 = std::make_shared<model::vec::Normal>(-3 * arma::ones(d), arma::ones(d));
	auto n1 = std::make_shared<model::vec::Normal>( 3 * arma::ones(d), arma::ones(d));
	model::vec::mixture::Logpi mix{n0,n1};
	arma::vec x{arma::ones(d)};
	std::cout << mix << std::endl;
	std::cout << mix.logpi(x) << std::endl;
    }

    TEST(gradient_mixture)
    {
	auto n0 = std::make_shared<model::vec::Normal>(-3 * arma::ones(d), arma::ones(d));
	auto n1 = std::make_shared<model::vec::Normal>( 3 * arma::ones(d), arma::ones(d));
	model::vec::mixture::Gradient mix{n0,n1};
	arma::vec x{arma::ones(d)};
	std::cout << mix << std::endl;
	std::cout << mix.logpi(x) << std::endl;
	std::cout << mix.gradient(x) << std::endl;
    }

    TEST(hessian_mixture)
    {
	auto n0 = std::make_shared<model::vec::Normal>(-3 * arma::ones(d), arma::ones(d));
	auto n1 = std::make_shared<model::vec::Normal>( 3 * arma::ones(d), arma::ones(d));
	model::vec::mixture::Hessian mix{n0, n1};
	arma::vec x{arma::ones(d)};
	std::cout << mix << std::endl;
	std::cout << mix.logpi(x) << std::endl;
	std::cout << mix.gradient(x) << std::endl;
	std::cout << mix.negative_hessian(x) << std::endl;
    }

    TEST(draw_logpi_mixture)
    {
	auto n0 = std::make_shared<model::vec::Normal>(-3 * arma::ones(d), arma::ones(d));
	auto n1 = std::make_shared<model::vec::Normal>( 3 * arma::ones(d), arma::ones(d));
	model::vec::mixture::DrawableLogpi mix{n0,n1};
	std::cout << mix << std::endl;
	arma::vec x = mix.draw();
	std::cout << x << std::endl;
	std::cout << mix.logpi(x) << std::endl;
    }

    TEST(draw_gradient_mixture)
    {
	auto n0 = std::make_shared<model::vec::Normal>(-3 * arma::ones(d), arma::ones(d));
	auto n1 = std::make_shared<model::vec::Normal>( 3 * arma::ones(d), arma::ones(d));
	model::vec::mixture::DrawableGradient mix{n0,n1};
	std::cout << mix << std::endl;
	arma::vec x = mix.draw();
	std::cout << x << std::endl;
	std::cout << mix.logpi(x) << std::endl;
	std::cout << mix.gradient(x) << std::endl;
    }

    TEST(draw_hessian_mixture)
    {
	auto n0 = std::make_shared<model::vec::Normal>(-3 * arma::ones(d), arma::ones(d));
	auto n1 = std::make_shared<model::vec::Normal>( 3 * arma::ones(d), arma::ones(d));
	model::vec::mixture::DrawableHessian mix{n0,n1};
	std::cout << mix << std::endl;
	arma::vec x = mix.draw();
	std::cout << x << std::endl;
	std::cout << mix.logpi(x) << std::endl;
	std::cout << mix.gradient(x) << std::endl;
	std::cout << mix.negative_hessian(x) << std::endl;
    }
}

SUITE(mixture)
{
    TEST(logpi_mixture)
    {
	arma::arma_rng::set_seed_random();
	auto n0 = std::make_shared<model::vec::Normal>(-3 * arma::ones(2), arma::ones(2));
	auto n1 = std::make_shared<model::vec::Normal>( 3 * arma::ones(2), arma::ones(2));
	model::vec::mixture::Logpi mix{n0,n1};
	arma::vec x{arma::ones(2)};
	std::cout << mix << std::endl;
	std::cout << mix.logpi(x) << std::endl;
    }

    TEST(gradient_mixture)
    {
	auto n0 = std::make_shared<model::vec::Normal>(-3 * arma::ones(2), arma::ones(2));
	auto n1 = std::make_shared<model::vec::Normal>( 3 * arma::ones(2), arma::ones(2));
	model::vec::mixture::Gradient mix{n0,n1};
	arma::vec x{arma::ones(2)};
	std::cout << mix << std::endl;
	std::cout << mix.logpi(x) << std::endl;
	std::cout << mix.gradient(x) << std::endl;
    }

    TEST(hessian_mixture)
    {
	auto n0 = std::make_shared<model::vec::Normal>(-3 * arma::ones(2), arma::ones(2));
	auto n1 = std::make_shared<model::vec::Normal>( 3 * arma::ones(2), arma::ones(2));
	model::vec::mixture::Hessian mix{n0, n1};
	arma::vec x{arma::ones(2)};
	std::cout << mix << std::endl;
	std::cout << mix.logpi(x) << std::endl;
	std::cout << mix.gradient(x) << std::endl;
	std::cout << mix.negative_hessian(x) << std::endl;
    }

    TEST(draw_logpi_mixture)
    {
	auto n0 = std::make_shared<model::vec::Normal>(-3 * arma::ones(2), arma::ones(2));
	auto n1 = std::make_shared<model::vec::Normal>( 3 * arma::ones(2), arma::ones(2));
	model::vec::mixture::DrawableLogpi mix{n0,n1};
	std::cout << mix << std::endl;
	arma::vec x = mix.draw();
	std::cout << x << std::endl;
	std::cout << mix.logpi(x) << std::endl;
    }

    TEST(draw_gradient_mixture)
    {
	auto n0 = std::make_shared<model::vec::Normal>(-3 * arma::ones(2), arma::ones(2));
	auto n1 = std::make_shared<model::vec::Normal>( 3 * arma::ones(2), arma::ones(2));
	model::vec::mixture::DrawableGradient mix{n0,n1};
	std::cout << mix << std::endl;
	arma::vec x = mix.draw();
	std::cout << x << std::endl;
	std::cout << mix.logpi(x) << std::endl;
	std::cout << mix.gradient(x) << std::endl;
    }

    TEST(draw_hessian_mixture)
    {
	auto n0 = std::make_shared<model::vec::Normal>(-3 * arma::ones(2), arma::ones(2));
	auto n1 = std::make_shared<model::vec::Normal>( 3 * arma::ones(2), arma::ones(2));
	model::vec::mixture::DrawableHessian mix{n0,n1};
	std::cout << mix << std::endl;
	arma::vec x = mix.draw();
	std::cout << x << std::endl;
	std::cout << mix.logpi(x) << std::endl;
	std::cout << mix.gradient(x) << std::endl;
	std::cout << mix.negative_hessian(x) << std::endl;
    }
}

int main(void)
{
    return UnitTest::RunAllTests();
}
