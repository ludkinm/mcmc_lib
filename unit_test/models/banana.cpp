#include "../tol.hpp"
#include "UnitTest++/UnitTest++.h"
#include "mcmc_lib/models/Banana.hpp"

using namespace model::vec;
SUITE(banana)
{
    TEST(base_banana)
    {
	Banana mod{1, 1, 0.5};
	arma::vec x = mod.draw();
	std::cout << mod << std::endl;
	std::cout << x << std::endl;
	std::cout << mod.logpi(x) << std::endl;
	std::cout << mod.gradient(x) << std::endl;
	std::cout << mod.negative_hessian(x) << std::endl;
    }
}


int main(void)
{
    return UnitTest::RunAllTests();
}
