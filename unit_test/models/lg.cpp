#include "../tol.hpp"
#include "UnitTest++/UnitTest++.h"
#include "mcmc_lib/models/LG.hpp"

using namespace model::vec;
SUITE(lg)
{

    TEST(lg)
    {
	arma::arma_rng::set_seed_random();
	LG1 mod{arma::ones(5)};
	arma::vec x = mod.draw();
	std::cout << mod << std::endl;
	std::cout << x << std::endl;
	std::cout << mod.dimension() << std::endl;
	std::cout << mod.logpi(x) << std::endl;
	std::cout << mod.gradient(x) << std::endl;
	std::cout << mod.negative_hessian(x) << std::endl;
    }
}


int main(void)
{
    return UnitTest::RunAllTests();
}
