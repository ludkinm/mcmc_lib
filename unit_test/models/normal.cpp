#include "../tol.hpp"
#include "UnitTest++/UnitTest++.h"
#include "mcmc_lib/models/Normal.hpp"

SUITE(normal)
{

    const arma::uword d = 5;
    const arma::vec x = arma::ones(d);

    TEST(normal01)
    {
	const model::vec::Normal normal{arma::zeros(d), arma::ones(d)};
	CHECK_CLOSE(-2.5, normal.logpi(x), tol);
	CHECK_ARRAY_CLOSE(-arma::ones(d), normal.gradient(x), d, tol);
	CHECK_ARRAY_CLOSE( arma::ones(d), arma::vec(normal.negative_hessian(x)), d, tol);
	std::cout << normal << std::endl;
    }

    TEST(normal11)
    {
	const model::vec::Normal normal{arma::ones(d), arma::ones(d)};
	CHECK_CLOSE(0.0, normal.logpi(x), tol);
	CHECK_ARRAY_CLOSE(arma::zeros(d), normal.gradient(x), d, tol);
	CHECK_ARRAY_CLOSE(arma::ones(d), arma::vec(normal.negative_hessian(x)), d, tol);
    }

    TEST(normal_lin)
    {
	const model::vec::Normal normal{arma::zeros(d), arma::linspace(1, d, d)};
	// -0.5 * (x/s)^2
	CHECK_CLOSE(-0.73180555555556, normal.logpi(x), tol);
	CHECK_ARRAY_CLOSE(-1.0/arma::square(arma::linspace(1, d, d)), normal.gradient(x), d, tol);
	CHECK_ARRAY_CLOSE( 1.0/arma::square(arma::linspace(1, d, d)), arma::vec(normal.negative_hessian(x)), d, tol);
    }

    TEST(normal_lin_noncentre)
    {
	const model::vec::Normal normal{3 * arma::ones(d), arma::linspace(1, d, d)};
	// -0.5 * (x/s)^2
	CHECK_CLOSE(-2.9272222222222, normal.logpi(x), tol);
	CHECK_ARRAY_CLOSE(2.0/arma::square(arma::linspace(1, d, d)), normal.gradient(x), d, tol);
	CHECK_ARRAY_CLOSE(1.0/arma::square(arma::linspace(1, d, d)), arma::vec(normal.negative_hessian(x)), d, tol);
    }

}


int main(void)
{
    return UnitTest::RunAllTests();
}
