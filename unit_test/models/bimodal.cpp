#include "../tol.hpp"
#include "UnitTest++/UnitTest++.h"
#include "mcmc_lib/models/Bimodal.hpp"

using namespace model::vec;
SUITE(bimodal)
{
    TEST(base_bimodal)
    {
	arma::arma_rng::set_seed_random();
	Bimodal mod{1, 1, 0.5};
	arma::vec x = mod.draw();
	std::cout << mod << std::endl;
	std::cout << x << std::endl;
	std::cout << mod.dimension() << std::endl;
	std::cout << mod.logpi(x) << std::endl;
	std::cout << mod.gradient(x) << std::endl;
	std::cout << mod.negative_hessian(x) << std::endl;
    }
}


int main(void)
{
    return UnitTest::RunAllTests();
}
