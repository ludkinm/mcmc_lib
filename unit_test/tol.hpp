#pragma once

// deterministic tolerance
static const double tol{1e-8};

// Monte Carlo tolerance
static const double mctol{0.1};
