#include "../tol.hpp"
#include "UnitTest++/UnitTest++.h"
#include "mcmc_lib/models/Normal.hpp"
#include "mcmc_lib/states/Modelled.hpp"

using namespace arma;
using model::vec::Normal;
using state::modelled::Gradient;

SUITE(gradient_state)
{
    const uword d{5};
    auto mod = std::make_shared<Normal>(ones(d), ones(d));
    Gradient p{mod};

    TEST(state_constructor){
	p = 2*ones(d);

	const vec Ex{2 * ones(d)};
	const double El{-2.5};
	const vec Eg{-ones(d)};

	CHECK_ARRAY_CLOSE(Ex, arma::vec(p), d, tol);
	CHECK_CLOSE(El, p.get_logpi(), tol);
	CHECK_ARRAY_CLOSE(Eg, p.get_gradient(), d, tol);

	DEBUG_COUT(p);
    }
}

int main(void)
{
    return UnitTest::RunAllTests();
}
