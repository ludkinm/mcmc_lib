#include "../tol.hpp"
#include "UnitTest++/UnitTest++.h"
#include "mcmc_lib/models/Normal.hpp"
#include "mcmc_lib/states/Modelled.hpp"
#include "mcmc_lib/states/Cache.hpp"
#include "mcmc_lib/util/SquareMatrices.hpp"

using namespace arma;
using model::vec::Normal;
using namespace state;

SUITE(eig_cache_state)
{
    const uword d{5};
    auto mod = std::make_shared<Normal>(ones(d), ones(d));

    modelled::Hessian tmp{mod};
    state_cache::Hessian<modelled::Hessian<Normal>, matrices::EigenMatrix> p{tmp};

    TEST(state_constructor){
	p = 2*ones(d);

	const vec Ex{2 * ones(d)};
	const double El{-2.5};
	const vec Eg{-ones(d)};
	const mat Eh{eye(d,d)};

	CHECK_ARRAY_CLOSE(Ex, arma::vec(p), d, tol);
	CHECK_CLOSE(El, p.get_logpi(), tol);
	CHECK_ARRAY_CLOSE(Eg, p.get_gradient(), d, tol);
	CHECK_ARRAY_CLOSE(Eh, arma::mat(p.get_negative_hessian()), d, tol);

	DEBUG_COUT(p);
    }
}

int main(void)
{
    return UnitTest::RunAllTests();
}
