#include "../tol.hpp"
#include "UnitTest++/UnitTest++.h"
#include "mcmc_lib/bases/State.hpp"


struct Fake: public state::base::Logpi<double>{
    XType x{};
    std::ostream& operator<<(std::ostream& os) const override { return os << x; }
    double get_logpi(void) override { return x; }
    arma::uword dimension(void) const override { return 1; }
    operator arma::vec() const override { return arma::vec(x); }
    Fake& operator=(const arma::vec& y) override { x = y(0); return *this; }
    Fake& operator=(const XType& y) { x = y; return *this; }
};

SUITE(fake)
{
    TEST(init){
	Fake f;
	std::cout << f << std::endl;
	f = 42.0;
	std::cout << f << std::endl;
	f = arma::vec{-65.0};
	std::cout << f << std::endl;
	std::cout << "dim = " << f.dimension() << std::endl;
    }
}

int main(void)
{
    return UnitTest::RunAllTests();
}
