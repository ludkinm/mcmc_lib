#include "../tol.hpp"
#include "UnitTest++/UnitTest++.h"
#include "mcmc_lib/models/Normal.hpp"
#include "mcmc_lib/states/Modelled.hpp"
#include "mcmc_lib/states/Cache.hpp"

using namespace arma;
using model::vec::Normal;
using namespace state;

SUITE(cache_lp_state)
{
    const uword d{5};
    auto mod = std::make_shared<Normal>(ones(d), ones(d));

    modelled::Logpi tmp{mod};
    state_cache::Logpi p{tmp};

    TEST(state_constructor){
	p = 2*ones(d);

	const vec Ex{2 * ones(d)};
	const double El{-2.5};

	CHECK_ARRAY_CLOSE(Ex, arma::vec(p), d, tol);
	CHECK_CLOSE(El, p.get_logpi(), tol);

	DEBUG_COUT(p);
    }

}


int main(void)
{
    return UnitTest::RunAllTests();
}
