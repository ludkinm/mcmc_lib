#include <armadillo>
#include "draw.hpp"
#include "mcmc_lib/util/grab_arg.hpp"
#include "mcmc_lib/models/Banana.hpp"
#include "mcmc_lib/models/Bimodal.hpp"
#include "mcmc_lib/models/Donut.hpp"
#include "mcmc_lib/models/Logistic.hpp"
#include "mcmc_lib/models/LG.hpp"
#include "mcmc_lib/models/model-factories.hpp"
#include "mcmc_lib/models/MVN.hpp"
#include "mcmc_lib/models/Normal.hpp"
#include "mcmc_lib/models/PlusPrism.hpp"
#include "mcmc_lib/models/Product.hpp"
#include "mcmc_lib/models/Quartic.hpp"
#include "mcmc_lib/models/QG.hpp"
#include "mcmc_lib/models/SpikeNSlab.hpp"

using namespace model::vec;
using model::unit_scaled;
using model::linear_decrease_scaled;
using model::linear_increase_scaled;

int main(int argc, char *argv[])
{
    const arma::uword d = grab_arg<int>(1, argc, argv, 3);
    const arma::uword n = grab_arg<int>(2, argc, argv, 1000);

    const std::string prefix{"to_plot"};
    std::cout << "using d,n = " << d << ", " << n << std::endl;

    draw_and_save(linear_increase_scaled<Normal>(d, 10), n, prefix);
    draw_and_save(linear_increase_scaled<Logistic>(d, 10), n, prefix);
    draw_and_save(linear_increase_scaled<Quartic>(d, 10), n, prefix);
    draw_and_save(linear_increase_scaled<LG1>(d, 10), n, prefix);
    draw_and_save(linear_increase_scaled<LG2>(d, 10), n, prefix);
    draw_and_save(linear_increase_scaled<LG5>(d, 10), n, prefix);
    draw_and_save(linear_increase_scaled<QG1>(d, 10), n, prefix);
    draw_and_save(linear_increase_scaled<QG3>(d, 10), n, prefix);
    draw_and_save(linear_increase_scaled<Banana>(d, 10), n, prefix);
    draw_and_save(linear_increase_scaled<Bimodal>(d, 10), n, prefix);
    draw_and_save(linear_increase_scaled<PlusPrism>(d, 10), n, prefix);
    draw_and_save(linear_increase_scaled<SpikeNSlab>(d, 10), n, prefix);
    draw_and_save(linear_increase_scaled<Donut>(d, 10), n, prefix);
    draw_and_save(unit_scaled<Normal>(d), n, prefix);
    draw_and_save(unit_scaled<Logistic>(d), n, prefix);
    draw_and_save(unit_scaled<Quartic>(d), n, prefix);
    draw_and_save(unit_scaled<LG1>(d), n, prefix);
    draw_and_save(unit_scaled<LG2>(d), n, prefix);
    draw_and_save(unit_scaled<LG5>(d), n, prefix);
    draw_and_save(unit_scaled<QG1>(d), n, prefix);
    draw_and_save(unit_scaled<QG3>(d), n, prefix);
    draw_and_save(unit_scaled<Banana>(d), n, prefix);
    draw_and_save(unit_scaled<Bimodal>(d), n, prefix);
    draw_and_save(unit_scaled<PlusPrism>(d), n, prefix);
    draw_and_save(unit_scaled<SpikeNSlab>(d), n, prefix);
    draw_and_save(unit_scaled<Donut>(d), n, prefix);
    return 0;
}
