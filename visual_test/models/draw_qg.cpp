#include <armadillo>
#include "draw.hpp"
#include "mcmc_lib/models/QG.hpp"
#include "mcmc_lib/util/grab_arg.hpp"

// Used to get the constants for QGa models for a=1,2,3,5
// For a given 'a' to find 'b' such that stddev of samples is 1.0

int main(int argc, char *argv[])
{
    const double a = grab_arg<double>(1, argc, argv, 10);
    const double b = grab_arg<double>(2, argc, argv, 10);
    const arma::uword d = grab_arg<int>(3, argc, argv, 10);
    const arma::uword n = grab_arg<int>(4, argc, argv, 100000);
    arma::arma_rng::set_seed_random();
    model::vec::QG mod{arma::ones(d), a, b};
    draws(mod, n);
    return 0;
}
