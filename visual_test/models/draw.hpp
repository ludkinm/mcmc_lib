#pragma once

#include <armadillo>
#include <string>
#include <ostream>
#include "mcmc_lib/bases/Model.hpp"

arma::mat draws(const model::vec::DrawableLogpi& m, arma::uword n)
{
    std::cout << m << std::endl << "d=" << m.dimension() << std::endl << "n=" << n << std::endl;
    arma::mat samples(n, m.dimension());
    std::cout << samples.size() << std::endl;
    for(arma::uword i = 0; i < n; ++i)
	samples.row(i) = m.draw().t();
    std::cout << m << ". Standard deviations:"  << std::endl;
    std::cout << stddev(samples) << std::endl;
    return samples;
}

void draw_and_save(const model::vec::DrawableLogpi& m, arma::uword n, const std::string& filePrefix)
{
    arma::mat samples{draws(m, n)};
    samples.save(filePrefix + "_" + m.name + ".tab", arma::raw_ascii);
}
