#include <memory>
#include <iostream>
#include <iomanip>

int main()
{
    // How to tell if two shared_ptrs point to the same thing?
    auto A = std::make_shared<int>(2);
    auto B{A};
    std::cout << std::boolalpha;
    std::cout << "(A, *A) = " << A << ", " << *A << std::endl;
    std::cout << "(B, *B) = " << B << ", " << *B << std::endl;
    std::cout << "(A==B): " << (A==B) << std::endl;


    return 0;
}
