
#include <iostream>
#include "armadillo"

using namespace arma;
using namespace std;

int main(void)
{
    double time;
    mat M;
    vec x;
    vec v;
    vec y;
    wall_clock timer;
    uword d = 1;
    while(d < 10000){
	cout << d << endl;
	M.eye(d, d);
	v.ones(d);
	x.ones(d);

	timer.tic();
	y.zeros(d);
	for(int i = 0; i < 10000; ++i){
	    y += M * x;
	}
	time = timer.toc();
	cout << "DiagMat * vec took " << time << endl;

	timer.tic();
	y.zeros(d);
	for(int i = 0; i < 10000; ++i)
	    y += v % x;
	time = timer.toc();
	cout << "Vector  % vec took " << time << endl;
	d *= 10;
    }
    return 0;
}
