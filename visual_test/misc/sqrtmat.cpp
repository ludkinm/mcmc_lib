#include "mcmc_lib/util/SquareMatrices.hpp"
#include "mcmc_lib/util/grab_arg.hpp"

/** Generate from a multivariate normal using different matrix objects
    - save to csv
    - plot to check they look the same
*/

using namespace arma;
using namespace matrices;

int main(int argc, char *argv[])
{
    const uword n_samples{grab_arg<uword>(1, argc, argv, 10000)};
    const uword d{grab_arg<uword>(2, argc, argv, 2)};

    arma::arma_rng::set_seed_random();
    const mat            sig_sqrt{randu(d,d)};
    const mat            sig_mat{sig_sqrt.t() * sig_sqrt};
    const SquareMatrix   sig_sqr{sig_mat};
    const EigenMatrix    sig_eig{sig_mat};
    const AbsEigenMatrix sig_abseig{sig_mat};

    vec z;
    mat xs_mat(n_samples, d);
    mat xs_square(n_samples, d);
    mat xs_eig(n_samples, d);
    mat xs_abseig(n_samples, d);

    arma::arma_rng::set_seed(1);
    for(uword i = 0; i < n_samples; ++i){
	z.randn(d);
	xs_mat.row(i)    = (arma::chol(sig_mat).t() * z).t();
	xs_square.row(i) = sig_sqr.sqrt_times(z).t();
	xs_eig.row(i)    = sig_eig.sqrt_times(z).t();
	xs_abseig.row(i) = sig_abseig.sqrt_times(z).t();
    }

    xs_mat.save("normal_sqrt_samples_arma_mat.tab", raw_ascii);
    xs_square.save("normal_sqrt_samples_square.tab", raw_ascii);
    xs_eig.save("normal_sqrt_samples_eig.tab", raw_ascii);
    xs_abseig.save("normal_sqrt_samples_abseig.tab", raw_ascii);

    return 0;
}
