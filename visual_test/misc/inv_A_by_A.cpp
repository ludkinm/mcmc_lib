#include <iostream>
#include "armadillo"

using namespace arma;
using namespace std;

int main(void)
{
    double time;
    mat A;
    wall_clock timer;
    uword d = 1;
    mat x;

    while(d < 10000){
	cout << d << endl;
	// ensure A has an inverse
	A.randn(d, d);
	A = A.t() * A;

	timer.tic();
	for(int i = 0; i < 10000; ++i){
	    x = inv(A) * A;
	}
	time = timer.toc();
	cout << "inv(A) * A took " << time << endl;

	d *= 10;
    }
    return 0;
}
