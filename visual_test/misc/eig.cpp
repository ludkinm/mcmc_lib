#include <armadillo>
#include <iostream>
#include <iomanip>

using namespace arma;

int main(void)
{
    std::cout << std::boolalpha;
    const double tol = 1e-6;
    const int nruns = 10000;
    wall_clock timer;

    mat A = randu<mat>(5, 5);
    mat B = A.t()*A;
    vec x = randn(5);

    vec eigval;
    mat eigvec;
    eig_sym(eigval, eigvec, B);

    A = eigvec * diagmat(eigval) * eigvec.t();
    std::cout << "DECOMP: " << approx_equal(A, B, "absdiff", tol) << std::endl;


    std::cout << "MULTIPLY: " << (approx_equal(B*x, eigvec * diagmat(eigval) * eigvec.t() * x, "absdiff", tol) & approx_equal(B*x, eigvec * (eigval % (eigvec.t() * x)), "absdiff", tol)) << std::endl;
    {
	timer.tic();
	for(int i =0; i < nruns; ++i)
	    vec z = B * x;
	std::cout << timer.toc() << std::endl;

	timer.tic();
	for(int i =0; i < nruns; ++i)
	    vec z = eigvec * diagmat(eigval) * eigvec.t() * x;
	std::cout << timer.toc() << std::endl;

	timer.tic();
	for(int i =0; i < nruns; ++i)
	    vec z = eigvec * (eigval % (eigvec.t() * x));
	std::cout << timer.toc() << std::endl;
    }

    std::cout << "LOG_DET: " << approx_equal(arma::vec{real(arma::log_det(B))}, arma::vec{arma::sum(log(eigval))}, "absdiff", tol) << std::endl;
    {
	timer.tic();
	for(int i =0; i < nruns; ++i)
	    double l = real(arma::log_det(B));
	std::cout << timer.toc() << std::endl;

	timer.tic();
	for(int i =0; i < nruns; ++i)
	    double l = arma::sum(log(eigval));
	std::cout << timer.toc() << std::endl;
    }

    std::cout << "SQRT" << std::endl;
    {
	A = randu<mat>(2, 2);
	B = A.t()*A;
	eig_sym(eigval, eigvec, B);
	mat xs(nruns,2);

	timer.tic();
	for(int i =0; i < nruns; ++i){
	    x = randn(2);
	    xs.row(i) = (chol(B).t()*x).t();
	}
	std::cout << timer.toc() << std::endl;
	xs.save("chol.csv", csv_ascii);

	xs = mat(nruns,2);
	timer.tic();
	for(int i =0; i < nruns; ++i){
	    x = randn(2);
	    xs.row(i) = (eigvec * diagmat(sqrt(eigval)) * eigvec.t() * x).t();
	}
	std::cout << timer.toc() << std::endl;
	xs.save("full_eig.csv", csv_ascii);

	xs = mat(nruns,2);
	timer.tic();
	for(int i =0; i < nruns; ++i){
	    x = randn(2);
	    xs.row(i) = (eigvec * (sqrt(eigval) % x)).t();
	}
	std::cout << timer.toc() << std::endl;
	xs.save("half_eig.csv", csv_ascii);

    }

    std::cout << "INV_SQRT" << std::endl;
    {
	A = randu<mat>(2, 2);
	B = A.t()*A;
	eig_sym(eigval, eigvec, B);
	mat xs(nruns,2);

	timer.tic();
	for(int i =0; i < nruns; ++i){
	    x = randn(2);
	    xs.row(i) = solve(chol(B), x).t();
	}
	std::cout << timer.toc() << std::endl;
	xs.save("inv_chol.csv", csv_ascii);

	xs = mat(nruns,2);
	timer.tic();
	for(int i =0; i < nruns; ++i){
	    x = randn(2);
	    xs.row(i) = (eigvec * diagmat(1.0/sqrt(eigval)) * eigvec.t() * x).t();
	}
	std::cout << timer.toc() << std::endl;
	xs.save("inv_full_eig.csv", csv_ascii);

	xs = mat(nruns,2);
	timer.tic();
	for(int i =0; i < nruns; ++i){
	    x = randn(2);
	    xs.row(i) = (eigvec * (x/sqrt(eigval))).t();
	}
	std::cout << timer.toc() << std::endl;
	xs.save("inv_half_eig.csv", csv_ascii);

    }

    return 0;
}
