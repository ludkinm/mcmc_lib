Style guide:
Classes are ThisIsAClass
variables are thisIsAVariable
functions and methods are this_is_a_function_or_method

This project provides an interface to run MCMC algorithms.
It implements some transition kernels and is extendable to new kernels and new models.

Installing:
0) Install dependancies
(a) Armadillo version 9+ available on gitlab https://gitlab.com/conradsnicta/armadillo-code
(b) Boost https://www.boost.org
(c) CMake https://cmake.org
(d) Optional - If you want to run tests you need UnitTest++ -- https://github.com/unittest-cpp/unittest-cpp
(e) Optional - If you want to build the docs you will need doxygen -- http://www.doxygen.nl/manual/install.html

```sh
$ mkdir build
$ cd build
$ cmake -DCMAKE_BUILD_TYPE=Release -DBUILD_DOCS=FALSE  ..
$ make
```
