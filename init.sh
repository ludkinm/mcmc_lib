export CXX=g++-8

if [ "$1" = "BUILD" ]; then
    mkdir -p builds/build
    cd builds/build
    cmake -v -DCMAKE_BUILD_TYPE=Release -DBUILD_DOCS=FALSE  ../..
fi

if [ "$1" = "DEBUG" ]; then
    mkdir -p builds/debug
    cd builds/debug
    cmake -v -DCMAKE_BUILD_TYPE=Debug -DBUILD_DOCS=FALSE  ../..
fi

if [ "$1" = "INFO" ]; then
    mkdir -p builds/info
    cd builds/info
    cmake -v -DCMAKE_BUILD_TYPE=RelWithDebInfo -DBUILD_DOCS=FALSE ../..
fi

if [ "$1" = "INSTALL" ]; then
    mkdir -p builds/install
    cd builds/install
    cmake -v -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=~/.local -DBUILD_DOCS=TRUE -DBUILD_EX=FALSE -DBUILD_TESTS=FALSE ../..
fi
